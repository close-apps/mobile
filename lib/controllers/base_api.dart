import 'dart:convert';

import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/preferences_util.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/request_custom.dart';
import 'package:closeapps/models/response_custom.dart';

class BaseApi {
  Future<ResponseCustom> appelApi(
      {required String endpointApi,
      required RequestCustom request,
      dynamic data,
      dynamic datas}) async {
    Utilities.begin("$endpointApi");

    if (request.user == null) {
      request.user = await PreferencesUtil.instance.getInt(id);
    }
    if (data != null) {
      request.data = data;
    }
    if (datas != null) {
      request.datas = datas;
    }

    //var response = await netWorkCalls.post(bodyRequest: request, uri: apiBaseUrl + "entreprise/getByCriteria") ;
    var response =
        await netWorkCalls.post(bodyRequest: request, endpointApi: endpointApi);
    // var response = await netWorkCalls.postInLocal(
    //     bodyRequest: request, endpointApi: endpointApi);
    var res1 = ResponseCustom.fromJson(jsonDecode(utf8.decode(response)));

    //var res1 = ResponseCustom.fromJson(jsonDecode(response));

    Utilities.log("contenu ResponseCustom ${res1.toJson()}");
    return res1;
  }

  Future<ResponseCustom> newAppelApi(
      {required String endpointApi, dynamic data, dynamic datas}) async {
    Utilities.begin("$endpointApi");

    RequestCustom request = RequestCustom();
    request.user = await PreferencesUtil.instance.getInt(id);
    if (data != null) {
      request.data = data;
    }
    if (datas != null) {
      request.datas = datas;
    }
    var response =
        await netWorkCalls.post(bodyRequest: request, endpointApi: endpointApi);
    // var response = await netWorkCalls.postInLocal(
    //     bodyRequest: request, endpointApi: endpointApi);

    var res1 = ResponseCustom.fromJson(jsonDecode(utf8.decode(response)));

    //var res1 = ResponseCustom.fromJson(jsonDecode(response));

    Utilities.log("contenu ResponseCustom ${res1.toJson()}");
    return res1;
  }

  ResponseCustom testApiCustom(
      {required String apiBaseUrl, required RequestCustom request}) {
    Utilities.begin("$apiBaseUrl");

    //var response = await netWorkCalls.post(bodyRequest: request, uri: apiBaseUrl + "entreprise/getByCriteria") ;
    var response =
        netWorkCalls.postCustom(bodyRequest: request, endpointApi: apiBaseUrl);
    var res1 = ResponseCustom.fromJson(jsonDecode(response));

    Utilities.log("contenu ResponseCustom ${res1.toJson()}");
    return res1;
  }

  /*
  ResponseCustom<T> testApiCustom <T>({String apiBaseUrl, RequestCustom request}){
    print("Begin $apiBaseUrl") ;

    //var response = await netWorkCalls.post(bodyRequest: request, uri: apiBaseUrl + "entreprise/getByCriteria") ;
    var response = netWorkCalls.postCustom(bodyRequest: request, uri: apiBaseUrl) ;
    var res1 = ResponseCustom.fromJson(jsonDecode(response)) ;

    print("contenu ResponseCustom ${res1.toJson()}") ;
    print("End $apiBaseUrl") ;
    return res1 ;
  }
 
  */
}
