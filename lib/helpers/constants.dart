import 'dart:io';

import 'package:closeapps/controllers/base_api.dart';
import 'package:closeapps/models/enseigne.dart';
import 'package:closeapps/models/fidelite_carte.dart';
import 'package:closeapps/models/menu.dart';
import 'package:closeapps/models/user.dart';
import 'package:closeapps/network/network_calls.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';

// l = Libelle de ce qui suit en respectant la casse
// l = Libelle de ce qui suit en respectant la casse

const colorRedLogo = Color(0xFFE34234);
const colorBlueLogo = Color(0xFF000537);
const colorChat = Color(0xFF03a84e);

const colorWitheLogo = Colors.white;

const Map<int, Color> colorCodesBlueLogo = {
  50: Color.fromRGBO(0, 5, 55, .1),
  100: Color.fromRGBO(0, 5, 55, .2),
  200: Color.fromRGBO(0, 5, 55, .3),
  300: Color.fromRGBO(0, 5, 55, .4),
  400: Color.fromRGBO(0, 5, 55, .5),
  500: Color.fromRGBO(0, 5, 55, .6),
  600: Color.fromRGBO(0, 5, 55, .7),
  700: Color.fromRGBO(0, 5, 55, .8),
  800: Color.fromRGBO(0, 5, 55, .9),
  900: Color.fromRGBO(0, 5, 55, 1),
};

const Map<int, Color> colorCodesRedLogo = {
  50: Color.fromRGBO(227, 66, 52, .1),
  100: Color.fromRGBO(227, 66, 52, .2),
  200: Color.fromRGBO(227, 66, 52, .3),
  300: Color.fromRGBO(227, 66, 52, .4),
  400: Color.fromRGBO(227, 66, 52, .5),
  500: Color.fromRGBO(227, 66, 52, .6),
  600: Color.fromRGBO(227, 66, 52, .7),
  700: Color.fromRGBO(227, 66, 52, .8),
  800: Color.fromRGBO(227, 66, 52, .9),
  900: Color.fromRGBO(227, 66, 52, 1),
};
const MaterialColor colorBlueLogoToMaterialColor =
    MaterialColor(0xFF000537, colorCodesBlueLogo);

const MaterialColor colorRedLogoToMaterialColor =
    MaterialColor(0xFFE34234, colorCodesRedLogo);

const logoBlue = "images/logo_blue.png";
const logoWithoutFond = "images/logo_without_fond.png";
const logoWhite = "images/logo_white.png";
const repLogo = "images/logo_entreprises/";
const googleLogo = "images/google_logo.png";
const facebookLogo = "images/facebook_logo.png";

final client = Client();
HttpClient httpClient = new HttpClient();
final netWorkCalls = NetworkCalls();
final baseApi = BaseApi();

const baseUrlAPIStaging = "http://close-apps-back-end.herokuapp.com/";
const defaultUrlLogoOnLine = "https://picsum.photos/id/4/80/80";

//const baseUrlAPIDev = "https://close-apps-back-end.herokuapp.com/";
const baseUrlAPIDev = "close-apps-back-end.herokuapp.com";
const baseUrlAPIProd = "close-apps.com:8080";

const baseUrlAPIDevLocal = "192.168.1.100:8080";

//const baseUrlAPIDev = "http://192.168.1.66:8080/";
//const baseUrlAPIDev = "http://192.168.1.21:8080/";
//const baseUrlAPIDev = "http://192.168.1.100:8080/";
//const baseUrlAPIDev = "http://192.168.1.101:8080/";
//final baseUrlAPIDev = "http://192.168.1.103:8080/";
//final baseUrlAPIDev = "http://192.168.1.102:8080/";

const nbreSecondAnimation = 5;
const zero = 0.0;
const indexCarte = 0;
const indexTampon = 1;
const indexParametre = 2;
const double defaultHeightForSizeBox = 10;

const maxLinesDescriptionPDFTampon = 3;

const fontSize = 20.0;

const expandedHeight = 200.0;

const int defaultNbreTampon = 10;
const int defaultNbrePallier = 4;

const style20RedBold =
    TextStyle(color: colorRedLogo, fontWeight: FontWeight.bold, fontSize: 20);

const style20BlueBold =
    TextStyle(color: colorBlueLogo, fontWeight: FontWeight.bold, fontSize: 20);

// type de input
const typePhone = "phone";
const typeLogin = "login";
const typeEmail = "email";
const typePassword = "password";
const typeNumber = "number";
const typeDatetime = "datetime";
const typeText = "text";
const typeFile = "file";

// caractères spéciaux

const sautDelLine = "\n";
const retourChariot = "\r";
const tabulation = "\t";
const champObligatoire = " *";
const espace = " ";
const tiret = " - ";
const deuxPoint = ": ";

// ******************************** BEGIN  INTERNALISATION ************************************
const pdvSansCoordonneMap =
    "Ce point de vente n'a pas de coordonnée google maps";
const messageNotificationDelitPDF = "Message d'alerte";
const appName = "Close";
const lienApp = "https://play.google.com/store/apps/details?id=com.closeapps";
const lNewUser = "New User";
const lHome = "Accueil";
const lPDV = "PDV";
const introductionNousEvaluer =
    "Merci de nous donner vos impresions de Close afin qu'elle s'améliore";
const lContact = "Contact";
const lContacts = "Contacts";
const lOldUser = "Old User";
const lCartes = "Cartes";
const lDeals = "Deals";
const lParametres = "Parametres";
const lTampon = "Tampons";
const lWelcome = "Bienvenue";
const lInscription = "Inscription";
//const lInscription = "Inscription";
const lAvezVousEteParrainer = "Avez vous été parrainé ?";
const lSinscrire = "S'inscrire";
const lPasswordOublie = "Mot de passe oublié";
const cPasswordOublie = "Mot de passe oublié ?";
//const cChangerPassword = "Changer votre mot de passe";
const lChangerPassword = "Changer votre mot de passe";
const lAProposDeClose = "A propos de Close";
const defaultContentAProposDeClose = "A propos de Close";
const lPartagerClose = "Partager Close";
const lModifierPreferencesApp = "Modifier préférences Close";
const lSupprimerCompte = "Supprimer son compte";
const lNousContacter = "Nous contacter";
const lNousSuivre = "Nous suivre";
const lNousEvaluer = "Nous évaluer";
const lSeConnecter = "Se connecter";
const lSeDeconnecter = "Se deconnecter";
const lMAJProfil = "Modifier Profil";
const lModifier = "Modifier";
const lEnvoyer = "Envoyer";
const lMessageBienvenueConnexion = "Bienvenue chez CLOSE";
const lAVenir = "A venir chez CLOSE";
const lValiderConditions = "Merci d'accepter les conditons d'utilisation !!!";
const lAccepterConditions = "Merci d'avoir accepter nos conditons !!!";
const lLireConditions = "Merci d'avoir lu nos conditons d'utilisation !!!";
const lValider = "Valider";
const lConditionsDutilisation = "Conditions d'utilisation";
const defaultConditionsDutilisation =
    "Qua procella, illo eternus semel cui propello. Arma iniquus tribuo legentis victum tergo victor repeto, multus. Qui volup amita porro perseverantia. Positus lacunar qui praecepio St. Incertus surgo vires nolo. \n Cogo, modicus Malbodiensis defigo, maxime nutus. Spectaculum optimus defluo. Locupleto memor tamisium fodio priores, reddo praecox antea, suum. Sitis orbis duro, cedo moris prolusio nimis consui iuvo, imago. Placo novus";
const questionConditionsDutilisation = "Accepter les conditions d'utilisation";
const lSincrireAuProgramme = "S'inscrire au programme";
const lDefaultConditionsDutilisation =
    "Aucune condition renseigné par l'enseigne pour le moment !!!";
const lScannerMaCarte = "Scanner ma carte ";
const lSuccesSouscriptionProgramme = "Votre programme a été ajouté avec succès";

const lDescription = "Description";
const lCommentaire = "Commmentaire";

const progFid1 = "PROGR FID 1";
const progFid2 = "PROGR FID 2";
//const progFid3 = "PROGRAMME FID 3";
const progFid3 = "Programme de fidélité fidélité 3";

const libelleBouttonConnexionGoogle = "Se connecter avec google";
const libelleBouttonConnexionFacebook = "Se connecter avec facebook";

const libelleBouttonSinscrireGoogle = "S'inscrire avec google";
const libelleBouttonSinscrireFacebook = "S'inscrire avec facebook";
const lFacebook = "Facebook";
const lGoogle = "Google";

const lReponseNon = "Non";
const lReponseOui = "Oui";
const lAnnuler = "Annuler";
const lFermer = "Fermer";
const lNePlusMeRappeler = "Ne plus me rappeler";
const lVerrouillage = "Verrouillage";
const lAction = "Action";
const lAjouter = "Ajouter";
const lParrainer = "Parrainer";
const lConfirmer = "Oui, confirmer";

const uniteDuree = "Unité de la durée";
const lNom = "Nom";
const lId = "Id";
const lCarteId = "carteId";
const lEnseigneId = "enseigneId";
const lPrenoms = "Prenoms";
const lIsConnected = "lIsConnected";
const lEmail = "Email";
const lPhone = "Telephone";
const lPhoneParrain = "Telephone Parrain";
const lConfirmerPassword = "Confirmer mot de passe";
const lConfirmerNouveauPassword = "Confirmer nouveau mot de passe";
const lCommune = "Commune";
const lVille = "Ville";
const lLogin = "Login";
const lIdentifiant = "Identifiant";
const lEmailParrain = "Email parrain";
const lTelephoneParrain = "Téléphone parrain";
const lEmailReceveur = "Email destinataire";
const lTelephoneReceveur = "Téléphone destinataire";
const lEmailFilleul = "Email filleul";
const lTelephoneFilleul = "Téléphone filleul";
const lConnexion = "Connexion";
const lPassword = "Password";
const lMotDePasse = "Mot de passe";
const lAncienPassword = "Ancien mot de passe";
const lNouveauPassword = "Nouveau mot de passe";

const hintSearchEntrepriseByPassword = "Saisir le mot de passe de l'entreprise";

const oldPassword = "Ancien mot de passe";
const newPassword = "Nouveau mot de passe";
const confirmerNewPassword = "Confirmer nouveau mot de passe";
const message = "Message";
const objet = "Objet";
const urlFileOnLine =
    "https://close-apps-front-office.herokuapp.com/assets/images/logo_fond_bleu.png";
const listeVide = "La liste des filleuls est vide. Merci d'en ajouter";
const erreurSaisieTexte = "Ce champ ne doit pas etre vide";
const erreurPassword = "Les deux mots de passe saisis ne sont pas identique";
const erreurSaisieEmail = "Ce champ doit etre un mail";
const erreurPointAPartager = "Le nombre saisi est supérieur à la valeur dispo";
const erreurPartagerZeroPoint = "On ne peut pas partager 0 point";
const erreurValeurSaisie = "Veuillez saisir une valeur correcte !!!";
const erreurPartagerPointNegatif =
    "On ne peut pas dépenser des points négatifs";
const messageChampObligatoire = "Ce champ est obligatoire";
const formatInvalide = "Ce format est invalide";

//const regexChiffreStrictPositif ="r'^[0-9]+$'" ;

const lInscriptionReussi = "Inscription reussi";
const lConnexionReussi = "Connexion reussi !!!";
const lErreurConnexion =
    "Connexion echoué!!! Identifiant et/ou mot de passe incorrect";

const lProblemeDeConnexion =
    "Erreur de connexion. Veuillez réessayer plutard !!!";

const succes = "Operation effectuée avec succès !!!";
const echec = "Votre opération a echoué. Veuillez réessayer plutard !!!";
const problemeDeConnexion =
    "Erreur de connexion. Veuillez réessayer plutard !!!";
const erreurConnexion =
    "Connexion echoué!!! Identifiant et/ou mot de passe incorrect";
const accesInterdit = "Accès interdit";
const inscriptionReussi = "Inscription reussi";
const majReussi = "Mise à jour reussi";

const messageApresInscription = "Votre inscription a reussi !!!";
const messageMAJProfil = "Votre mise à jour été prise en compte";
const okButtonLabel = "OK";

const notificationPasswordOublie =
    "Veuillez verifier votre boite mail/spam pour la suite !!!";

const aucuneDonneeDisponible =
    "Aucune donnée disponible pour le moment. Merci de rester connecté afin de profiter de notre plateforme !!!";
//const aucuneDonneeDisponible = "Bienvenue à vous. Vous pouvez souscrire à des nouveaux programmes à l'aide du bouton +. Merci de rester connecté afin de profiter de notre plateforme !!!";

const aucuneDonneeCorrespondante =
    "Aucune donnée ne correspond à ce critère !!!";
const aucunPDVPourCePDF = "Aucun point de vente disponible pour ce PDF. !!!";

const lPointDeVente = "Point de vente ";
const lPoints = "Points";

const lParrainerDesAmis = "Parrainer des amis";
const lMaCarte = "Ma Carte";
const lPartagerDesPoints = "Partager des points";
const lMaCagnotte = "Ma cagnotte";
const lDepenserDesPoints = "Dépenser des points";
const lHistorique = "Historique";
const lDureeDeValidite = "Durée de validité";

const lTitleBoiteDialogue = "Boite de dialogue";
const lContenuBoiteDialogue = "Contenu boite de dialogue";

const lConfirmation = "Confirmation";
const lContenuConfirmationParrainage = "Confirmez-vous votre parrainage ?";
const lEnvoiConfirme = "Votre partage a été effectué avec succès !!!";
const lRetourAMesCartes = "Retour à mes cartes";

const lEnLigne = "En ligne";
const lEnMagasin = "En magasin";
const lSuivant = "Suivant";
const lPartager = "Partager";
const lRecevoir = "Recevoir";
const lMin = "Min";
const lMax = "Max";
const lXOF = "XOF";
const lOperation = "Opération";
const lDate = "Date";
const lLieu = "Lieu";

// ******************************** END  INTERNALISATION ************************************

// *********************** BEGIN API ****************************

// methode
const create = "/create";
const update = "/update";
const delete = "/delete";
const connexion = "/connexion";
const partagerPoint = "/partagerPoint";
const forceDelete = "/forceDelete";
const getByCriteria = "/getByCriteria";
const getByCriteriaCustom = "/getByCriteriaCustom";
const changerPassword = "/changerPassword";
const passwordOublier = "/passwordOublier";
const updateContenuNotificationDispo = "/updateContenuNotificationDispo";
const getBase64Files = "/getBase64Files";
const parrainer = "/parrainer";

// codes typeAction
const CREDITER = "CREDITER";
const DEBITER = "DEBITER";
const PARRAINER = "PARRAINER";
const PARTAGER = "PARTAGER";

// nom des tables
const pays = 'pays';
const ville = 'ville';
const role = 'role';
const offre = 'offre';
const appelDoffres = 'appelDoffres';
const appelDoffresRestreint = 'appelDoffresRestreint';
const enseigne = "enseigne";

const entreprise = 'entreprise';
const user = 'user';
const domaine = 'domaine';

const historiqueCarteProgrammeFideliteCarte =
    "historiqueCarteProgrammeFideliteCarte";
const historiqueCarteProgrammeFideliteTampon =
    "historiqueCarteProgrammeFideliteTampon";
const souscriptionProgrammeFideliteCarte = "souscriptionProgrammeFideliteCarte";
const notesClose = "notesClose";

const pointDeVenteProgrammeFideliteCarte = "pointDeVenteProgrammeFideliteCarte";
const pointDeVenteProgrammeFideliteTampon =
    "pointDeVenteProgrammeFideliteTampon";
const statutJuridique = 'statutJuridique';
const typeAppelDoffres = 'typeAppelDoffres';
const critereAppelDoffres = 'critereAppelDoffres';
const secteurDactivite = 'secteurDactivite';
const fichier = 'fichier';
const typeEntreprise = 'typeEntreprise';
const typeDuree = 'typeDuree';
const secteurDactiviteEntreprise = "secteurDactiviteEntreprise";

const programmeFideliteCarte = 'programmeFideliteCarte';
const programmeFideliteTampon = 'programmeFideliteTampon';
const souscriptionProgrammeFideliteTampon =
    "souscriptionProgrammeFideliteTampon";
const pointDeVente = 'pointDeVente';

// endpoint
const endpointGetPays = pays + getByCriteria;
const endpointGetRole = role + getByCriteria;
const endpointGetStatutJuridique = statutJuridique + getByCriteria;
const endpointGetTypeEntreprise = typeEntreprise + getByCriteria;
const endpointGetTypeAppelDoffres = typeAppelDoffres + getByCriteria;
const endpointGetCritereAppelDoffres = critereAppelDoffres + getByCriteria;

// constantes BD

const typeString = "STRING";
const typeDate = "DATE";
const typeBoolean = "BOOLEAN";

const API_KEY = "AIzaSyAD6jRBM-AFDDyVuF7vVNp7c_kBbehfqNo";
const BROUILLON = "BROUILLON";
const ENTRANT = "ENTRANT";
const TERMINER = "TERMINER";
const RESTREINT = "RESTREINT";
const LIBELLE = "libelle";
const secteurDactiviteLibelle = "secteurDactiviteLibelle";
const critereAppelDoffresLibelle = "critereAppelDoffresLibelle";
const name = "name";
const libelleExtension = "extension";

const urlFichier = "urlFichier";
const fichierBase64 = "fichierBase64";
const baseUrlWithSSL = "https://spasuce.com/api";
const baseUrlWithoutSSL = "http://207.180.196.73:8080";

// datas close
const telephoneClose = "+2250707070707";
const lTelephoneClose = "+225 0707070707";
const emailClose = "contact@close-apps.com";
const lEmailClose = "contact@close-apps.com";
const facebookClose = "";
const lFacebookClose = "Close";
const lFacebookClose2 = "Rejoignez nous sur Facebook";
const instagramClose = "";
const whatsappClose = "+2250758406013";
const lWhatsappClose = "+225 0758406013";
const telegramClose = "+2250707070707";
const twitterClose = "";
const lTwitterClose = "Rejoignez nous sur Twitter";
const sitewebClose = "close-apps.com";

const id = "id";
const nom = "nom";
const carteId = "carteId";
const enseigneId = "enseigneId";
const prenoms = "prenoms";
const isConnected = "isConnected";
const email = "email";
const telephone = "telephone";
const login = "login";
const phone = "phone";
const password = "password";

// mode form & option submit
const UPDATE = "UPDATE";
const CREATE = "CREATE";
const READ = "READ";

// CONSTANT FORMAT DATE

const dd_MM_yyyy_slash = 'dd/MM/yyyy';
const dd_MM_yyyy_slash_HH_mm_ss = 'dd/MM/yyyy HH:mm:ss';
const dd_MM_yyyy_tiret = 'dd-MM-yyyy';

const HH_colon_mm = 'HH:mm';
const HH_dot_mm = 'HH.mm';
const H_colon_mm = 'H:mm'; // 2:56 et non 02:56
const h_colon_mm_space_a = 'h:mm'; // 2:00 pm pour 14:00

final DateFormat formatter = DateFormat(dd_MM_yyyy_slash_HH_mm_ss);

const operatorDifferent = DataParam(operator: "<>");
const operatorSuperieurA = DataParam(operator: ">=");
const operatorInferieurA = DataParam(operator: "<=");

// *********************** END API ****************************

// ******************************** BEGIN  DATAS FOR TESTING ************************************

const List<Menu> footerMenu = <Menu>[
  Menu(title: lCartes, icon: Icons.credit_card, color: colorBlueLogo),
  Menu(title: lTampon, icon: Icons.bubble_chart, color: colorBlueLogo
      //code: PRESENTATION
      ),
  //Menu(title: lDeals, icon: Icons.business, color: colorBlueLogo),
  Menu(title: lParametres, icon: Icons.settings, color: colorBlueLogo
      //code: FONCTIONNEMENT
      ),
  // MenuFooter(title: 'Autres', icon:Icons.tune, color:Colors.teal),
  //*/
];

/// la liste de certaines enseignes

const Spasuce = Enseigne(
    title: "SPASUCE",
    colorLogo: Colors.blueAccent,
    logo: "${repLogo}spasuce.jpeg");
const Angular = Enseigne(
    title: "ANGULAR", colorLogo: Colors.red, logo: "${repLogo}angular.png");
const CinqPourCent = Enseigne(
    title: "CINQ POUR CENT",
    colorLogo: Colors.deepOrangeAccent,
    logo: "${repLogo}cinqPourCent.jpeg");
const DixPourCnet = Enseigne(
    title: "DIX POUR CENT",
    colorLogo: Colors.blue,
    logo: "${repLogo}dixPourCent.jpg");
const CinquantePourCent = Enseigne(
    title: "CINQUANTE POUR CENT",
    colorLogo: Colors.black,
    logo: "${repLogo}cinquantePourCent.jpg");
const HuitPourCent = Enseigne(
    title: "HUIT POUR CENT",
    colorLogo: Colors.green,
    logo: "${repLogo}huitPourCent.jpeg");
const ML =
    Enseigne(title: "ML", colorLogo: Colors.black, logo: "${repLogo}ml.png");
const MECANIQUE = Enseigne(
    title: "MECANIQUE",
    colorLogo: Colors.grey,
    logo: "${repLogo}mecanique.jpg");
const ELECTRICITE = Enseigne(
    title: "ELECTRICITE",
    colorLogo: Colors.indigo,
    logo: "${repLogo}electricite.jpg");
const INFORMATIQUE = Enseigne(
    title: "INFORMATIQUE",
    colorLogo: Colors.black,
    logo: "${repLogo}informatique.jpg");

List<User> datasUser = [
  // CinqPourCent
  User(
      actionLibelle: "Crédit",
      lieu: "Abobo",
      dateAction: "12/11/2020",
      xof: "1000 F CFA",
      point: "100"),
  User(
      actionLibelle: "Crédit",
      lieu: "Anyama",
      dateAction: "13/11/2020",
      xof: "6000 F CFA",
      point: "300"),
  User(
      actionLibelle: "Débit",
      lieu: "Trechville",
      dateAction: "21/11/2020",
      xof: "5000 F CFA",
      point: "200"),
  User(
      actionLibelle: "Débit",
      lieu: "Adjamé",
      dateAction: "12/12/2020",
      xof: "3000 F CFA",
      point: "150"),
  User(
      actionLibelle: "Crédit",
      lieu: "Korhogo",
      dateAction: "15/12/2020",
      xof: "2000 F CFA",
      point: "100"),
  User(
      actionLibelle: "Débit",
      lieu: "Bouaké",
      dateAction: "20/12/2020",
      xof: "1500 F CFA",
      point: "100"),
];
const List<FideliteCarte> datasCartes = [
  // CinqPourCent
  FideliteCarte(title: progFid1, enseigneFideliteCarte: CinqPourCent),
  FideliteCarte(title: progFid2, enseigneFideliteCarte: CinqPourCent),
  FideliteCarte(title: progFid3, enseigneFideliteCarte: CinqPourCent),

  // HuitPourCent
  FideliteCarte(title: progFid1, enseigneFideliteCarte: HuitPourCent),
  FideliteCarte(title: progFid2, enseigneFideliteCarte: HuitPourCent),
  FideliteCarte(title: progFid3, enseigneFideliteCarte: HuitPourCent),

  // DixPourCnet
  FideliteCarte(title: progFid1, enseigneFideliteCarte: DixPourCnet),
  FideliteCarte(title: progFid2, enseigneFideliteCarte: DixPourCnet),
  FideliteCarte(title: progFid3, enseigneFideliteCarte: DixPourCnet),

  // CinquantePourCent
  FideliteCarte(title: progFid1, enseigneFideliteCarte: CinquantePourCent),
  FideliteCarte(title: progFid2, enseigneFideliteCarte: CinquantePourCent),
  FideliteCarte(title: progFid3, enseigneFideliteCarte: CinquantePourCent),

  // Spasuce
  FideliteCarte(title: progFid1, enseigneFideliteCarte: Spasuce),
  FideliteCarte(title: progFid2, enseigneFideliteCarte: Spasuce),
  FideliteCarte(title: progFid3, enseigneFideliteCarte: Spasuce),

  // ML
  FideliteCarte(title: progFid1, enseigneFideliteCarte: ML),
  FideliteCarte(title: progFid2, enseigneFideliteCarte: ML),
  FideliteCarte(title: progFid3, enseigneFideliteCarte: ML),

  // INFORMATIQUE
  FideliteCarte(title: progFid1, enseigneFideliteCarte: INFORMATIQUE),
  FideliteCarte(title: progFid2, enseigneFideliteCarte: INFORMATIQUE),
  FideliteCarte(title: progFid3, enseigneFideliteCarte: INFORMATIQUE),

  // ELECTRICITE
  FideliteCarte(title: progFid1, enseigneFideliteCarte: ELECTRICITE),
  FideliteCarte(title: progFid2, enseigneFideliteCarte: ELECTRICITE),
  FideliteCarte(title: progFid3, enseigneFideliteCarte: ELECTRICITE),

  // MECANIQUE
  FideliteCarte(title: progFid1, enseigneFideliteCarte: MECANIQUE),
  FideliteCarte(title: progFid2, enseigneFideliteCarte: MECANIQUE),
  FideliteCarte(title: progFid3, enseigneFideliteCarte: MECANIQUE),

  // Angular
  FideliteCarte(title: progFid1, enseigneFideliteCarte: Angular),
  FideliteCarte(title: progFid2, enseigneFideliteCarte: Angular),
  FideliteCarte(title: progFid3, enseigneFideliteCarte: Angular),
];

// ******************************** END  DATAS FOR TESTING ************************************
