import 'package:closeapps/helpers/communications_service.dart';
import 'package:get_it/get_it.dart';

//GetIt locator = GetIt();
GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerSingleton(CommunicationService());
}
