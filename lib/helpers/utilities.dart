import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/preferences_util.dart';
import 'package:closeapps/models/fidelite_carte.dart';
import 'package:closeapps/models/response_custom.dart';
import 'package:closeapps/models/user.dart';
import 'package:closeapps/verification_in_background.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:firebase_auth/firebase_auth.dart' as FireUser;
import 'package:screen_brightness/screen_brightness.dart';

class Utilities {
  static begin(element) {
    print("*********************BEGIN $element*********************");
  }

  static log(element) {
    print("*********************LOG $element*********************");
  }

  static end(element) {
    print("*********************END $element*********************");
  }

  static navigatorPush({
    required BuildContext context,
    dynamic view,
  }) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => view));
  }

  static resetAndOpenPage({@required dynamic context, dynamic view}) {
    begin("resetAndOpenPage");
    //pushNamedAndRemoveUntil
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (BuildContext context) =>
                view != null ? view : VerificationInBackground()),
        (Route<dynamic> route) => false

        //ModalRoute.withName('/'),
        );
    end("resetAndOpenPage");
  }

  static RaisedButton getButtonSubmit({
    Function()? onPressed,
    String libelle = "Soumettre",
    RoundedRectangleBorder? shape,
    Color textColor = Colors.white,
    Color color = colorRedLogo,
    bool isDisable = false,
  }) {
    return RaisedButton(
      color: color,
      textColor: textColor,
      //color: isDisable ? null : color,
      //textColor: isDisable ? Colors.black : textColor,
      //disabledColor: ConstApp.color_button_disable,
      disabledColor: Colors.grey[300],
      disabledTextColor: Colors.black,
      shape: shape != null ? shape : Utilities.getDefaultShape(),
      child: Text(libelle),
      onPressed: isDisable ? null : (onPressed ?? () {}),
    );
  }

  static getEdgeInsets({double valeur = 8.0}) {
    return EdgeInsets.all(valeur);
  }

  static getDefaultShape(
          {double topRight = 10.0,
          double topLeft = 10.0,
          double bottomLeft = 10.0,
          double bottomRight = 10.0}) =>
      RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(topLeft),
          topRight: Radius.circular(topRight),
          bottomLeft: Radius.circular(bottomLeft),
          bottomRight: Radius.circular(bottomRight),
        ),
      );

  static getDefaultInputDecoration({
    required String labelText,
    double borderRadiusValue = 10,
    bool isRequiredChamp = false,
    Widget? suffixIcon,
    Widget? prefixIcon,
  }) {
    return InputDecoration(
      suffixIcon: suffixIcon,
      prefixIcon: prefixIcon,
      labelText: isRequiredChamp ? labelText + champObligatoire : labelText,
      border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(borderRadiusValue)),
    );
  }

  static getDefaultSizeBoxWithHeight(
      {double height = defaultHeightForSizeBox}) {
    return SizedBox(
      height: height,
    );
  }

  static getShape(
          {double topRight = 0.0,
          double topLeft = 0.0,
          double bottomLeft = 0.0,
          double bottomRight = 0.0}) =>
      RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(topLeft),
          topRight: Radius.circular(topRight),
          bottomLeft: Radius.circular(bottomLeft),
          bottomRight: Radius.circular(bottomRight),
        ),
      );

  static style(
          {Color color = colorRedLogo,
          FontWeight fontWeight = FontWeight.bold,
          double fontSize = fontSize}) =>
      TextStyle(color: color, fontWeight: fontWeight, fontSize: fontSize);
  static getToast(String message,
      {dynamic toastLength = Toast.LENGTH_SHORT,
      dynamic gravity = ToastGravity.BOTTOM,
      Color backgroundColor = Colors.black,
      Color textColor = Colors.white,
      fontSize}) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: toastLength,
        gravity: gravity,
        timeInSecForIosWeb: 1,
        backgroundColor: backgroundColor,
        textColor: textColor,
        fontSize: 16.0);
  }

  static alertDialogCustom(
      {required BuildContext context,
      String title = lTitleBoiteDialogue,
      String content = lContenuBoiteDialogue,
      Function()? leftFuction,
      String libelleLeftFuction = lReponseNon,
      Function()? rightFuction,
      String libelleRightFuction = lReponseOui,
      bool afficherToast = true,
      bool isHtmlContent = false}) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text(title),
          content: isHtmlContent
              ? //
              Html(data: content)
              // Text(
              //     content,
              //   )
              : Text(
                  content,
                ),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text(libelleLeftFuction),
              onPressed: leftFuction ??
                  () {
                    Navigator.pop(context);
                    if (afficherToast) {
                      Utilities.getToast(lLireConditions);
                    }
                  },
            ),
            FlatButton(
              child: Text(libelleRightFuction),
              onPressed: rightFuction ??
                  () {
                    Navigator.pop(context);
                    if (afficherToast) {
                      Utilities.getToast(lAccepterConditions);
                    }
                  },
            ),
          ],
        );
      },
    );
  }

  static getDefaultDivider(
      {Color color = colorBlueLogo,
      double endIndent = 10,
      //double height = 10,
      double indent = 10}) {
    return Divider(
      color: colorBlueLogo,
      endIndent: endIndent,
      indent: indent,
      //height: height,
    );
  }

  // static validator(dynamic value,
  //     {String typeContentToValidate,
  //     int minLengthValue,
  //     int maxLengthValue,
  //     dynamic message = erreurSaisieTexte}) {
  //   //begin("validator $value");
  //   if (value != null && !value.isEmpty) {
  //     //if (value != null) {
  //     if (minLengthValue != null &&
  //         minLengthValue > 0 &&
  //         value.toString().length < minLengthValue) {
  //       return message;
  //     }
  //     if (maxLengthValue != null &&
  //         maxLengthValue > 0 &&
  //         value.toString().length > maxLengthValue) {
  //       return message;
  //     }

  //     switch (typeContentToValidate) {
  //       case typeEmail:
  //         Pattern pattern =
  //             r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  //         RegExp regex = new RegExp(pattern);
  //         if (!regex.hasMatch(value)) return formatInvalide;

  //         break;

  //       case typePassword:
  //         break;
  //       case typeLogin:
  //         break;
  //       case typePhone:
  //         break;
  //       case typeNumber:
  //         break;

  //       default:
  //         break;
  //     }
  //   } else {
  //     //end("validator $value");

  //     return message;
  //   }
  // }

  static clearForm(List<TextEditingController> controllers) {
    if (controllers != null && controllers.isNotEmpty) {
      for (var controller in controllers) {
        if (controller != null) {
          controller.clear();
        }
      }
    }
  }

  static loadingIndicator(
      {required BuildContext context,
      Color color = colorBlueLogo,
      Indicator indicator = Indicator.ballScaleMultiple}) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return LoadingIndicator(
            indicatorType: indicator,
            colors: [color],
          );
        });
  }

  static Future<void> setBrightness(double brightness) async {
    try {
      await ScreenBrightness.setScreenBrightness(brightness);
    } catch (e) {
      debugPrint(e.toString());
      throw 'Failed to set brightness';
    }
  }

  static Future<void> resetBrightness() async {
    try {
      await ScreenBrightness.resetScreenBrightness();
    } catch (e) {
      debugPrint(e.toString());
      throw 'Failed to reset brightness';
    }
  }

  static messageApi({ResponseCustom? response, String? typeContenu}) {
    var message = problemeDeConnexion;
    if (response != null) {
      if (response.hasError != null && !response.hasError!) {
        if (response.status != null) {
          // a voir
          Utilities.getToast(response.status['message']);
        } else {
          Utilities.getToast(succes);
        }
      } else {
        if (response.status != null) {
          Utilities.getToast(response.status['message']);
        }
      }
    } else {
      switch (typeContenu) {
        case lConnexion:
          message = erreurConnexion;
          break;
        default:
          break;
      }
      Utilities.getToast(echec);
    }
  }

  static logByTag({String tag = "LOGGER", required String contenu}) {
    print(tag + " : " + contenu);
  }

  static showDialogCustom(
      {required BuildContext context,
      String titre = lTitleBoiteDialogue,
      String contenu = lContenuBoiteDialogue,
      Function()? rightFuction,
      String libelleRightFuction = okButtonLabel,
      bool isHtmlContent = false}) {
    showDialog(
      context: context,
      //barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(titre),
          content: isHtmlContent
              ? //
              Html(
                  data: contenu,
                )
              // Text(
              //     contenu,
              //     textAlign: TextAlign.justify,
              //   )
              : Text(
                  contenu,
                  textAlign: TextAlign.justify,
                ),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: new Text(libelleRightFuction),
              onPressed: rightFuction ??
                  () {
                    Navigator.of(context).pop();
                    //Utilities.onSelectDrawerItem(context: context, view: Offres());
                  },
            ),
          ],
        );
      },
    );
  }

  static saveInLocalBd({required User dataUser}) async {
    await PreferencesUtil.instance.putInt(id, dataUser.id!);
    await PreferencesUtil.instance.putInt(carteId, dataUser.carteId!);
    //await PreferencesUtil.instance.putInt(enseigneId, dataUser.enseigneId!);
    if (dataUser.nom != null) {
      await PreferencesUtil.instance.putString(nom, dataUser.nom!);
    }
    if (dataUser.email != null) {
      await PreferencesUtil.instance.putString(email, dataUser.email!);
    }
    if (dataUser.login != null) {
      await PreferencesUtil.instance.putString(login, dataUser.login!);
    }
    if (dataUser.phone != null) {
      await PreferencesUtil.instance.putString(phone, dataUser.phone!);
    }
    if (dataUser.telephone != null) {
      await PreferencesUtil.instance.putString(telephone, dataUser.telephone!);
    }
    if (dataUser.prenoms != null) {
      await PreferencesUtil.instance.putString(prenoms, dataUser.prenoms!);
    }
    await PreferencesUtil.instance.putBool(isConnected, true);
    //await PreferencesUtil.instance.putString(password, dataUser.password);
  }

  // static get idUserConnected async {
  //   await PreferencesUtil.instance.getInt(id);
  // }

  // static get carteIdUserConnected async {
  //   await PreferencesUtil.instance.getInt(carteId);
  // }

  static String lContenuConfirmationPartagePoints(
      {String? points, String? receveur}) {
    return "Voulez vous envoyer $points points à $receveur ?";
  }

  static EdgeInsetsGeometry paddingAll({double allValue = 8.0}) {
    return EdgeInsets.all(allValue);
  }

  static TableRow emptyTableRow({int nbreColumn = 2}) {
    List<Widget> listContainer = [];
    for (var i = 0; i < nbreColumn; i++) {
      listContainer.add(Container());
    }
    return TableRow(
      children: listContainer,
    );
  }

  static List<TableRow> emptyTable(
      {TableRow? headerTable, int nbreColumn = 2}) {
    List<Widget> listContainer = [];
    List<TableRow> listTableRow = [];
    if (headerTable == null) {
      headerTable = headerHistoriqueTable;
    }
    listTableRow.add(headerTable);

    for (var i = 0; i < nbreColumn; i++) {
      listContainer.add(Container());
    }
    listTableRow.add(TableRow(
      children: listContainer,
    ));
    return listTableRow;
  }

  static TableRow get headerHistoriqueTable {
    return TableRow(
      children: [
        Text(
          lOperation,
          style: Utilities.style(fontSize: 16, color: colorBlueLogo),
        ),
        Text(
          lDate,
          style: Utilities.style(fontSize: 16, color: colorBlueLogo),
        ),
        Text(
          lPoints,
          style: Utilities.style(fontSize: 16, color: colorBlueLogo),
        ),
        Text(
          lXOF,
          style: Utilities.style(fontSize: 16, color: colorBlueLogo),
        ),
        Text(
          lLieu,
          style: Utilities.style(fontSize: 16, color: colorBlueLogo),
        ),
      ],
    );
  }

  static TableRow get headerParrainageTable {
    return TableRow(
      children: [
        Text(
          lTelephoneFilleul,
          style: Utilities.style(fontSize: 16, color: colorBlueLogo),
        ),
        Text(
          lEmailFilleul,
          style: Utilities.style(fontSize: 16, color: colorBlueLogo),
        ),
        Text(
          lAction,
          style: Utilities.style(fontSize: 16, color: colorBlueLogo),
        ),
      ],
    );
  }

  static incrementer(TextEditingController controller) {
    if (controller.text != null &&
        controller.text.isNotEmpty &&
        RegExp(r'^[0-9]+$').hasMatch(controller.text)) {
      controller.text = "${int.parse(controller.text) + 1}";
    } else {
      controller.text = "1";
    }
  }

  static decrementer(TextEditingController controller) {
    if (controller.text != null &&
        controller.text.isNotEmpty &&
        RegExp(r'^[0-9]+$').hasMatch(controller.text) &&
        int.parse(controller.text) > 1) {
      controller.text = "${int.parse(controller.text) - 1}";
    } else {
      controller.text = "1";
    }
  }

  static int getXOF({FideliteCarte? fideliteCarte}) {
    var xof = 0;
    if (fideliteCarte != null &&
        fideliteCarte.programmeFideliteCarteCorrespondanceNbrePoint != null &&
        fideliteCarte.programmeFideliteCarteNbrePointObtenu != null &&
        fideliteCarte.nbrePointActuelle != null) {
      xof = ((fideliteCarte.nbrePointActuelle! *
                  //fideliteCarte.programmeFideliteCartePourXAcheter!) /
                  fideliteCarte
                      .programmeFideliteCarteCorrespondanceNbrePoint!) /
              fideliteCarte.programmeFideliteCarteNbrePointObtenu!)
          .round() // arrondir
          .toInt();
    }
    return xof;
  }

  static int getNbreColPourAfficherTampon(
      {int nbreTampon = defaultNbreTampon,
      int nbrePallier = defaultNbrePallier}) {
    int nbreCol = 0;

    if ((nbreTampon % nbrePallier) > 0) {
      nbreCol = (nbreTampon ~/ nbrePallier) + 1;
    } else {
      nbreCol = (nbreTampon ~/ nbrePallier);
    }
    print("nbreCol : $nbreCol");

    return nbreCol;
  }

  static isNotBlank(String value) {
    return (value != null && value.isNotEmpty);
  }

  static String initialStringValueWidget(String initialValue,
          {String? defaultValue}) =>
      isNotBlank(initialValue) ? initialValue : (defaultValue ?? "");

  static setUserByFireUserGoogle({FireUser.User? fireUser, User? user}) {
    if (fireUser != null && user != null) {
      List<String>? tabNom = fireUser.displayName?.split(" ");
      if (tabNom != null) {
        user.nom = tabNom.last;
        user.prenoms = tabNom.first;
      }
      user.telephone = fireUser.phoneNumber;
      user.email = fireUser.email;
      user.telephoneGoogle = fireUser.phoneNumber;
      user.emailGoogle = fireUser.email;
      user.byFirebaseUser = true;
    }
  }

  static setUserByFireUserOld({FireUser.User? fireUser, User? user}) {
    if (fireUser != null && user != null) {
      //user = new User();
      List<String>? tabNom = fireUser.displayName?.split(" ");
      if (tabNom != null) {
        user.nom = tabNom.last;
        user.prenoms = tabNom.first;
      }
      user.telephone = fireUser.phoneNumber;
      user.email = fireUser.email;
      user.byFirebaseUser = true;
    }
  }
}
