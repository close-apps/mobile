import 'dart:io';

import 'package:url_launcher/url_launcher.dart';

class CommunicationService {
  void map({String homeLat = "37.3230", String homeLng = "-122.0312"}) => launch(
      "https://www.google.com/maps/search/?api=1&query=${homeLat},${homeLng}");
  void call(String number) => launch("tel:$number");
  void sendSms(String number) => launch("sms:$number");
  void sendEmail(String email) => launch("mailto:$email");
  void facebook(String facebook) => launch("fb://profile/$facebook");
  void twitter({String twitter = ""}) => launch("https://twitter.com/$twitter");

  void whatsapp(String number) => launch(url(number));

  String url(String phone) {
    if (Platform.isAndroid) {
      // add the [https]
      //return "https://wa.me/$phone/?text=${Uri.parse(message)}";
      return "https://wa.me/$phone";
      // new line
    } else {
      // add the [https]
      return "https://api.whatsapp.com/send?phone=$phone";
      // new line
    }
  }
}
