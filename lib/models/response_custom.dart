import 'package:json_annotation/json_annotation.dart';

part 'response_custom.g.dart';

@JsonSerializable()
class ResponseCustom {
  bool? hasError;

  int? count;
  dynamic status;

  List<dynamic>? items;

  factory ResponseCustom.fromJson(Map<String, dynamic> json) =>
      _$ResponseCustomFromJson(json);

  Map<String, dynamic> toJson() => _$ResponseCustomToJson(this);

  ResponseCustom({this.hasError, this.count, this.status, this.items});
}

/*
class ResponseCustom {

  bool hasError;

  int count ;
  dynamic status;

  List<dynamic> items;

  factory ResponseCustom.fromJson(Map<String, dynamic> json) => _$ResponseCustomFromJson(json);

  Map<String, dynamic> toJson() => _$ResponseCustomToJson(this);

  ResponseCustom({this.hasError, this.count, this.status, this.items});
}
*/
