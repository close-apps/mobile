import 'package:flutter/material.dart';

class Enseigne {
  final String? title;
  final String? description;
  final String? code;
  final List<String>? childrens;
  final IconData? icon;
  final logo;
  final bool? isSelect;
  final Color? colorLogo;
  final dynamic view;

  const Enseigne(
      {this.title,
      this.icon,
      this.description,
      this.logo,
      this.colorLogo,
      this.childrens,
      this.view,
      this.code,
      this.isSelect = false});
}
