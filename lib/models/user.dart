import 'package:json_annotation/json_annotation.dart';
//import 'package:to_string/to_string.dart';

part 'user.g.dart';

@JsonSerializable()
//@ToString()
class User {
  int? id;
  int? enseigneId;
  int? carteId;
  int? programmeFideliteCarteId;
  int? programmeFideliteTamponId;

  int? userId;

  bool? isActiveEnseigne;
  int? nbreEtoile;
  dynamic commentaire;

  dynamic villePointDeVente;
  dynamic communePointDeVente;
  dynamic programmeFideliteTamponLibelle;
  bool? byFirebaseUser;
  bool? isUserMobile;

  String? nom;
  String? prenoms;
  String? login;
  String? email;
  String? emailGoogle;
  String? emailFacebook;
  String? password;
  String? newPassword;
  String? phone;
  String? telephone;
  String? telephoneGoogle;
  String? telephoneFacebook;
  String? phoneParrain;
  String? ville;
  String? commune;
  String? dateNaissance;
  String? emailParrain;
  String? telephoneParrain;
  String? telephoneReceveur;
  String? emailReceveur;
  String? description;
  String? pointPartager;
  String? createdAt;

  String? actionLibelle;
  String? lieu;
  String? point;
  String? xof;
  String? dateAction;

  dynamic urlLogo;

  String? pointDeVenteNom;
  String? typeActionLibelle;
  //String nbrePointOperation;
  int? nbrePointOperation;
  int? sommeOperation;

  String? adresse;

  String? villeLibelle;
  String? communeLibelle;

  dynamic datasUser;

  dynamic programmeFideliteCartePourXAcheter;
  dynamic programmeFideliteCarteNbrePointObtenu;
  dynamic programmeFideliteCarteCorrespondanceNbrePoint;

  dynamic dataProgrammeFideliteTampon;
  dynamic dataProgrammeFideliteCarte;
  dynamic dataCarte;

  int? nbreTamponActuelle;
  int? nbreTamponPrecedent;
  int? nbrePointPrecedent;
  dynamic currentDateAction;
  dynamic currentDateActionParam;
  dynamic conditionsDutilisation;

  User({
    this.id,
    this.enseigneId,
    this.carteId,
    this.userId,
    this.nom,
    this.prenoms,
    this.login,
    this.byFirebaseUser,
    this.isUserMobile = true,
    this.email,
    this.emailFacebook,
    this.emailGoogle,
    this.password,
    this.newPassword,
    this.telephone,
    this.telephoneFacebook,
    this.telephoneGoogle,
    this.phone,
    this.phoneParrain,
    this.ville,
    this.commune,
    this.nbreEtoile,
    this.isActiveEnseigne,
    this.commentaire,
    this.dateNaissance,
    this.emailParrain,
    this.telephoneParrain,
    this.programmeFideliteCarteId,
    this.telephoneReceveur,
    this.emailReceveur,
    this.description,
    this.pointPartager,
    this.createdAt,
    this.dateAction,
    this.actionLibelle,
    this.lieu,
    this.point,
    this.xof,
    this.villeLibelle,
    this.communeLibelle,
    this.nbrePointOperation,
    this.sommeOperation,
    this.typeActionLibelle,
    this.pointDeVenteNom,
    this.datasUser,
    this.programmeFideliteCarteNbrePointObtenu,
    this.programmeFideliteCartePourXAcheter,
    this.programmeFideliteCarteCorrespondanceNbrePoint,
    this.programmeFideliteTamponId,
    this.adresse,
    this.dataCarte,
    this.dataProgrammeFideliteCarte,
    this.dataProgrammeFideliteTampon,
    this.nbrePointPrecedent,
    this.nbreTamponActuelle,
    this.nbreTamponPrecedent,
    this.currentDateAction,
    this.currentDateActionParam,
    this.conditionsDutilisation,
    this.urlLogo,
    this.communePointDeVente,
    this.programmeFideliteTamponLibelle,
    this.villePointDeVente,
  });

  /*
  @override
  String toString() {
    // TODO: implement toString
    return super.toString();
  }
  */

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);

  static List<User> fromJsons(List<dynamic> jsons) => _$UserFromJsons(jsons);

  @override
  String toString() {
    // [_$CatToString] is generated at `cat.g.dart`,
    // and it returns likes this:
    // "Cat{leg: Cat.leg, color: white, weight: 1.2, wings: null, _heart: warm, hasWings: false}"
    return _$UserToString(this);
  }
}
