import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

class Menu {
  final String? title;
  final String? code;
  final List<String>? childrens;
  final IconData? icon;
  final bool isSelect;
  final Color? color;
  final dynamic view;

  const Menu(
      {this.title,
      this.icon,
      this.color,
      this.childrens,
      this.view,
      this.code,
      this.isSelect = false});
}

@JsonSerializable()
class DataParam {
  final String? operator;
  final String? start;
  final String? end;

  const DataParam({
    this.operator,
    this.start,
    this.end,
  });

  Map<String, dynamic> toJson() => _$DataParamToJson(this);

  Map<String, dynamic> _$DataParamToJson(DataParam instance) =>
      <String, dynamic>{
        'operator': instance.operator,
        'start': instance.start,
        'end': instance.end,
      };
}
