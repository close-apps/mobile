// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response_custom.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResponseCustom _$ResponseCustomFromJson(Map<String, dynamic> json) {
  return ResponseCustom(
    hasError: json['hasError'],
    count: json['count'],
    status: json['status'],
    items: json['items'],
  );
}

Map<String, dynamic> _$ResponseCustomToJson(ResponseCustom instance) =>
    <String, dynamic>{
      'hasError': instance.hasError,
      'count': instance.count,
      'status': instance.status,
      'items': instance.items,
    };

ResponseCustom _$ResponseCustomFromJsonOld(Map<String, dynamic> json) {
  return ResponseCustom(
    hasError:
        json['hasError'] != null ? json['hasError'] as bool : json['hasError'],
    count: json['count'] != null ? json['count'] as int : json['count'],
    status: json['status'],
    items: json['items'] != null ? json['items'] as List : json['items'],
  );
}
