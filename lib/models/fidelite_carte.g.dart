// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fidelite_carte.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FideliteCarte _$FideliteCarteFromJson(Map<String, dynamic> json) {
  return FideliteCarte(
    id: json['id'],
    title: json['title'],
    description: json['description'],
    urlLogo: json['urlLogo'],
    programmeFideliteTamponLibelle: json['programmeFideliteTamponLibelle'],
    nom: json['nom'],
    libelle: json['libelle'],
    correspondanceNbrePoint: json['correspondanceNbrePoint'],
    dataEnseigne: json['dataEnseigne'],
    enseigneId: json['enseigneId'],
    enseigneNom: json['enseigneNom'],
    gain: json['gain'],
    isDesactived: json['isDesactived'],
    isDispoInAllPdv: json['isDispoInAllPdv'],
    isValeurMaxGain: json['isValeurMaxGain'],
    nbrePDV: json['nbrePDV'],
    programmeFideliteCarteId: json['programmeFideliteCarteId'],
    programmeFideliteTamponId: json['programmeFideliteTamponId'],
    programmeFideliteCarteLibelle: json['programmeFideliteCarteLibelle'],
    nbrePallier: json['nbrePallier'],
    nbrePointObtenu: json['nbrePointObtenu'],
    nbreTampon: json['nbreTampon'],
    nbreUtilisateur: json['nbreUtilisateur'],
    pointObtenuParFilleul: json['pointObtenuParFilleul'],
    pointObtenuParParrain: json['pointObtenuParParrain'],
    pourXAcheter: json['pourXAcheter'],
    valeurMaxGain: json['valeurMaxGain'],
    icon: json['icon'],
    enseigneFideliteCarte: json['enseigneFideliteCarte'],
    colorLogo: json['colorLogo'],
    enseigneCouleur: json['enseigneCouleur'],
    enseigneUrlLogo: json['enseigneUrlLogo'],
    view: json['view'],
    code: json['code'],
    isSelect: json['isSelect'],

    emailParrain: json['emailParrain'],
    telephoneParrain: json['telephoneParrain'],
    telephoneReceveur: json['telephoneReceveur'],
    emailReceveur: json['emailReceveur'],
    pointPartager: json['pointPartager'],
    createdAt: json['createdAt'],
    actionLibelle: json['actionLibelle'],
    dateAction: json['dateAction'],
    lieu: json['lieu'],
    xof: json['xof'],
    point: json['point'],
    communeLibelle: json['communeLibelle'],
    villeLibelle: json['villeLibelle'],
    nbrePointOperation: json['nbrePointOperation'],
    sommeOperation: json['sommeOperation'],
    //nbrePointOperation: json['nbrePointOperation'] ,
    typeActionLibelle: json['typeActionLibelle'],
    pointDeVenteNom: json['pointDeVenteNom'],
    datasUser: json['datasUser'],
    programmeFideliteCarteNbrePointObtenu:
        json['programmeFideliteCarteNbrePointObtenu'],
    programmeFideliteCartePourXAcheter:
        json['programmeFideliteCartePourXAcheter'],
    nbrePointActuelle: json['nbrePointActuelle'],
    programmeFideliteCarteCorrespondanceNbrePoint:
        json['programmeFideliteCarteCorrespondanceNbrePoint'],

    dataProgrammeFideliteCarte: json['dataProgrammeFideliteCarte'],
    dataProgrammeFideliteTampon: json['dataProgrammeFideliteTampon'],
    dataCarte: json['dataCarte'],

    nbrePointPrecedent: json['nbrePointPrecedent'],
    nbreTamponActuelle: json['nbreTamponActuelle'],
    nbreTamponPrecedent: json['nbreTamponPrecedent'],
    carteId: json['carteId'],
    currentDateAction: json['currentDateAction'],
    conditionsDutilisation: json['conditionsDutilisation'],
    villePointDeVente: json['villePointDeVente'],
    communePointDeVente: json['communePointDeVente'],
    longitude: json['longitude'],
    latitude: json['latitude'],
    isNotifed: json['isNotifed'],
    isLocked: json['isLocked'],
    unsubscribe: json['unsubscribe'],
    raisonLocked: json['raisonLocked'],
    raisonUnsubscribe: json['raisonUnsubscribe'],
    messageNotifed: json['messageNotifed'],
    isShowLastNotification: json['isShowLastNotification'],
  );
}

List<FideliteCarte> _$FideliteCarteFromJsons(List<dynamic> jsons) {
  return jsons.map((json) => _$FideliteCarteFromJson(json)).toList();
  // List<FideliteCarte> items = List();
  // for (var json in jsons) {
  //   items.add(_$FideliteCarteFromJson(json));
  // }
  // return items;
}
// List<FideliteCarte> _$FideliteCarteFromJsons(List<Map<String, dynamic>> jsons) {
//   List<FideliteCarte> items = List();
//   for (var json in jsons) {
//     items.add(_$FideliteCarteFromJson(json));
//   }
//   return items;
// }

Map<String, dynamic> _$FideliteCarteToJson(FideliteCarte instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'libelle': instance.libelle,
      'nom': instance.nom,
      'description': instance.description,
      'code': instance.code,
      'icon': instance.icon,
      'isSelect': instance.isSelect,
      'view': instance.view,
      'nbrePallier': instance.nbrePallier,
      'enseigneFideliteCarte': instance.enseigneFideliteCarte,
      'dataEnseigne': instance.dataEnseigne,
      'nbrePDV': instance.nbrePDV,
      'nbreUtilisateur': instance.nbreUtilisateur,
      'isDispoInAllPdv': instance.isDispoInAllPdv,
      'valeurMaxGain': instance.valeurMaxGain,
      'isValeurMaxGain': instance.isValeurMaxGain,
      'gain': instance.gain,
      'nbreTampon': instance.nbreTampon,
      'enseigneId': instance.enseigneId,
      'enseigneNom': instance.enseigneNom,
      'pourXAcheter': instance.pourXAcheter,
      'nbrePointObtenu': instance.nbrePointObtenu,
      'correspondanceNbrePoint': instance.correspondanceNbrePoint,
      'pointObtenuParParrain': instance.pointObtenuParParrain,
      'pointObtenuParFilleul': instance.pointObtenuParFilleul,
      'isDesactived': instance.isDesactived,
      'urlLogo': instance.urlLogo,
      'programmeFideliteTamponLibelle': instance.programmeFideliteTamponLibelle,
      'colorLogo': instance.colorLogo,
      'enseigneCouleur': instance.enseigneCouleur,
      'enseigneUrlLogo': instance.enseigneUrlLogo,
      'programmeFideliteCarteId': instance.programmeFideliteCarteId,
      'programmeFideliteCarteLibelle': instance.programmeFideliteCarteLibelle,
      'programmeFideliteTamponId': instance.programmeFideliteTamponId,
      'telephoneParrain': instance.telephoneParrain,
      'emailParrain': instance.emailParrain,
      'emailReceveur': instance.emailReceveur,
      'telephoneReceveur': instance.telephoneReceveur,
      'pointPartager': instance.pointPartager,
      'createdAt': instance.createdAt,
      'actionLibelle': instance.actionLibelle,
      'createdateActiondAt': instance.dateAction,
      'lieu': instance.lieu,
      'xof': instance.xof,
      'point': instance.point,
      'villeLibelle': instance.villeLibelle,
      'communeLibelle': instance.communeLibelle,
      'pointDeVenteNom': instance.pointDeVenteNom,
      'typeActionLibelle': instance.typeActionLibelle,
      'nbrePointOperation': instance.nbrePointOperation,
      'sommeOperation': instance.sommeOperation,
      'datasUser': instance.datasUser,
      'datasUprogrammeFideliteCarteNbrePointObtenuser':
          instance.programmeFideliteCarteNbrePointObtenu,
      'programmeFideliteCartePourXAcheter':
          instance.programmeFideliteCartePourXAcheter,
      'nbrePointActuelle': instance.nbrePointActuelle,
      'programmeFideliteCarteCorrespondanceNbrePoint':
          instance.programmeFideliteCarteCorrespondanceNbrePoint,
      'dataCarte': instance.dataCarte,
      'dataProgrammeFideliteCarte': instance.dataProgrammeFideliteCarte,
      'dataProgrammeFideliteTampon': instance.dataProgrammeFideliteTampon,
      'nbreTamponPrecedent': instance.nbreTamponPrecedent,
      'nbrePointPrecedent': instance.nbrePointPrecedent,
      'nbreTamponActuelle': instance.nbreTamponActuelle,
      'carteId': instance.carteId,
      'currentDateAction': instance.currentDateAction,
      'conditionsDutilisation': instance.conditionsDutilisation,
      'villePointDeVente': instance.villePointDeVente,
      'communePointDeVente': instance.communePointDeVente,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'isNotifed': instance.isNotifed,
      'isLocked': instance.isLocked,
      'unsubscribe': instance.unsubscribe,
      'messageNotifed': instance.messageNotifed,
      'raisonLocked': instance.raisonLocked,
      'raisonUnsubscribe': instance.raisonUnsubscribe,
      'isShowLastNotification': instance.isShowLastNotification,
    };

// **************************************************************************
// ToStringGenerator
// **************************************************************************

String _$FideliteCarteToString(FideliteCarte o) {
  return """FideliteCarte{title: ${o.title}, libelle: ${o.libelle}, nom: ${o.nom}, description: ${o.description}, code: ${o.code}, icon: ${o.icon}, isSelect: ${o.isSelect}, view: ${o.view}, nbrePallier: ${o.nbrePallier}, enseigneFideliteCarte: ${o.enseigneFideliteCarte}, dataEnseigne: ${o.dataEnseigne}, nbrePDV: ${o.nbrePDV}, nbreUtilisateur: ${o.nbreUtilisateur}, isDispoInAllPdv: ${o.isDispoInAllPdv}, valeurMaxGain: ${o.valeurMaxGain}, isValeurMaxGain: ${o.isValeurMaxGain}, gain: ${o.gain}, nbreTampon: ${o.nbreTampon}, enseigneId: ${o.enseigneId}, enseigneNom: ${o.enseigneNom}, pourXAcheter: ${o.pourXAcheter}, nbrePointObtenu: ${o.nbrePointObtenu}, correspondanceNbrePoint: ${o.correspondanceNbrePoint}, pointObtenuParParrain: ${o.pointObtenuParParrain}, pointObtenuParFilleul: ${o.pointObtenuParFilleul}, isDesactived: ${o.isDesactived}, urlLogo: ${o.urlLogo}, programmeFideliteTamponLibelle: ${o.programmeFideliteTamponLibelle}}""";
}

FideliteCarte _$FideliteCarteFromJsonOld(Map<String, dynamic> json) {
  return FideliteCarte(
    id: json['id'] as int,
    title: json['title'] as String,
    description: json['description'] as String,
    urlLogo: json['urlLogo'],
    programmeFideliteTamponLibelle: json['programmeFideliteTamponLibelle'],
    nom: json['nom'] as String,
    libelle: json['libelle'] as String,
    correspondanceNbrePoint: json['correspondanceNbrePoint'],
    dataEnseigne: json['dataEnseigne'],
    enseigneId: json['enseigneId'] as int,
    enseigneNom: json['enseigneNom'],
    gain: json['gain'],
    isDesactived: json['isDesactived'],
    isDispoInAllPdv: json['isDispoInAllPdv'] as bool,
    isValeurMaxGain: json['isValeurMaxGain'] as bool,
    nbrePDV: json['nbrePDV'] as int,
    programmeFideliteCarteId: json['programmeFideliteCarteId'] as int,
    programmeFideliteTamponId: json['programmeFideliteTamponId'] as int,
    programmeFideliteCarteLibelle: json['programmeFideliteCarteLibelle'],
    nbrePallier: json['nbrePallier'],
    nbrePointObtenu: json['nbrePointObtenu'],
    nbreTampon: json['nbreTampon'],
    nbreUtilisateur: json['nbreUtilisateur'] as int,
    pointObtenuParFilleul: json['pointObtenuParFilleul'],
    pointObtenuParParrain: json['pointObtenuParParrain'],
    pourXAcheter: json['pourXAcheter'],
    valeurMaxGain: json['valeurMaxGain'],
    icon: json['icon'],
    enseigneFideliteCarte: json['enseigneFideliteCarte'],
    colorLogo: json['colorLogo'],
    enseigneCouleur: json['enseigneCouleur'],
    enseigneUrlLogo: json['enseigneUrlLogo'],
    view: json['view'],
    code: json['code'] as String,
    isSelect: json['isSelect'] as bool,

    emailParrain: json['emailParrain'] as String,
    telephoneParrain: json['telephoneParrain'] as String,
    telephoneReceveur: json['telephoneReceveur'] as String,
    emailReceveur: json['emailReceveur'] as String,
    pointPartager: json['pointPartager'] as String,
    createdAt: json['createdAt'] as String,
    actionLibelle: json['actionLibelle'] as String,
    dateAction: json['dateAction'] as String,
    lieu: json['lieu'] as String,
    xof: json['xof'] as String,
    point: json['point'] as String,
    communeLibelle: json['communeLibelle'] as String,
    villeLibelle: json['villeLibelle'] as String,
    nbrePointOperation: json['nbrePointOperation'] as int,
    sommeOperation: json['sommeOperation'] as int,
    //nbrePointOperation: json['nbrePointOperation'] as String,
    typeActionLibelle: json['typeActionLibelle'] as String,
    pointDeVenteNom: json['pointDeVenteNom'] as String,
    datasUser: json['datasUser'],
    programmeFideliteCarteNbrePointObtenu:
        json['programmeFideliteCarteNbrePointObtenu'] as int,
    programmeFideliteCartePourXAcheter:
        json['programmeFideliteCartePourXAcheter'] as int,
    nbrePointActuelle: json['nbrePointActuelle'] as int,
    programmeFideliteCarteCorrespondanceNbrePoint:
        json['programmeFideliteCarteCorrespondanceNbrePoint'] as int,

    dataProgrammeFideliteCarte: json['dataProgrammeFideliteCarte'],
    dataProgrammeFideliteTampon: json['dataProgrammeFideliteTampon'],
    dataCarte: json['dataCarte'],

    nbrePointPrecedent: json['nbrePointPrecedent'] as int,
    nbreTamponActuelle: json['nbreTamponActuelle'] as int,
    nbreTamponPrecedent: json['nbreTamponPrecedent'] as int,
    carteId: json['carteId'] as int,
    currentDateAction: json['currentDateAction'],
    conditionsDutilisation: json['conditionsDutilisation'],
    villePointDeVente: json['villePointDeVente'],
    communePointDeVente: json['communePointDeVente'],
    longitude: json['longitude'],
    latitude: json['latitude'],
  );
}
