import 'package:closeapps/models/enseigne.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
//import 'package:to_string/to_string.dart';

part 'fidelite_carte.g.dart';

@JsonSerializable()
//@ToString()
class FideliteCarte {
  final int? id;
  final String? title;
  final String? libelle;
  final String? nom;
  final String? description;
  final String? code;
  final dynamic icon;
  final bool? isSelect;
  final bool? isLocked;
  final bool? isNotifed;
  final bool? unsubscribe;

  final dynamic messageNotifed;
  final dynamic raisonLocked;
  final dynamic raisonUnsubscribe;

  final dynamic view;
  final dynamic nbrePallier;
  final dynamic enseigneFideliteCarte;
  final dynamic dataEnseigne;
  final int? nbrePDV;
  final int? nbreUtilisateur;
  final bool? isDispoInAllPdv;
  final dynamic valeurMaxGain;
  
  final bool? isShowLastNotification;
  final bool? isValeurMaxGain;
  final dynamic gain;
  final dynamic nbreTampon;
  final int? enseigneId;
  final dynamic enseigneNom;
  final dynamic pourXAcheter;
  final dynamic nbrePointObtenu;
  final dynamic correspondanceNbrePoint;
  final dynamic pointObtenuParParrain;
  final dynamic pointObtenuParFilleul;
  final dynamic isDesactived;
  final dynamic urlLogo;
  final dynamic programmeFideliteTamponLibelle;
  final dynamic programmeFideliteCarteLibelle;
  final dynamic colorLogo;
  final dynamic enseigneCouleur;
  final dynamic enseigneUrlLogo;
  final double? longitude;
  final double? latitude;

  final dynamic villePointDeVente;
  final dynamic communePointDeVente;

  final int? programmeFideliteCarteId;
  final int? programmeFideliteTamponId;

  final String? emailParrain;
  final String? telephoneParrain;
  final String? telephoneReceveur;
  final String? emailReceveur;
  final String? pointPartager;
  final String? createdAt;

  final String? actionLibelle;
  final String? lieu;
  final String? point;
  final String? xof;
  final String? dateAction;

  final String? pointDeVenteNom;
  final String? typeActionLibelle;
  //String nbrePointOperation;
  final int? nbrePointOperation;
  final int? sommeOperation;

  final String? villeLibelle;
  final String? communeLibelle;

  final dynamic datasUser;

  final int? programmeFideliteCartePourXAcheter;
  final int? programmeFideliteCarteNbrePointObtenu;
  final int? programmeFideliteCarteCorrespondanceNbrePoint;

  final int? nbrePointActuelle;

  final dynamic dataProgrammeFideliteTampon;
  final dynamic dataProgrammeFideliteCarte;
  final dynamic dataCarte;

  final int? nbreTamponActuelle;
  final int? nbreTamponPrecedent;
  final int? nbrePointPrecedent;
  final int? carteId;
  final dynamic currentDateAction;
  final dynamic conditionsDutilisation;

  const FideliteCarte({
    this.title,
    this.id,
    this.description,
    this.urlLogo,
    this.programmeFideliteTamponLibelle,
    this.nom,
    this.libelle,
    this.correspondanceNbrePoint,
    this.dataEnseigne,
    this.longitude,
    this.latitude,
    this.enseigneId,
    this.enseigneNom,
    this.gain,
    this.isDesactived,
    this.isDispoInAllPdv,
    this.isValeurMaxGain,
    this.nbrePDV,
    this.nbrePallier,
    this.nbrePointObtenu,
    this.nbreTampon,
    this.nbreUtilisateur,
    this.pointObtenuParFilleul,
    this.pointObtenuParParrain,
    this.pourXAcheter,
    this.valeurMaxGain,
    this.icon,
    this.enseigneFideliteCarte,
    this.view,
    this.colorLogo,
    this.enseigneCouleur,
    this.enseigneUrlLogo,
    this.programmeFideliteTamponId,
    this.programmeFideliteCarteId,
    this.programmeFideliteCarteLibelle,
    this.code,
    this.isSelect = false,
    this.emailParrain,
    this.telephoneParrain,
    this.telephoneReceveur,
    this.emailReceveur,
    this.pointPartager,
    this.createdAt,
    this.dateAction,
    this.actionLibelle,
    this.lieu,
    this.point,
    this.xof,
    this.villeLibelle,
    this.communeLibelle,
    this.nbrePointOperation,
    this.sommeOperation,
    this.typeActionLibelle,
    this.pointDeVenteNom,
    this.datasUser,
    this.programmeFideliteCarteNbrePointObtenu,
    this.programmeFideliteCartePourXAcheter,
    this.nbrePointActuelle,
    this.programmeFideliteCarteCorrespondanceNbrePoint,
    this.dataCarte,
    this.dataProgrammeFideliteCarte,
    this.dataProgrammeFideliteTampon,
    this.nbrePointPrecedent,
    this.nbreTamponActuelle,
    this.nbreTamponPrecedent,
    this.carteId,
    this.currentDateAction,
    this.conditionsDutilisation,
    this.communePointDeVente,
    this.villePointDeVente,
    this.isLocked,
    this.isNotifed,
    this.unsubscribe,
    this.raisonUnsubscribe,
    this.raisonLocked,
    this.messageNotifed,
    this.isShowLastNotification
  });

  factory FideliteCarte.fromJson(Map<String, dynamic> json) =>
      _$FideliteCarteFromJson(json);

  // static List<FideliteCarte> fromJsons(List<Map<String, dynamic>> jsons) =>
  //     _$FideliteCarteFromJsons(jsons);

  static List<FideliteCarte> fromJsons(List<dynamic> jsons) =>
      _$FideliteCarteFromJsons(jsons);

  Map<String, dynamic> toJson() => _$FideliteCarteToJson(this);

  @override
  String toString() {
    // [_$CatToString] is generated at `cat.g.dart`,
    // and it returns likes this:
    // "Cat{leg: Cat.leg, color: white, weight: 1.2, wings: null, _heart: warm, hasWings: false}"
    return _$FideliteCarteToString(this);
  }
}
