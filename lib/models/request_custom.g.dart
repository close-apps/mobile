// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'request_custom.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RequestCustom _$RequestCustomFromJson(Map<String, dynamic> json) {
  return RequestCustom(
    isAnd: json['isAnd'],
    data: json['data'],
    user: json['user'],
    datas: json['datas'],
  );
}

Map<String, dynamic> _$RequestCustomToJson(RequestCustom instance) =>
    <String, dynamic>{
      'isAnd': instance.isAnd,
      'data': instance.data,
      'user': instance.user,
      'datas': instance.datas,
    };

RequestCustom _$RequestCustomFromJsonOld(Map<String, dynamic> json) {
  return RequestCustom(
    isAnd: json['isAnd'] as bool,
    data: json['data'],
    user: json['user'] as int,
    datas: json['datas'] as List,
  );
}
