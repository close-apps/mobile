import 'dart:convert';

import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/preferences_util.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/fidelite_carte.dart';
import 'package:closeapps/models/request_custom.dart';
import 'package:closeapps/models/response_custom.dart';
import 'package:closeapps/models/user.dart';
import 'package:closeapps/views/menu_pdf_tampon.dart';
import 'package:closeapps/views/test_qr_code.dart';
import 'package:flutter/material.dart';
import 'package:qr/qr.dart';
//import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:qr_flutter/qr_flutter.dart';

class ScannerTampon extends StatefulWidget {
  ScannerTampon(
      {Key? key,
      this.souscriptionPDFTampon,
      this.idCarte,
      this.somme,
      required this.programmeFideliteTamponId,
      required this.souscriptionProgrammeFideliteTamponId,
      this.qrText,
      this.typeActionCode})
      : super(key: key);
  String? qrText;
  String? typeActionCode;
  int? idCarte;
  int programmeFideliteTamponId;
  int souscriptionProgrammeFideliteTamponId;
  int? somme;
  FideliteCarte? souscriptionPDFTampon;

  @override
  _ScannerTamponState createState() => _ScannerTamponState();
}

class _ScannerTamponState extends State<ScannerTampon> {
  var qrTextLocal = '';
  bool _waiting = true;
  bool isScanner = false;

  bool _isKeptOn = false;
  double _brightness = 1.0;

  // final GlobalKey qrKey = GlobalKey(
  //   debugLabel: 'QR',
  // );
  // QRViewController controller;
  // final qrCode = new QrCode(4, QrErrorCorrectLevel.L);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // getQrText();
    // initPlatformState();
    // verifierScan();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    getQrText();
    initPlatformState();
    verifierScan();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    isScanner = true;
    //free brightness (it works only on Android):
    Utilities.resetBrightness();
  }

  @override
  Widget build(BuildContext context) {
    // qrCode.addData("Hello, world in QR form!");
    // qrCode.make();
    // print(qrCode.toString());
    return Scaffold(
      appBar: AppBar(
          title: Text(
        (widget.souscriptionPDFTampon != null &&
                    widget.souscriptionPDFTampon!.libelle != null
                ? widget.souscriptionPDFTampon!.libelle!
                : "CLOSE APP") +
            sautDelLine +
            lScannerMaCarte,
      )),
      body: Center(
        child: (_waiting)
            ? CircularProgressIndicator()
            : QrImage(
                data: widget.qrText ?? "RAS",
                size: 250.0,
              ),
        // new Column(children: <Widget>[
        //     new Row(
        //         mainAxisAlignment: MainAxisAlignment.center,
        //         children: <Widget>[
        //           new Text("Screen is kept on ? "),
        //           new Checkbox(
        //               value: _isKeptOn,
        //               onChanged: (bool b) {
        //                 Screen.keepOn(b);
        //                 setState(() {
        //                   _isKeptOn = b;
        //                 });
        //               })
        //         ]),
        //     new Text("Brightness :"),
        //     new Slider(
        //         value: _brightness,
        //         onChanged: (double b) {
        //           setState(() {
        //             print("b : $b");
        //             _brightness = b;
        //           });
        //           Screen.setBrightness(b);
        //         })
        //   ])

        // child: QRView(
        //   key: qrKey,
        //   onQRViewCreated: _onQRViewCreated,
        //   overlay: QrScannerOverlayShape(
        //     borderColor: Colors.red,
        //     borderRadius: 10,
        //     borderLength: 30,
        //     borderWidth: 10,
        //     cutOutSize: 300,
        //   ),
        // ),
      ),
    );
  }

  // void _onQRViewCreated(QRViewController qRViewController) {
  //   qrCode.addData("Hello, world in QR form!");
  //   qrCode.make();

  //   controller = qRViewController;
  //   controller.scannedDataStream.listen((scanData) {
  //     print("qrText : $qrText");
  //     setState(() {
  //       qrText = scanData;
  //     });
  //   });
  // }

  getQrText() async {
    //dynamic carteId = await Utilities.carteIdUserConnected;
    //dynamic userId = await Utilities.idUserConnected;
    dynamic carte = await PreferencesUtil.instance.getInt(carteId);

    //var qrText = "";

    qrTextLocal = '$carte' + espace + '${widget.programmeFideliteTamponId}';

    if (widget.typeActionCode != null) {
      qrTextLocal += espace + widget.typeActionCode!;
    }
    if (widget.somme != null) {
      qrTextLocal += espace + '${widget.somme}';
    }

    widget.qrText = qrTextLocal;

    print("widget.qrText: ${widget.qrText}");
    setState(() {
      _waiting = false;
    });

    // if (widget.typeActionCode != null) {
    //   qrTextLocal += espace + widget.typeActionCode;
    // }

    //return qrTextLocal;
  }

  // verification method if new user or old
  verifierScan() async {
    // connexion in dataBase or networkCall

    Utilities.begin("verifierScan");
    // faire le get
    User data = User();

    data.currentDateAction = formatter.format(DateTime.now());
    //data.currentDateAction = "04/11/2020 08:17:00";

    print("data.currentDateAction : ${data.currentDateAction}");
    data.currentDateActionParam = operatorSuperieurA;

    print("data.currentDateActionParam : ${data.currentDateActionParam}");

    //data.carteId = widget.idCarte;
    data.carteId = await PreferencesUtil.instance.getInt(carteId);
    data.programmeFideliteTamponId = widget.programmeFideliteTamponId;

    while (!isScanner) {
      await Future.delayed(Duration(seconds: nbreSecondAnimation), () {});

      // data.carteId = 1;
      // data.programmeFideliteTamponId = 1;

      RequestCustom request = RequestCustom();
      request.user = await PreferencesUtil.instance.getInt(id);
      request.data = data;

      ResponseCustom response = await baseApi.appelApi(
        endpointApi: historiqueCarteProgrammeFideliteTampon + getByCriteria,
        request: request,
      );

      if (response != null &&
          response.hasError != null &&
          !response.hasError! &&
          response.items != null) {
        isScanner = true;
        Utilities.getToast(succes);

        Navigator.of(context).pop(); // lever le scan
        Navigator.of(context).pop(); // lever l'ancien menu
        Utilities.navigatorPush(
            context: context,
            view: MenuPdfTampon(
                souscriptionPDFTamponId:
                    widget.souscriptionProgrammeFideliteTamponId));
      } else {
        //Utilities.messageApi(response: response);
      }
    }
    //return true;
  }

  initPlatformState() async {
    // Get the current brightness:

    await Utilities.setBrightness(1);

    //double brightness = await FlutterScreen.brightness;

    // Set the brightness:
    // FlutterScreen.setBrightness(7.5);

    // Check if the screen is kept on:
    //bool isKeptOn = await FlutterScreen.isKeptOn;

    // Prevent screen from going into sleep mode:
    //FlutterScreen.keepOn(true);

    setState(() {
      //print("keptOn : $isKeptOn");
      //print("brightness : $brightness");
      // _isKeptOn = isKeptOn;
      //_brightness = brightness;
      //Screen.keepOn(true);
      //Screen.setBrightness(700.0);
    });
  }
}
