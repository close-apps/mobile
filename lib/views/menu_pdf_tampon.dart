import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/helpers/communications_service.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/service_locator.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/fidelite_carte.dart';
import 'package:closeapps/models/response_custom.dart';
import 'package:closeapps/models/user.dart';
import 'package:closeapps/views/contact_enseigne.dart';
import 'package:closeapps/views/point_de_vente_tampon.dart';
import 'package:closeapps/views/scanner_tampon.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';

class MenuPdfTampon extends StatefulWidget {
  MenuPdfTampon(
      {Key? key,
      required this.souscriptionPDFTamponId,
      this.messageNotifed,
      this.isShowLastNotification})
      : super(key: key);

  int souscriptionPDFTamponId;
  String? messageNotifed;
  bool? isShowLastNotification;
  @override
  _MenuPdfTamponState createState() => _MenuPdfTamponState();
}

class _MenuPdfTamponState extends State<MenuPdfTampon> {
  final CommunicationService _communicationService =
      locator<CommunicationService>();

  final String number = "+22558406013";
  final String email = "dancamdev@example.com";

  FideliteCarte? programmeFideliteTampon;
  FideliteCarte? souscriptionPDFTampon;

  List<Widget> tampons = [];
  int nbreTamponActuelle = 1;
  bool isWaitingData = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    Utilities.begin("didChangeDependencies");
    initData();
  }

  @override
  Widget build(BuildContext context) {
    Utilities.begin("build");

    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

    return Scaffold(
      appBar: AppBar(
          title: Text(
        souscriptionPDFTampon != null && souscriptionPDFTampon!.libelle != null
            ? souscriptionPDFTampon!.libelle!
            //: "CLOSE APP",
            : "...",
      )),
      body: isWaitingData
          ? Center(child: CircularProgressIndicator())
          : SingleChildScrollView(
              child: Center(
                child: Column(
                  children: [
                    IgnorePointer(
                      child: GridView.count(
                        //childAspectRatio: 2,
                        //reverse: true,

                        crossAxisCount: (programmeFideliteTampon != null)
                            ? Utilities.getNbreColPourAfficherTampon(
                                nbrePallier:
                                    programmeFideliteTampon!.nbrePallier ??
                                        defaultNbrePallier,
                                nbreTampon:
                                    programmeFideliteTampon!.nbreTampon ??
                                        defaultNbreTampon)
                            : 0,
                        //children: tampons,
                        children: List.generate(
                            programmeFideliteTampon!.nbreTampon ??
                                defaultNbreTampon, (index) {
                          --nbreTamponActuelle;
                          //nbreTamponActuelle--;
                          return IconButton(
                              icon: Icon(
                                (souscriptionPDFTampon != null &&
                                        nbreTamponActuelle > 0)
                                    // ? Icons.radio_button_checked
                                    //? Icons.check_circle_outline
                                    ? Icons.fact_check
                                    //: Icons.radio_button_off,
                                    : Icons.check_box_outline_blank,
                                size: 40.0,
                              ),
                              onPressed: () {});
                        }),
                        shrinkWrap: true,
                      ),
                    ),

                    IconButton(
                        icon: Icon(
                          Icons.qr_code_scanner,
                          size: 40.0,
                        ),
                        onPressed: () {
                          Utilities.navigatorPush(
                              context: context,
                              view: ScannerTampon(
                                souscriptionProgrammeFideliteTamponId:
                                    widget.souscriptionPDFTamponId,
                                souscriptionPDFTampon: souscriptionPDFTampon,
                                idCarte: souscriptionPDFTampon!.carteId ?? null,
                                programmeFideliteTamponId:
                                    souscriptionPDFTampon!
                                        .programmeFideliteTamponId!,
                              ));
                        }),
                    // Table(
                    //   children: [
                    //     TableRow(
                    //         children:
                    //             List.generate(3, (index) => Icon(Icons.fact_check)))
                    //   ],
                    // ),
                    // Table(
                    //     children: List.generate(
                    //   3,
                    //   (index) => TableRow(
                    //       children:
                    //           List.generate(3, (index) => Icon(Icons.fact_check))),
                    // )),
                    // child: GridView(
                    //   gridDelegate: null,
                    //   children: tampons,
                    // ),
                    //),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Utilities.getButtonSubmit(
                              libelle: lPDV,
                              onPressed: () {
                                Utilities.navigatorPush(
                                    context: context,
                                    view: PointDeVenteTampon(
                                      pdfTampon: souscriptionPDFTampon,
                                      idCarte: souscriptionPDFTampon!.carteId,
                                      programmeFideliteTamponId:
                                          souscriptionPDFTampon!
                                                  .programmeFideliteTamponId ??
                                              0,
                                    ));
                              }),
                          Utilities.getButtonSubmit(
                              libelle: lContact,
                              onPressed: () {
                                Utilities.navigatorPush(
                                    context: context,
                                    view: ContactEnseigne(
                                      programmeFideliteTampon:
                                          FideliteCarte.fromJson(
                                              souscriptionPDFTampon!
                                                  .dataProgrammeFideliteTampon),
                                    ));
                              }),
                        ],
                      ),
                    ),
                    Utilities.getDefaultSizeBoxWithHeight(),
                    InkWell(
                      onTap: () {
                        Utilities.showDialogCustom(
                            context: context,
                            titre: lConditionsDutilisation,
                            //contenu: lorem(paragraphs: 2, words: 100),
                            contenu: (programmeFideliteTampon != null &&
                                    programmeFideliteTampon!
                                            .conditionsDutilisation !=
                                        null)
                                ? programmeFideliteTampon!
                                    .conditionsDutilisation
                                : lDefaultConditionsDutilisation,
                            libelleRightFuction: lFermer,
                            rightFuction: () {
                              Navigator.pop(context);
                              Utilities.getToast(lLireConditions);
                            },
                            isHtmlContent: true);
                      },
                      child: Center(
                        child: Text(
                          lConditionsDutilisation,
                          style: Utilities.style(),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
    );
  }

  initData() async {
    User data = User();

    if (widget.messageNotifed != null &&
        (widget.isShowLastNotification == null ||
            !widget.isShowLastNotification!)) {
      Utilities.alertDialogCustom(
          context: context,
          title: messageNotificationDelitPDF,
          content: widget.messageNotifed!,
          libelleRightFuction: lFermer,
          libelleLeftFuction: lNePlusMeRappeler,
          afficherToast: false,
          leftFuction: nePlusMeRappelerNotification);
    }

    data.id = widget.souscriptionPDFTamponId;
    ResponseCustom response = await baseApi.newAppelApi(
      data: data,
      endpointApi: souscriptionProgrammeFideliteTampon + getByCriteria,
    );

    if (response != null &&
        response.hasError != null &&
        !response.hasError! &&
        response.items != null &&
        response.items!.length > 0) {
      souscriptionPDFTampon = FideliteCarte.fromJson(response.items![0]);

      if (souscriptionPDFTampon != null &&
          souscriptionPDFTampon!.dataProgrammeFideliteTampon != null) {
        programmeFideliteTampon = FideliteCarte.fromJson(
            souscriptionPDFTampon!.dataProgrammeFideliteTampon);

        if (souscriptionPDFTampon!.nbreTamponActuelle != null) {
          nbreTamponActuelle += souscriptionPDFTampon!.nbreTamponActuelle!;
        }
      }
    }
    buildTampon();

    isWaitingData = false;
    setState(() {});
  }

  nePlusMeRappelerNotification() async {
    Navigator.pop(context);
    FideliteCarte data = FideliteCarte(
        id: widget.souscriptionPDFTamponId, isShowLastNotification: true);

    baseApi.newAppelApi(
        datas: [data],
        endpointApi:
            souscriptionProgrammeFideliteTampon + update).then((reponse) {
      print("succes nePlusMeRappelerNotification");
    }).catchError((onError) {
      print("error nePlusMeRappelerNotification");
    });
  }

  buildTampon() {
    int nbreTamponActuelle = souscriptionPDFTampon!.nbreTamponActuelle ?? 0;
    for (var i = 0; i < programmeFideliteTampon!.nbreTampon; i++) {
      tampons.add(IconButton(
          icon: Icon(
            (souscriptionPDFTampon != null && nbreTamponActuelle > 0)
                // ? Icons.radio_button_checked
                //? Icons.check_circle_outline
                ? Icons.fact_check
                //: Icons.radio_button_off,
                : Icons.check_box_outline_blank,
            size: 40.0,
          ),
          onPressed: () {}));
      --nbreTamponActuelle;
    }
  }

  test() {
    _openUrl("tel:+22558406013");
  }

  Future<void> _openUrl(String url) async {
    //if (await canLaunch(url)) {
    await launch(url);
    // } else {
    //   throw 'Could not launch $url';
    // }
  }

  // @override
  // // TODO: implement mounted
  // bool get mounted => super.mounted;
  // @override
  // void dispose() {
  //   // TODO: implement dispose
  //   super.dispose();
  //   Utilities.begin("dispose");
  // }

  // @override
  // void didUpdateWidget(covariant MenuPdfTampon oldWidget) {
  //   // TODO: implement didUpdateWidget
  //   super.didUpdateWidget(oldWidget);
  //   Utilities.begin("didUpdateWidget");
  // }

  // @override
  // void reassemble() {
  //   super.reassemble();
  //   Utilities.begin("reassemble");
  // }

  // @override
  // void deactivate() {
  //   // TODO: implement deactivate
  //   super.deactivate();
  //   Utilities.begin("deactivate");
  // }

}
