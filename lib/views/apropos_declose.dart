import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

class AproposeDeClose extends StatefulWidget {
  AproposeDeClose({Key? key}) : super(key: key);

  @override
  _AproposeDeCloseState createState() => _AproposeDeCloseState();
}

class _AproposeDeCloseState extends State<AproposeDeClose> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //appBar: AppBar(
        title: Text(lAProposDeClose),
      ),
      body: SingleChildScrollView(
        child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(children: [
              Image.asset(logoBlue),
              Utilities.getDefaultSizeBoxWithHeight(),
              Html(data: defaultContentAProposDeClose),
            ])),
      ),
    );
  }
}
