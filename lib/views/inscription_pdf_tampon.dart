import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/preferences_util.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/fidelite_carte.dart';
import 'package:closeapps/models/request_custom.dart';
import 'package:closeapps/models/response_custom.dart';
import 'package:closeapps/models/user.dart';
import 'package:closeapps/views/home.dart';
import 'package:closeapps/views/inscription_tampon_ok.dart';
import 'package:closeapps/views/menu_pdf_tampon.dart';
import 'package:flutter/material.dart';

class InscriptionPDFTampon extends StatefulWidget {
  FideliteCarte? tampon;
  InscriptionPDFTampon({Key? key, this.tampon}) : super(key: key);

  @override
  _InscriptionPDFTamponState createState() => _InscriptionPDFTamponState();
}

class _InscriptionPDFTamponState extends State<InscriptionPDFTampon> {
  bool _isDisable = false;

  bool _isSuscribe = true;
  bool _isSelected = false;
  User _user = new User();
  User? dataEnseigne;
  @override
  Widget build(BuildContext context) {
    Utilities.begin("InscriptionPDFTampon/build");

    if (widget.tampon != null && widget.tampon!.dataEnseigne != null) {
      dataEnseigne = User.fromJson(widget.tampon!.dataEnseigne);
    }
    return Scaffold(
        body: CustomScrollView(
      shrinkWrap: true,
      slivers: <Widget>[
        SliverAppBar(
          backgroundColor:
              widget.tampon != null && widget.tampon!.enseigneCouleur != null
                  ? Color(widget.tampon!.enseigneCouleur)
                  : colorBlueLogo,
          expandedHeight: expandedHeight,
          floating: false,
          pinned: true,

          /*
          title: Text(
            widget.carte != null ? widget.carte.title : "Aucune Libelle",
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: Utilities.style(
              color: colorWitheLogo,
            ),
          ),*/

          //textTheme: TextTheme(headline1:Utilities.style() ) ,
          flexibleSpace: FlexibleSpaceBar(
            collapseMode: CollapseMode.none,
            background:
                (widget.tampon != null && widget.tampon!.urlLogo != null)
                    ? Image.network(
                        widget.tampon!.urlLogo,
                        //widget.tampon.urlLogo,

                        fit: BoxFit.fill,
                        //width: double.infinity,
                        //height: double.infinity,
                      )
                    : Image.asset(
                        logoBlue,
                        fit: BoxFit.fill,
                        // width: double.infinity,
                        // height: double.infinity,
                      ),
            // Image.asset(
            //   widget.tampon != null && widget.tampon.urlLogo != null
            //       ? widget.tampon.urlLogo
            //       : logoBlue,
            //   fit: BoxFit.fill,
            //   //width: double.infinity,
            //   //height: expandedHeight,
            // ),
            title: Text(
              widget.tampon != null && widget.tampon!.libelle != null
                  ? widget.tampon!.libelle!
                  : "Aucun Libelle",
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: Utilities.style(
                color: colorWitheLogo,
                //fontSize: 20
              ),
            ),
          ),
        ),
        SliverFillRemaining(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            //mainAxisSize: MainAxisSize.max,
            //verticalDirection: VerticalDirection.down,
            //crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text((dataEnseigne != null && dataEnseigne!.nom != null)
                  ? dataEnseigne!.nom!
                  : ""),
              Text((dataEnseigne != null && dataEnseigne!.adresse != null)
                  ? dataEnseigne!.adresse!
                  : dataEnseigne!.telephone!),
              ListTile(
                  title: Text(widget.tampon!.libelle!),
                  subtitle: Text(
                    widget.tampon!.description != null
                        ? widget.tampon!.description!
                        : "La description du programme ...",
                    maxLines: maxLinesDescriptionPDFTampon,
                    overflow: TextOverflow.ellipsis,
                  ),
                  trailing: (_isSuscribe)
                      ? IconButton(
                          icon: Icon(Icons.radio_button_checked),
                          onPressed: null,
                        )
                      : IconButton(
                          icon: Icon((_isSelected)
                              ? Icons.radio_button_checked
                              : Icons.radio_button_off),
                          onPressed: () {
                            _isSelected = !_isSelected;
                            setState(() {});
                          },
                        )),
              // Utilities.getButtonSubmit(
              //     libelle: lSinscrire,
              //     isDisable: _isDisable,
              //     onPressed: _submit)
              InkWell(
                  splashColor: colorRedLogo,
                  onTap: () {
                    validerCondition(context: context, model: widget.tampon);
                  },
                  child: Container(
                    color: colorRedLogo,
                    padding: EdgeInsets.all(12.0),
                    //child: Text('Flat Button'),
                    child: Text(
                      lSinscrire,
                      style: Utilities.style(color: Colors.white),
                    ),
                  )

                  /*
                      RaisedButton(
                    color: colorRedLogo,
                    textColor: Colors.white,
                    //color: isDisable ? null : color,
                    //textColor: isDisable ? Colors.black : textColor,
                    //disabledColor: ConstApp.color_button_disable,
                    disabledColor: Colors.grey[300],
                    disabledTextColor: Colors.black,
                    shape: Utilities.getDefaultShape(),
                    child: Text(lSinscrire),
                    onPressed : () {}
                  )

*/
                  //Utilities.getButtonSubmit(libelle: lSinscrire, isDisable: _isDisable),
                  ),
            ],
          ),
        )
      ],
    ));
  }

  validerCondition({required BuildContext context, dynamic model}) {
    Utilities.alertDialogCustom(
        context: context,
        title: questionConditionsDutilisation,
        //content: lorem(paragraphs: 2, words: 100),
        content: (model != null && model.conditionsDutilisation != null)
            ? model.conditionsDutilisation
            : lDefaultConditionsDutilisation,
        rightFuction: () {
          Navigator.pop(context);
          _submit();
          Utilities.getToast(lAccepterConditions);
        },
        leftFuction: () {
          Navigator.pop(context);
          Utilities.getToast(lValiderConditions);
        },
        isHtmlContent: true);
  }

  _submit() async {
    setState(() {
      // griser button
      _isDisable = true;
    });

    Utilities.loadingIndicator(context: context, color: colorRedLogo);

    // ajout des informations
    _user.carteId = await PreferencesUtil.instance.getInt(carteId);
    //_user.carteId = await Utilities.carteIdUserConnected;

    _user.programmeFideliteTamponId =
        widget.tampon != null ? widget.tampon!.id : null;
    RequestCustom request = RequestCustom();
    request.user = await PreferencesUtil.instance.getInt(id);
    request.datas = [_user];

    ResponseCustom response = await baseApi.appelApi(
        endpointApi: souscriptionProgrammeFideliteTampon + create,
        request: request);

    Navigator.pop(context); // pour retirer le progressing

    if (response != null && response.hasError != null && !response.hasError!) {
      Utilities.getToast(lSuccesSouscriptionProgramme);
      // Utilities.resetAndOpenPage(
      //     context: context,
      //     view: InscriptionTamponOk(
      //       souscriptionPdfTampon: FideliteCarte.fromJson(response.items[0]),
      //     ));

      // Utilities.resetAndOpenPage(
      //     context: context,
      //     view: MenuPdfTampon(
      //         souscriptionPDFTamponId:
      //             FideliteCarte.fromJson(response.items[0]).id));

      Utilities.resetAndOpenPage(
          context: context,
          view: Home(
            currentIndex: indexTampon,
          ));
    } else {
      Utilities.messageApi(response: response);
    }

    setState(() {
      // degriser button
      _isDisable = false;
    });
  }
}
