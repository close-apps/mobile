import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/custom_widgets/card_pdv.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/fidelite_carte.dart';
import 'package:closeapps/models/request_custom.dart';
import 'package:closeapps/models/response_custom.dart';
import 'package:closeapps/models/user.dart';
import 'package:closeapps/views/google_map_view.dart';
import 'package:closeapps/views/template_afficher_text.dart';
import 'package:flutter/material.dart';

class PointDeVenteTampon extends StatefulWidget {
  PointDeVenteTampon(
      {Key? key,
      this.pdfTampon,
      this.idCarte,
      required this.programmeFideliteTamponId})
      : super(key: key);
  int? idCarte;
  int programmeFideliteTamponId;
  FideliteCarte? pdfTampon;

  @override
  _PointDeVenteTamponState createState() => _PointDeVenteTamponState();
}

class _PointDeVenteTamponState extends State<PointDeVenteTampon> {
  List<Widget> itemsPdvdPdf = [];
  bool _waitingItemsPdvPdf = true;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    _getListCustomPDVTampon();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
        (widget.pdfTampon != null && widget.pdfTampon!.libelle != null
                ? widget.pdfTampon!.libelle!
                : "CLOSE APP") +
            sautDelLine +
            lPointDeVente,
      )),
      body: (_waitingItemsPdvPdf)
          ? Center(child: CircularProgressIndicator())
          : (itemsPdvdPdf.isNotEmpty)
              ? GridView.count(
                  crossAxisCount: 2,
                  // crossAxisSpacing: 40,
                  padding: EdgeInsets.all(16.0),
                  children: itemsPdvdPdf,
                )
              : TemplateAfficherText(
                  contenu: aucuneDonneeDisponible,
                ),
    );
  }

  _getListCustomPDVTampon() async {
    User data = User();
    // appel de service
    RequestCustom request = RequestCustom();

    data.programmeFideliteTamponId = widget.programmeFideliteTamponId;
    data.isActiveEnseigne = true;

    request.data = data;

    ResponseCustom response = await baseApi.appelApi(
        endpointApi: pointDeVenteProgrammeFideliteTampon + getByCriteria,
        request: request);

    // construction du itemsPdvdPdf
    if (response.hasError != null && !response.hasError!) {
      List<FideliteCarte> items = [];

      if (response.items != null && response.items!.isNotEmpty) {
        items = FideliteCarte.fromJsons(response.items!);
        List<FideliteCarte> listAutresPDV = [];

        items.forEach((element) {
          // if (items.length > 1) {
          //   listAutresPDV = List<FideliteCarte>.from(items);
          //   listAutresPDV.remove(element);
          // }

          CardPDV newCustomPDVCard =
              CardPDV(informationsPDVDuPDF: element, listPDVDuPDF: items);

          //listAutresPDV = [];
          itemsPdvdPdf.add(newCustomPDVCard);
        });
      }
    } else {}

    setState(() {
      _waitingItemsPdvPdf = false;
    });
  }
}
