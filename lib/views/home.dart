import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/custom_widgets/card_custom.dart';
import 'package:closeapps/custom_widgets/item_custom.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/preferences_util.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/fidelite_carte.dart';
import 'package:closeapps/models/menu.dart';
import 'package:closeapps/models/request_custom.dart';
import 'package:closeapps/models/response_custom.dart';
import 'package:closeapps/models/user.dart';
import 'package:closeapps/views/changer_password.dart';
import 'package:closeapps/views/inscription.dart';
import 'package:closeapps/views/inscription_pdf_carte.dart';
import 'package:closeapps/views/inscription_pdf_tampon.dart';
import 'package:closeapps/views/is_locked_pdf.dart';
import 'package:closeapps/views/menu_pdf_carte.dart';
import 'package:closeapps/views/menu_pdf_tampon.dart';
import 'package:closeapps/views/parametrages.dart';
import 'package:closeapps/views/template_afficher_text.dart';
import 'package:flutter/material.dart';
import 'package:loading_indicator/loading_indicator.dart';

class Home extends StatefulWidget {
  int currentIndex;

  Home({Key? key, this.currentIndex = 0}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  //int widget.currentIndex = 0;
  bool _isSearching = false;
  bool _isDefaultAffichage = true;
  bool _isAddCard = false;
  bool _isLoading = true;
  //List<FideliteCarte> localDatasCartes = List();
  //List<dynamic> datas = List();
  List<FideliteCarte> datas = [];
  List<FideliteCarte> datasTampon = [];
  List<FideliteCarte> datasCarte = [];
  List<FideliteCarte> allDatas = [];

  TextEditingController _searchQuery = TextEditingController();
  String searchQuery = "Search query";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //localDatasCartes = datasCartes; //
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    _initDatasOnLine();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //title: Text(lHome),
        //backgroundColor: colorRedLogo,
        //backgroundColor: colorBlueLogo,
        leading: (_isSearching &&
                footerMenu[widget.currentIndex].title != lParametres)
            ? BackButton(
                onPressed:
                    _isAddCard ? _changeIsSearchingAddCard : _changeIsSearching)
            : null,
        title: (_isSearching &&
                footerMenu[widget.currentIndex].title != lParametres)
            ? _buildSearchField()
            : Text(footerMenu[widget.currentIndex].title!),
        actions: _buildActions(),
        //leading: Icon(Icons.add_circle_outline),
      ),
      body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: (footerMenu[widget.currentIndex].title != lParametres)
              //child: (widget.currentIndex != (footerMenu.length - 1))
              ? (_isLoading
                  ? Center(
                      child: LoadingIndicator(
                      //indicatorType: Indicator.ballScaleMultiple,
                      indicatorType: Indicator.ballClipRotateMultiple,
                      //indicatorType: Indicator.ballPulse,
                      //indicatorType: Indicator.ballScale,
                      colors: [colorRedLogo],
                    ))
                  //? Center(child: CircularProgressIndicator())
                  : _buildBody())
              :
              // Column(
              //     mainAxisAlignment: MainAxisAlignment.center,
              //     crossAxisAlignment: CrossAxisAlignment.stretch,
              //     children: <Widget>[
              //       Utilities.getButtonSubmit(
              //           //child: Text(lSeDeconnecter),
              //           libelle: lSeDeconnecter,
              //           onPressed: () async {
              //             await PreferencesUtil.instance
              //                 .putBool(isConnected, false);
              //             Utilities.resetAndOpenPage(context: context);
              //           }),
              //       Utilities.getButtonSubmit(
              //           //child: Text(lMAJProfil),
              //           libelle: lMAJProfil,
              //           onPressed: () async {
              //             //await PreferencesUtil.instance.putBool(lIsConnected, false);
              //             //Utilities.resetAndOpenPage(context: context);

              //             // get idUserConnecter
              //             //String login = await PreferencesUtil.instance.getString(lLogin);
              //             int userId =
              //                 await PreferencesUtil.instance.getInt(id);
              //             Utilities.navigatorPush(
              //                 context: context,
              //                 view: Inscription(
              //                   isUpdate: true,
              //                   //login: login,
              //                   userId: userId,
              //                 ));
              //           }),
              //       Utilities.getButtonSubmit(
              //           //child: Text(lSeDeconnecter),
              //           libelle: lChangerPassword,
              //           onPressed: () {
              //             Utilities.navigatorPush(
              //                 context: context, view: ChangerPassword());
              //           }),
              //     ],
              //   )

              Parametrages()),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: widget.currentIndex,
        items: footerMenu.map((Menu menu) {
          //items: ConstApp.menuFooterBasic.map((Menu destination) {
          return BottomNavigationBarItem(
            label: menu.title,
            icon: Icon(
              menu.icon,
            ),
            //backgroundColor: menu.color,
            // title: Text(menu.title)
          );
        }).toList(),
        /*const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            backgroundColor: colorWitheLogo,
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            backgroundColor: colorWitheLogo,
            title: Text('Business'),
          ),
          BottomNavigationBarItem(
            backgroundColor: colorWitheLogo,
            icon: Icon(Icons.school),
            title: Text('School'),
          ),
        ],*/

        onTap: selectIndex,

        fixedColor: colorRedLogo,
        //elevation: 100,
        // selectedFontSize: 16,

        //backgroundColor: colorRedLogo,
        backgroundColor: colorBlueLogo,
        unselectedItemColor: colorWitheLogo,
        //selectedItemColor: colorWitheLogo,
      ),
    );
  }

  selectIndex(int index) async {
    setState(() {
      print("widget.currentIndex");
      widget.currentIndex = index;
    });
    //await _initDatasOnLine(index: index);
    _initDatasOnLine(index: index);
  }

  _initDatasOnLine({int? index}) async {
    Utilities.begin("_initDatasOnLine");
    switch (footerMenu[index != null ? index : widget.currentIndex].title) {
      case lCartes:
        getDatas(
            tableName: souscriptionProgrammeFideliteCarte,
            tableNameAllDatas: programmeFideliteCarte);
        //getAllDatas(tableName: programmeFideliteCarte);
        break;
      case lTampon:
        getDatas(
            tableName: souscriptionProgrammeFideliteTampon,
            tableNameAllDatas: programmeFideliteTampon);
        //getAllDatas(tableName: programmeFideliteTampon);
        break;
      case lParametres:
        break;
      default:
    }
  }

  _initDatasInLocal({int? index}) {
    Utilities.begin("_initDatasInLocal");
    switch (footerMenu[index != null ? index : widget.currentIndex].title) {
      case lCartes:
        datas = datasCarte;

        break;
      case lTampon:
        datas = datasTampon;
        break;
      case lParametres:
        break;
      default:
    }
    //setState(() {});
  }

  _initActions() {
    // _actions = [];
  }

  _onClickSearch() {
    _changeIsSearching();
  }

  _onClickSearchAddCard() {
    _changeIsSearching();
  }

  _changeIsSearching() {
    setState(() {
      _searchQuery.clear();
      //localDatasCartes = datasCartes;
      if (_isAddCard) {
        //datas = allDatas;
        afficherPDFNonSouscrit();
      } else {
        _initDatasInLocal(index: widget.currentIndex);
      }
      _isSearching = !_isSearching;
    });
  }

  void afficherPDFNonSouscrit() {
    if (datas == null || datas.length <= 0) {
      print("*******************  datas null");
      datas = allDatas;
    } else {
      print("******************* datas not null");
      switch (footerMenu[widget.currentIndex].title) {
        case lCartes:
          datas = allDatas
              .where((element) => datas.every((element2) =>
                  element2.programmeFideliteCarteId != element.id))
              .toList();
          break;
        case lTampon:
          datas = allDatas
              .where((element) => datas.every((element2) =>
                  element2.programmeFideliteTamponId != element.id))
              .toList();
          break;
        default:
      }
    }
  }

  _changeIsSearchingAddCard() {
    _isAddCard = false;
    _changeIsSearching();
  }

  _onClear() {
    //_isAddCard = !_isAddCard;
    if (_searchQuery == null || _searchQuery.text.isEmpty) {
      _changeIsSearching();
    } else {
      _searchQuery.clear();
      updateSearchQuery("");
    }
  }

  _onClearAddCard() {
    _isAddCard = false;
    _onClear();
  }

  Widget _buildBody() {
    if (_isDefaultAffichage) {
      return (datas != null && datas.length > 0)
          ? GridView.count(
              crossAxisCount: 2,
              children: datas
                  .map((souscriptionPdfOuPdf) => InkWell(
                      //onTap: () => widget.onTap,
                      onTap: () {
                        if (_isAddCard) {
                          Utilities.getToast("Ajouter Card");

                          switch (footerMenu[widget.currentIndex].title) {
                            case lTampon:
                              Utilities.navigatorPush(
                                  context: context,
                                  view: InscriptionPDFTampon(
                                    tampon: souscriptionPdfOuPdf,
                                  ));
                              break;
                            case lCartes:
                              Utilities.navigatorPush(
                                  context: context,
                                  view: InscriptionPdFCarte(
                                    carte: souscriptionPdfOuPdf,
                                  ));
                              break;
                            default:
                          }
                        } else {
                          switch (footerMenu[widget.currentIndex].title) {
                            case lTampon:
                              Utilities.navigatorPush(
                                  context: context,
                                  view: souscriptionPdfOuPdf.isLocked != null &&
                                          souscriptionPdfOuPdf.isLocked!
                                      ? IsLockedPdf(
                                          programmeFideliteLibelle:
                                              souscriptionPdfOuPdf.libelle,
                                          raisonLocked:
                                              souscriptionPdfOuPdf.raisonLocked,
                                        )
                                      : MenuPdfTampon(
                                          souscriptionPDFTamponId:
                                              souscriptionPdfOuPdf.id!,
                                          messageNotifed: souscriptionPdfOuPdf
                                              .messageNotifed,
                                          isShowLastNotification:
                                              souscriptionPdfOuPdf
                                                  .isShowLastNotification,
                                        ));
                              break;
                            case lCartes:
                              Utilities.navigatorPush(
                                  context: context,
                                  view: souscriptionPdfOuPdf.isLocked != null &&
                                          souscriptionPdfOuPdf.isLocked!
                                      ? IsLockedPdf(
                                          programmeFideliteLibelle:
                                              souscriptionPdfOuPdf.libelle,
                                          raisonLocked:
                                              souscriptionPdfOuPdf.raisonLocked,
                                        )
                                      : MenuPdfCarte(
                                          souscriptionPdfCarteId:
                                              souscriptionPdfOuPdf.id!,
                                          messageNotifed: souscriptionPdfOuPdf
                                              .messageNotifed,
                                          isShowLastNotification:
                                              souscriptionPdfOuPdf
                                                  .isShowLastNotification,
                                        ));
                              break;
                            default:
                          }
                          //Utilities.getToast(lAVenir + " " + carte.libelle);
                        }
                        //Utilities.getToast(lAVenir + " " + carte.title);
                      },
                      child: CardCustom(
                        pdfOuSouscriptionPdf: souscriptionPdfOuPdf,
                      )))
                  .toList())
          : ((_isAddCard || _isSearching) &&
                  allDatas != null &&
                  allDatas.length > 0)
              ? TemplateAfficherText(
                  contenu: aucuneDonneeCorrespondante,
                )
              : TemplateAfficherText(
                  contenu: aucuneDonneeDisponible,
                );
    } else {
      return (datas != null && datas.length > 0)
          ? ListView.builder(
              itemCount: datas.length,
              itemBuilder: (context, index) {
                FideliteCarte souscriptionPdfOuPdf = datas[index];
                return InkWell(
                    onTap: () {
                      if (_isAddCard) {
                        Utilities.getToast("Ajouter Card");
                        switch (footerMenu[widget.currentIndex].title) {
                          case lTampon:
                            Utilities.navigatorPush(
                                context: context,
                                view: InscriptionPDFTampon(
                                  tampon: souscriptionPdfOuPdf,
                                ));
                            break;
                          case lCartes:
                            Utilities.navigatorPush(
                                context: context,
                                view: InscriptionPdFCarte(
                                  carte: souscriptionPdfOuPdf,
                                ));
                            break;
                          default:
                        }
                      } else {
                        switch (footerMenu[widget.currentIndex].title) {
                          case lTampon:
                            Utilities.navigatorPush(
                                context: context,
                                view: souscriptionPdfOuPdf.isLocked != null &&
                                        souscriptionPdfOuPdf.isLocked!
                                    ? IsLockedPdf(
                                        programmeFideliteLibelle:
                                            souscriptionPdfOuPdf.libelle,
                                        raisonLocked:
                                            souscriptionPdfOuPdf.raisonLocked,
                                      )
                                    : MenuPdfTampon(
                                        souscriptionPDFTamponId:
                                            souscriptionPdfOuPdf.id!,
                                        messageNotifed:
                                            souscriptionPdfOuPdf.messageNotifed,
                                        isShowLastNotification:
                                            souscriptionPdfOuPdf
                                                .isShowLastNotification,
                                      ));
                            break;
                          case lCartes:
                            Utilities.navigatorPush(
                                context: context,
                                view: souscriptionPdfOuPdf.isLocked != null &&
                                        souscriptionPdfOuPdf.isLocked!
                                    ? IsLockedPdf(
                                        programmeFideliteLibelle:
                                            souscriptionPdfOuPdf.libelle,
                                        raisonLocked:
                                            souscriptionPdfOuPdf.raisonLocked,
                                      )
                                    : MenuPdfCarte(
                                        souscriptionPdfCarteId:
                                            souscriptionPdfOuPdf.id!,
                                        messageNotifed:
                                            souscriptionPdfOuPdf.messageNotifed,
                                        isShowLastNotification:
                                            souscriptionPdfOuPdf
                                                .isShowLastNotification,
                                      ));
                            break;
                          default:
                        }
                        //Utilities.getToast(lAVenir + " " + carte.libelle);

                      }
                    },
                    child: ItemCustom(
                      pdfOuSouscriptionPdf: souscriptionPdfOuPdf,
                    ));
              })
          : ((_isAddCard || _isSearching) &&
                  allDatas != null &&
                  allDatas.length > 0)
              ? TemplateAfficherText(
                  contenu: aucuneDonneeCorrespondante,
                )
              : TemplateAfficherText(
                  contenu: aucuneDonneeDisponible,
                );
    }
  }

  getDatas(
      {required String tableName,
      required String tableNameAllDatas,
      User? data}) async {
    setState(() {
      _isLoading = true;
    });
    RequestCustom request = RequestCustom();
    if (data == null) {
      data = User();
    }

    data.carteId = await PreferencesUtil.instance.getInt(carteId);
    data.userId = await PreferencesUtil.instance.getInt(id);
    data.isActiveEnseigne = true;
    //data.carteId = await Utilities.carteIdUserConnected;
    //data.userId = await Utilities.idUserConnected;

    //Utilities.begin(data.carteId);
    //Utilities.begin(data.userId);
    request.data = data;
    ResponseCustom response = await baseApi.appelApi(
        endpointApi: tableName + getByCriteria, request: request);

    if (response.hasError != null && !response.hasError!) {
      if (response.items != null && response.items!.isNotEmpty) {
        if (tableName == souscriptionProgrammeFideliteCarte) {
          datasCarte = FideliteCarte.fromJsons(response.items!);
        } else {
          if (tableName == souscriptionProgrammeFideliteTampon) {
            datasTampon = FideliteCarte.fromJsons(response.items!);
          }
        }
        datas = FideliteCarte.fromJsons(response.items!);
      } else {
        datas = [];
      }
      //datas = response.items;
      //allDatas = response.items;
    } else {}
    await getAllDatas(tableNameAllDatas: tableNameAllDatas);
    setState(() {
      _isLoading = false;
    });
  }

  getAllDatas({required String tableNameAllDatas}) async {
    RequestCustom request = RequestCustom();
    request.data = User(isActiveEnseigne: true);
    ResponseCustom response = await baseApi.appelApi(
        endpointApi: tableNameAllDatas + getByCriteria, request: request);
    if (response.hasError != null && !response.hasError!) {
      //datas = response.items;
      //allDatas = response.items;
      if (response.items != null && response.items!.isNotEmpty) {
        allDatas = FideliteCarte.fromJsons(response.items!);
      } else {
        allDatas = [];
      }
    } else {}
    if (_isAddCard) {
      afficherPDFNonSouscrit();
      //datas = allDatas;
    }
    setState(() {});
  }

/*

 getDatas({String tableName, User data}) async {
    setState(() {
      _isLoading = true;
    });
    RequestCustom request = RequestCustom();
    if (data == null) {
      data = User();
    }

    data.carteId = await PreferencesUtil.instance.getInt(carteId);
    data.userId = await PreferencesUtil.instance.getInt(carteId);

    request.data = data;
    ResponseCustom response = await baseApi.appelApi(
        apiBaseUrl: baseUrlAPIDev + tableName + getByCriteria,
        request: request);

    if (!response.hasError) {
      if (response.items != null) {
        if (tableName == souscriptionProgrammeFideliteCarte) {
          datasCarte = FideliteCarte.fromJsons(response.items);
        } else {
          if (tableName == souscriptionProgrammeFideliteTampon) {
            datasTampon = FideliteCarte.fromJsons(response.items);
          }
        }
        datas = FideliteCarte.fromJsons(response.items);
      } else {
        datas = List();
      }
      //datas = response.items;
      //allDatas = response.items;
    } else {}
    setState(() {
      _isLoading = false;
    });
  }

 
  getAllDatas({String tableName}) async {
    setState(() {
      _isLoading = true;
    });
    RequestCustom request = RequestCustom();
    request.data = User();
    ResponseCustom response = await baseApi.appelApi(
        apiBaseUrl: baseUrlAPIDev + tableName + getByCriteria,
        request: request);

    if (!response.hasError) {
      //datas = response.items;
      //allDatas = response.items;
      if (response.items != null) {
        allDatas = FideliteCarte.fromJsons(response.items);
      } else {
        allDatas = List();
      }
    } else {}
    setState(() {
      _isLoading = false;
    });
  }
  */

  _buildActions() {
    if (footerMenu[widget.currentIndex].title == lParametres) {
      return null;
    } else {
      if (_isSearching) {
        return <Widget>[
          /*IconButton(
          icon: Icon(Icons.search),
          onPressed: () {},
        ),*/
          IconButton(
            icon: Icon(Icons.clear),
            onPressed: (_isAddCard) ? _onClearAddCard : _onClear,
          ),
        ];
      }

      return <Widget>[
        IconButton(
          icon: Icon(Icons.search),
          //onPressed: _startSearch,
          onPressed: _onClickSearch,
        ),
        IconButton(
          icon: Icon(_isDefaultAffichage ? Icons.list : Icons.apps),
          //onPressed: _startSearch,
          onPressed: _rebuildAffichage,
        ),
        IconButton(
          icon: Icon(Icons.add_circle_outline),
          //onPressed: _startSearch,
          onPressed: _addCarte,
        )
      ];
    }
  }

  _rebuildAffichage() {
    _isDefaultAffichage = !_isDefaultAffichage;
    if (_isDefaultAffichage) {
      // mettre en liste
    } else {
      // mettre en card
    }
    setState(() {});
  }

  _addCarte() {
    //_isAddCard = !_isAddCard;
    _isAddCard = true;
    _changeIsSearching();
  }

  Widget _buildSearchField() {
    return new TextField(
      controller: _searchQuery,
      autofocus: true,
      decoration: const InputDecoration(
        hintText: 'Search...',
        border: InputBorder.none,
        hintStyle: const TextStyle(color: Colors.white30),
      ),
      style: const TextStyle(color: Colors.white, fontSize: 16.0),
      onChanged: updateSearchQuery,
    );
  }

  void updateSearchQuery(String newQuery) {
    if (_isAddCard) {
      if (newQuery == null || newQuery.isEmpty) {
        // print("search test " + test
        //localDatasCartes = datasCartes;
        datas = allDatas;
        print("je suis vide query " + newQuery);
      } else {
        String newQueryToCompare = newQuery.toUpperCase();
        Iterable<FideliteCarte> test = allDatas.where((element) =>
            element.libelle!.toUpperCase().contains(newQueryToCompare));
        if (test != null) {
          //localDatasCartes = test.toList();
          datas = test.toList();
        }
        print(test.toList().length);
      }
    } else {
      if (newQuery == null || newQuery.isEmpty) {
        // print("search test " + test
        //localDatasCartes = datasCartes;
        //datas = allDatas;
        _initDatasInLocal(index: widget.currentIndex);
        print("je suis vide query " + newQuery);
      } else {
        String newQueryToCompare = newQuery.toUpperCase();

        switch (footerMenu[widget.currentIndex].title) {
          case lCartes:
            Iterable<FideliteCarte> test = datasCarte.where((element) =>
                element.libelle!.toUpperCase().contains(newQueryToCompare));
            if (test != null) {
              //localDatasCartes = test.toList();
              datas = test.toList();
            }
            break;
          case lTampon:
            Iterable<FideliteCarte> test = datasTampon.where((element) =>
                element.libelle!.toUpperCase().contains(newQueryToCompare));
            if (test != null) {
              //localDatasCartes = test.toList();
              datas = test.toList();
            }
            break;
          case lParametres:
            break;
          default:
        }

        //print(test.toList().length);
      }
    }
    setState(() {
      searchQuery = newQuery;
    });
    print("datas.length ${datas.length}");
    print("search query " + newQuery);
  }

  // void updateSearchQuery(String newQuery) {

  //   if (_isAddCard) {
  //   } else {
  //   }
  //   if (newQuery == null || newQuery.isEmpty) {
  //     // print("search test " + test
  //     //localDatasCartes = datasCartes;
  //     //datas = allDatas;
  //     print("je suis vide query " + newQuery);
  //   } else {
  //     String newQueryToCompare = newQuery.toUpperCase();
  //     Iterable<FideliteCarte> test = allDatas.where((element) =>
  //         element.libelle.toUpperCase().contains(newQueryToCompare));
  //     if (test != null) {
  //       //localDatasCartes = test.toList();
  //       datas = test.toList();
  //     }
  //     print(test.toList().length);
  //   }
  //   setState(() {
  //     searchQuery = newQuery;
  //   });
  //   print("datas.length ${datas.length}");
  //   print("search query " + newQuery);
  // }
}
