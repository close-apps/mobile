import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/fidelite_carte.dart';
import 'package:closeapps/models/request_custom.dart';
import 'package:closeapps/models/response_custom.dart';
import 'package:closeapps/models/user.dart';
import 'package:closeapps/views/depenser_point_carte.dart';
import 'package:flutter/material.dart';
import 'package:getwidget/getwidget.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

class CagnotteCarte extends StatefulWidget {
  CagnotteCarte(
      {Key? key,
      this.souscriptionPdfCarteId,
      this.idCarte,
      required this.programmeFideliteCarteId})
      : super(key: key);
  int? idCarte;
  int programmeFideliteCarteId;
  int? souscriptionPdfCarteId;
  //FideliteCarte souscriptionPdfCarte;

  @override
  _CagnotteCarteState createState() => _CagnotteCarteState();
}

class _CagnotteCarteState extends State<CagnotteCarte> {
  String nbrePoint = "1000";
  String dateValidite = "20/11/2020";
  List<TableRow> listTableRow = [Utilities.headerHistoriqueTable];
  List<GFAccordion> listGFAccordion = [];
  bool _waitingHistorique = true;
  bool isWaitingData = true;

  FideliteCarte? souscriptionPdfCarte;

  int? programmeFideliteCarteId;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    // appel de service pour les datas
    initData();
    getContentTable();
  }

  @override
  Widget build(BuildContext context) {
    int xof = Utilities.getXOF(fideliteCarte: souscriptionPdfCarte);

    return Scaffold(
      appBar: AppBar(
          //appBar: AppBar(
          title: Text(
        (souscriptionPdfCarte != null && souscriptionPdfCarte!.libelle != null
                ? souscriptionPdfCarte!.libelle!
                : "CLOSE APP") +
            sautDelLine +
            lMaCagnotte,
      )),
      body:
          // SingleChildScrollView(
          //   padding: Utilities.paddingAll(),
          //   child:
          Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          // shrinkWrap: true,
          children: [
            Text(
              "${((souscriptionPdfCarte != null && souscriptionPdfCarte!.nbrePointActuelle != null) ? souscriptionPdfCarte!.nbrePointActuelle : "0")}" +
                  espace +
                  lPoints +
                  tiret +
                  '$xof' +
                  espace +
                  lXOF,
              style: Utilities.style(),
            ),
            Utilities.getDefaultSizeBoxWithHeight(),
            Text(
              lDureeDeValidite + deuxPoint + dateValidite,
              style: Utilities.style(),
            ),
            Utilities.getDefaultSizeBoxWithHeight(height: 30),
            Center(
              child: Utilities.getButtonSubmit(
                  libelle: lDepenserDesPoints,
                  onPressed: () {
                    Utilities.navigatorPush(
                        context: context,
                        view: DepenserPointCarte(
                          souscriptionPdfCarteId: souscriptionPdfCarte!.id!,
                          idCarte: widget.idCarte,
                        ));
                  }),
            ),
            Utilities.getDefaultSizeBoxWithHeight(),
            Text(
              lHistorique,
              style: Utilities.style(),
            ),
            Utilities.getDefaultSizeBoxWithHeight(),
            (_waitingHistorique)
                ? Center(child: CircularProgressIndicator())
                : Expanded(
                    child: ListView(
                      //shrinkWrap: true,
                      children: listGFAccordion,
                    ),
                  ),

            //Expanded(child: GFAccordion(child: listGFAccordion,)

            // Accordion(
            //     maxOpenSections: 4,
            //     headerBorderRadius: 20,
            //     headerBackgroundColor: Color(0xff999999),
            //     // headerPadding:
            //     //     EdgeInsets.symmetric(vertical: 7, horizontal: 15),
            //     children: listAccordionSection),

            // Table(
            //     columnWidths: {
            //       0: FlexColumnWidth(2),
            //       1: FlexColumnWidth(1.5),
            //       2: FlexColumnWidth(1),
            //       3: FlexColumnWidth(1),
            //       4: FlexColumnWidth(2),
            //     },
            //     //border: TableBorder.all(),
            //     children: (listTableRow != null && listTableRow.isNotEmpty)
            //         ? listTableRow
            //         : Utilities.emptyTable(nbreColumn: 5),
            //   ),
          ],
        ),
      ),
      //),
    );
  }

  initData() async {
    User data = User();
    data.id = widget.souscriptionPdfCarteId;
    ResponseCustom response = await baseApi.newAppelApi(
      data: data,
      endpointApi: souscriptionProgrammeFideliteCarte + getByCriteria,
    );

    if (response != null &&
        response.hasError != null &&
        !response.hasError! &&
        response.items != null &&
        response.items!.isNotEmpty) {
      souscriptionPdfCarte = FideliteCarte.fromJson(response.items![0]);
      programmeFideliteCarteId = souscriptionPdfCarte!.programmeFideliteCarteId;
    }
    isWaitingData = false;

    setState(() {});
  }

  getContentTable() async {
    User data = User();
    // appel de service
    RequestCustom request = RequestCustom();

    data.carteId = widget.idCarte;
    data.programmeFideliteCarteId = widget.programmeFideliteCarteId;

    request.data = data;

    ResponseCustom response = await baseApi.appelApi(
        endpointApi: historiqueCarteProgrammeFideliteCarte + getByCriteria,
        request: request);

    // construction du listTableRow
    if (response.hasError != null && !response.hasError!) {
      List<User> items = [];

      if (response.items != null && response.items!.isNotEmpty) {
        items = User.fromJsons(response.items!);
        //datasUser.forEach((element) {
        items.forEach((element) async {
          // print(
          //     "element.programmeFideliteCarteCorrespondanceNbrePoint : ${element.programmeFideliteCarteCorrespondanceNbrePoint}");
          // print(
          //     "element.programmeFideliteCartePourXAcheter : ${element.programmeFideliteCartePourXAcheter}");
          // print("element.nbrePointOperation : ${element.nbrePointOperation}");

          // String xof = "";
          // if (element.programmeFideliteCarteCorrespondanceNbrePoint != null &&
          //     element.programmeFideliteCartePourXAcheter != null &&
          //     element.nbrePointOperation != null) {
          //   xof =
          //       "${(element.nbrePointOperation * element.programmeFideliteCartePourXAcheter) / element.programmeFideliteCarteCorrespondanceNbrePoint}";
          // }
          // if (element.programmeFideliteCarteCorrespondanceNbrePoint != null &&
          //     element.programmeFideliteCarteNbrePointObtenu != null &&
          //     element.nbrePointOperation != null) {
          //   xof =
          //       "${(element.nbrePointOperation! * element.programmeFideliteCarteCorrespondanceNbrePoint!) / element.programmeFideliteCarteNbrePointObtenu!}";
          // }

          DateTime? dateAction;
          String? dateActionToString;
          if (element.dateAction != null) {
            // dateAction = DateFormat("dd/MM/yyyy HH:mm:ss")
            await initializeDateFormatting("fr_FR");

            //Intl.defaultLocale = "fr_FR";
            dateAction = DateFormat("dd/MM/yyyy").parse(element.dateAction!);
            dateActionToString = DateFormat("dd MMMM yyyy").format(dateAction);
          }
          print("dateActionToString ::: $dateActionToString");

          listGFAccordion.add(GFAccordion(
            titleBorder: Border.all(color: Color.fromRGBO(0, 5, 55, .2)),
            expandedTitleBackgroundColor: Color.fromRGBO(0, 5, 55, .2),
            titleBorderRadius: const BorderRadius.only(
                topLeft: Radius.circular(5), topRight: Radius.circular(5)),
            contentBorderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(5),
                bottomRight: Radius.circular(5)),
            margin: EdgeInsets.symmetric(horizontal: 0, vertical: 5),
            collapsedIcon: Icon(Icons.remove_red_eye),
            titleChild: Text(element.typeActionLibelle! +
                " du " +
                (dateActionToString ?? "")),
            contentChild: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Lieu: " +
                    (element.pointDeVenteNom ??
                        "")), //TODO : ajouter les villes au pdf
                Text("Nbre de point: " +
                    ("${element.nbrePointOperation ?? ''}")),
                Text("XOF: " + '${element.sommeOperation ?? ""}')
              ],
            ),
          ));

          // List<Widget> listWidget = [];
          // Widget colOperation = Text(element.typeActionLibelle ?? "");
          // Widget colDate = Text(dateActionToString ?? "");
          // Widget colPoint = Text("${element.nbrePointOperation ?? ''}");
          // Widget colXOF = Text(xof);
          // Widget colLieu =
          //     Text(element.lieu ?? "");

          // listWidget.add(colOperation);
          // listWidget.add(colDate);
          // listWidget.add(colPoint);
          // listWidget.add(colXOF);
          // listWidget.add(colLieu);
          // listTableRow.add(TableRow(children: listWidget));
        });
      }
    } else {}

    setState(() {
      _waitingHistorique = false;
    });
  }

  List<User> datasUser = [
    // CinqPourCent
    User(
        actionLibelle: "Crédit",
        lieu: "Abobo",
        dateAction: "12/11/2020",
        xof: "1000 F CFA",
        point: "100"),
    User(
        actionLibelle: "Crédit",
        lieu: "Anyama",
        dateAction: "13/11/2020",
        xof: "6000 F CFA",
        point: "300"),
    User(
        actionLibelle: "Débit",
        lieu: "Trechville",
        dateAction: "21/11/2020",
        xof: "5000 F CFA",
        point: "200"),
    User(
        actionLibelle: "Débit",
        lieu: "Adjamé",
        dateAction: "12/12/2020",
        xof: "3000 F CFA",
        point: "150"),
    User(
        actionLibelle: "Crédit",
        lieu: "Korhogo",
        dateAction: "15/12/2020",
        xof: "2000 F CFA",
        point: "100"),
    User(
        actionLibelle: "Débit",
        lieu: "Bouaké",
        dateAction: "20/12/2020",
        xof: "1500 F CFA",
        point: "100"),
  ];
}
