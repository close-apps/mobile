import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/preferences_util.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/views/apropos_declose.dart';
import 'package:closeapps/views/changer_password.dart';
import 'package:closeapps/views/inscription.dart';
import 'package:closeapps/views/nous_contacter_ou_suivre.dart';
import 'package:closeapps/views/nous_evaluer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:share_plus/share_plus.dart';

class Parametrages extends StatelessWidget {
  const Parametrages({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      //mainAxisAlignment: MainAxisAlignment.center,
      //crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        ListTile(
          leading: Icon(FontAwesomeIcons.userEdit),
          title: Text(lMAJProfil),
          onTap: () async {
            //await PreferencesUtil.instance.putBool(lIsConnected, false);
            //Utilities.resetAndOpenPage(context: context);

            // get idUserConnecter
            //String login = await PreferencesUtil.instance.getString(lLogin);
            int userId = await PreferencesUtil.instance.getInt(id);
            Utilities.navigatorPush(
                context: context,
                view: Inscription(
                  isUpdate: true,
                  //login: login,
                  userId: userId,
                ));
          },
          trailing: Icon(
            Icons.navigate_next_rounded,
          ),
          selected: true,
        ),
        Utilities.getDefaultDivider(),
        ListTile(
          leading: Icon(FontAwesomeIcons.lockOpen),
          title: Text(lChangerPassword),
          onTap: () {
            Utilities.navigatorPush(context: context, view: ChangerPassword());
          },
          trailing: Icon(
            Icons.navigate_next_rounded,
          ),
          selected: true,
        ),
        Utilities.getDefaultDivider(),
        ListTile(
          leading: Icon(FontAwesomeIcons.marker),
          title: Text(lNousEvaluer),
          onTap: () {
            Utilities.navigatorPush(context: context, view: NousEvaluer());
          },
          trailing: Icon(
            Icons.navigate_next_rounded,
          ),
          selected: true,
        ),
        Utilities.getDefaultDivider(),
        ListTile(
          leading: Icon(FontAwesomeIcons.shareAlt),
          title: Text(lPartagerClose),
          onTap: () {
            Share.share(
              "Voici le lien de l'application ${appName}. Merci de la télécharger afin de profiter de son contenu !!! ${lienApp}",
            );
          },
          trailing: Icon(
            Icons.navigate_next_rounded,
          ),
          selected: true,
        ),
        Utilities.getDefaultDivider(),

        ListTile(
          selected: true,
          leading: Icon(FontAwesomeIcons.thumbsUp),
          title: Text(lNousSuivre),
          onTap: () {
            Utilities.navigatorPush(
                context: context,
                view: NousContacterOuNouSuivre(
                  title: lNousSuivre,
                  isNousContacter: false,
                ));
          },
          trailing: Icon(
            Icons.navigate_next_rounded,
          ),
        ),
        Utilities.getDefaultDivider(),
        ListTile(
          selected: true,
          leading: Icon(FontAwesomeIcons.fileSignature),
          title: Text(lNousContacter),
          onTap: () {
            Utilities.navigatorPush(
                context: context,
                view: NousContacterOuNouSuivre(
                  title: lNousContacter,
                ));
          },
          trailing: Icon(
            Icons.navigate_next_rounded,
          ),
        ),
        Utilities.getDefaultDivider(),
        ListTile(
          selected: true,
          leading: Icon(FontAwesomeIcons.addressCard),
          title: Text(lAProposDeClose),
          onTap: () {
            Utilities.navigatorPush(context: context, view: AproposeDeClose());
          },
          trailing: Icon(
            Icons.navigate_next_rounded,
          ),
        ),
        Utilities.getDefaultDivider(),
        ListTile(
          leading: Icon(FontAwesomeIcons.signOutAlt),
          title: Text(lSeDeconnecter),
          onTap: () async {
            await PreferencesUtil.instance.putBool(isConnected, false);
            Utilities.resetAndOpenPage(context: context);
          },
          trailing: Icon(
            Icons.navigate_next_rounded,
          ),
          selected: true,
        ),
        // Utilities.getButtonSubmit(
        //     //child: Text(lMAJProfil),
        //     libelle: lMAJProfil,
        //     onPressed: () async {
        //       //await PreferencesUtil.instance.putBool(lIsConnected, false);
        //       //Utilities.resetAndOpenPage(context: context);

        //       // get idUserConnecter
        //       //String login = await PreferencesUtil.instance.getString(lLogin);
        //       int userId = await PreferencesUtil.instance.getInt(id);
        //       Utilities.navigatorPush(
        //           context: context,
        //           view: Inscription(
        //             isUpdate: true,
        //             //login: login,
        //             userId: userId,
        //           ));
        //     }),

        // Utilities.getButtonSubmit(
        //     //child: Text(lSeDeconnecter),
        //     libelle: lChangerPassword,
        //     onPressed: () {
        //       Utilities.navigatorPush(
        //           context: context, view: ChangerPassword());
        //     }),
        // Utilities.getButtonSubmit(
        //     //child: Text(lSeDeconnecter),
        //     libelle: lAProposDeClose,
        //     onPressed: () {
        //       Utilities.navigatorPush(
        //           context: context, view: AproposeDeClose());
        //     }),
        // Utilities.getButtonSubmit(
        //     //child: Text(lSeDeconnecter),
        //     libelle: lNousContacter,
        //     onPressed: () {
        //       Utilities.navigatorPush(
        //           context: context,
        //           view: NousContacterOuNouSuivre(
        //             title: lNousContacter,
        //           ));
        //     }),
        // Utilities.getButtonSubmit(
        //     //child: Text(lSeDeconnecter),
        //     libelle: lPartagerClose,
        //     onPressed: () {
        //       Share.share(
        //         "Voici le lien de l'application ${appName}. Merci de la télécharger afin de profiter de son contenu !!! ${lienApp}",
        //       );
        //     }),
        // Utilities.getButtonSubmit(
        //     //child: Text(lSeDeconnecter),
        //     libelle: lModifierPreferencesApp,
        //     onPressed: () => Utilities.getToast(lAVenir)),
        // Utilities.getButtonSubmit(
        //     //child: Text(lSeDeconnecter),
        //     libelle: lNousSuivre,
        //     onPressed: () {
        //       Utilities.navigatorPush(
        //           context: context,
        //           view: NousContacterOuNouSuivre(
        //             title: lNousSuivre,
        //             isNousContacter: false,
        //           ));
        //     }),
        // Utilities.getButtonSubmit(
        //     //child: Text(lSeDeconnecter),
        //     libelle: lNousEvaluer,
        //     onPressed: () {
        //       Utilities.navigatorPush(context: context, view: NousEvaluer());
        //     }),
        // Utilities.getButtonSubmit(
        //     //child: Text(lSeDeconnecter),
        //     libelle: lSeDeconnecter,
        //     onPressed: () async {
        //       await PreferencesUtil.instance.putBool(isConnected, false);
        //       Utilities.resetAndOpenPage(context: context);
        //     }),
        //ElevatedButton(onPressed: () {}, child: Text("A propos de Close"))
      ],
    );
  }
}
