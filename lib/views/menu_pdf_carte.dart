import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/preferences_util.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/fidelite_carte.dart';
import 'package:closeapps/models/response_custom.dart';
import 'package:closeapps/models/user.dart';
import 'package:closeapps/views/cagnotte_carte.dart';
import 'package:closeapps/views/depenser_point_carte.dart';
import 'package:closeapps/views/menu_partager_point.dart';
import 'package:closeapps/views/parrainer_amis.dart';
import 'package:closeapps/views/partager_point_carte.dart';
import 'package:closeapps/views/point_de_vente_carte.dart';
import 'package:closeapps/views/scanner_carte.dart';
import 'package:flutter/material.dart';

class MenuPdfCarte extends StatefulWidget {
  MenuPdfCarte(
      {Key? key,
      required this.souscriptionPdfCarteId,
      this.messageNotifed,
      this.isShowLastNotification})
      : super(key: key);

  int souscriptionPdfCarteId;
  String? messageNotifed;
  bool? isShowLastNotification;

  //FideliteCarte souscriptionPDF;

  @override
  _MenuPdfCarteState createState() => _MenuPdfCarteState();
}

class _MenuPdfCarteState extends State<MenuPdfCarte> {
  dynamic idCarte;
  dynamic programmeFideliteCarteId;

  bool isWaitingData = true;

  FideliteCarte? souscriptionPdfCarte;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    initData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
        souscriptionPdfCarte != null && souscriptionPdfCarte!.libelle != null
            ? souscriptionPdfCarte!.libelle!
            //: "CLOSE APP",
            : "...",
      )),
      body: isWaitingData
          ? Center(child: CircularProgressIndicator())
          : Column(
              children: [
                Expanded(
                  child: Center(
                    child: GridView.count(
                      crossAxisCount: 2,
                      shrinkWrap: true,
                      crossAxisSpacing: 40,
                      padding: EdgeInsets.all(16.0),
                      children: [
                        CustomCardMenuPdf(
                          libelle: lMaCarte,
                          onTap: () {
                            Utilities.navigatorPush(
                                context: context,
                                view: ScannerCarte(
                                  pdfCarte: souscriptionPdfCarte,
                                  idCarte: idCarte,
                                  souscriptionProgrammeFideliteCarteId:
                                      souscriptionPdfCarte!.id!,
                                  programmeFideliteCarteId:
                                      programmeFideliteCarteId,
                                ));
                          },
                        ),
                        CustomCardMenuPdf(
                          libelle: lMaCagnotte,
                          onTap: () {
                            Utilities.navigatorPush(
                                context: context,
                                view: CagnotteCarte(
                                  souscriptionPdfCarteId:
                                      souscriptionPdfCarte!.id!,
                                  idCarte: idCarte,
                                  programmeFideliteCarteId:
                                      programmeFideliteCarteId,
                                ));
                          },
                        ),
                        CustomCardMenuPdf(
                          libelle: lDepenserDesPoints,
                          onTap: () {
                            Utilities.navigatorPush(
                                context: context,
                                view: DepenserPointCarte(
                                  souscriptionPdfCarteId:
                                      souscriptionPdfCarte!.id!,
                                  idCarte: idCarte,
                                ));
                          },
                        ),
                        CustomCardMenuPdf(
                          libelle: lPartagerDesPoints,
                          onTap: () {
                            Utilities.navigatorPush(
                                context: context,
                                view: MenuPartagerPoint(
                                  souscriptionPdfCarte: souscriptionPdfCarte,
                                  idCarte: idCarte,
                                  programmeFideliteCarteId:
                                      programmeFideliteCarteId,
                                ));
                          },
                        ),
                        CustomCardMenuPdf(
                          libelle: lParrainerDesAmis,
                          onTap: () {
                            Utilities.navigatorPush(
                                context: context,
                                view: ParrainerAmis(
                                  souscriptionPdfCarte: souscriptionPdfCarte,
                                  idCarte: idCarte,
                                  programmeFideliteCarteId:
                                      programmeFideliteCarteId,
                                ));
                          },
                        ),
                        CustomCardMenuPdf(
                          libelle: lPointDeVente,
                          onTap: () {
                            Utilities.navigatorPush(
                                context: context,
                                view: PointDeVenteCarte(
                                  pdfCarte: souscriptionPdfCarte,
                                  idCarte: idCarte,
                                  programmeFideliteCarteId:
                                      programmeFideliteCarteId,
                                ));
                          },
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: InkWell(
                    onTap: () {
                      voirConditionsDutilisations(context: context);
                    },
                    child: Text(
                      lConditionsDutilisation,
                      style: Utilities.style(),
                    ),
                  ),
                )
              ],
            ),
    );
  }

  initData() async {
    idCarte = await PreferencesUtil.instance.getInt(carteId);

    User data = User();

    print("widget.messageNotifed :::: ${widget.messageNotifed}");

    if (widget.messageNotifed != null &&
        (widget.isShowLastNotification == null ||
            !widget.isShowLastNotification!)) {
      Utilities.alertDialogCustom(
          context: context,
          title: messageNotificationDelitPDF,
          content: widget.messageNotifed!,
          libelleRightFuction: lFermer,
          libelleLeftFuction: lNePlusMeRappeler,
          afficherToast: false,
          leftFuction: nePlusMeRappelerNotification);
    }

    data.id = widget.souscriptionPdfCarteId;
    ResponseCustom response = await baseApi.newAppelApi(
      data: data,
      endpointApi: souscriptionProgrammeFideliteCarte + getByCriteria,
    );

    if (response != null &&
        response.hasError != null &&
        !response.hasError! &&
        response.items != null &&
        response.items!.length > 0) {
      souscriptionPdfCarte = FideliteCarte.fromJson(response.items![0]);

      programmeFideliteCarteId = souscriptionPdfCarte!.programmeFideliteCarteId;
    }
    isWaitingData = false;

    setState(() {});
  }

  nePlusMeRappelerNotification() async {
    Navigator.pop(context);
    FideliteCarte data = FideliteCarte(
        id: widget.souscriptionPdfCarteId, isShowLastNotification: true);

    baseApi.newAppelApi(
        datas: [data],
        endpointApi:
            souscriptionProgrammeFideliteCarte + update).then((reponse) {
      print("succes nePlusMeRappelerNotification");
    }).catchError((onError) {
      print("error nePlusMeRappelerNotification");
    });
  }

  void voirConditionsDutilisations({required BuildContext context}) {
    Utilities.showDialogCustom(
        context: context,
        titre: lConditionsDutilisation,
        //content: lorem(paragraphs: 2, words: 100),
        contenu: (souscriptionPdfCarte != null &&
                souscriptionPdfCarte!.dataProgrammeFideliteCarte != null &&
                souscriptionPdfCarte!.dataProgrammeFideliteCarte![
                        'conditionsDutilisation'] !=
                    null)
            ? souscriptionPdfCarte!
                .dataProgrammeFideliteCarte!['conditionsDutilisation']
            : lDefaultConditionsDutilisation,
        rightFuction: () {
          // setState(() {
          //   _accepterCondition = false;
          // });
          Navigator.pop(context);
          Utilities.getToast(lLireConditions);
        },
        libelleRightFuction: lFermer,
        isHtmlContent: true);
  }
}

class CustomCardMenuPdf extends StatelessWidget {
  const CustomCardMenuPdf({
    Key? key,
    this.libelle,
    this.onTap,
  }) : super(key: key);

  final String? libelle;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: colorBlueLogo,
      //onTap: onTap ?? () {},
      onTap: onTap,
      child: Card(
        borderOnForeground: false,
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              libelle ?? lAVenir,
              style: style20BlueBold,
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}
