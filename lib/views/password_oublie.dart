import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/request_custom.dart';
import 'package:closeapps/models/response_custom.dart';
import 'package:closeapps/models/user.dart';
import 'package:closeapps/views/inscription.dart';
import 'package:flutter/material.dart';
import 'package:string_validator/string_validator.dart';

class PasswordOublie extends StatefulWidget {
  PasswordOublie({Key? key}) : super(key: key);

  @override
  _PasswordOublieState createState() => _PasswordOublieState();
}

class _PasswordOublieState extends State<PasswordOublie> {
  final _formKey = GlobalKey<FormState>();
  var apiBaseUrl = "";
  var endpointConnexion = "user/connexion";

  User _user = User();

  String? _email;

  String? _login;

  String? _password;

  bool _autovalidateEmail = false;

  bool _autovalidate = false;
  bool _dynamicShape = true;
  bool _isObscureTextPassword = true;

  bool _isDisable = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(lPasswordOublie),
      ),
      body: Center(
        child: ListView(shrinkWrap: true, children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Form(
              autovalidate: _autovalidate,
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                //crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  /*CircleAvatar(
                    backgroundColor: colorBlueLogo,
                    child: Image.asset(logoWhite),
                  ),
                  Text(
                    lMessageBienvenueConnexion,
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  */
                  //ImageIcon('images/logo.jpeg'),
                  //Utilities.loginFormField(_user, labelText: login),
                  //Utilities.passwordFormField(_user, labelText: password),

                  TextFormField(
                    onSaved: (input) {
                      _user.email = input!.trim();
                    }, // recuperation des données saisies
                    onChanged: (value) {
                      setState(() {
                        _dynamicShape = !_dynamicShape;
                      });
                    },
                    validator: (value) {
                      if (value != null && value.isEmpty) {
                        return erreurSaisieTexte;
                      }
                      if (!isEmail(value!)) {
                        return erreurSaisieEmail;
                      }
                      return null;
                      //Utilities.validator(value, typeContentToValidate: typeText);
                    },
                    decoration: Utilities.getDefaultInputDecoration(
                        labelText: lEmail, isRequiredChamp: true),

                    autovalidate: _autovalidateEmail,
                  ),

                  Utilities.getDefaultSizeBoxWithHeight(),

                  SizedBox(
                    width: double.infinity,
                    child: Utilities.getButtonSubmit(
                        // shape: (_dynamicShape)? Utilities.getShape(topLeft: 50, bottomRight: 50): Utilities.getShape(bottomLeft: 50, topRight: 50),
                        //child: Text(lEnvoyer),
                        libelle: lEnvoyer,
                        onPressed: _submit,
                        isDisable: _isDisable),
                  ),
                ],
              ),
            ),
          ),
        ]),
      ),
    );
  }

  _submit() async {
    if (_formKey.currentState!.validate()) {
      setState(() {
        // griser button
        _isDisable = true;
      });
      Utilities.loadingIndicator(context: context, color: colorRedLogo);

      // permet d'executer toutes les validator des TextFormField
      _formKey.currentState!.save();

      print(_user.toString());

      RequestCustom request = RequestCustom();

      _user.isUserMobile = true ;
      request.data = _user;

      String uri = user + passwordOublier;
      ResponseCustom response =
          await baseApi.appelApi(endpointApi: uri, request: request);

      Navigator.pop(context); // pour retirer le progressing

      if (response != null) {
        if (response.hasError != null && !response.hasError!) {
          Utilities.messageApi(response: response);
          /*
          await Utilities.showDialogCustom(
              context: context,
              titre: "Message",
              contenu: notificationPasswordOublie);
          */

          Utilities.getToast(notificationPasswordOublie);
          Utilities.resetAndOpenPage(context: context);
        } else {
          // verifier
          Utilities.messageApi(response: response);
        }
      }

      setState(() {
        // degriser button
        _isDisable = false;
      });
    } else {
      setState(() {
        _autovalidate = true;
      });
    }
  }
}
