import 'package:closeapps/helpers/constants.dart';
import 'package:flutter/material.dart';

class TemplateAfficherText extends StatelessWidget {
  String contenu;
  Color color;
  TemplateAfficherText(
      {Key? key, required this.contenu, this.color = colorRedLogo})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
          contenu,
          style: TextStyle(
            color: color,
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
