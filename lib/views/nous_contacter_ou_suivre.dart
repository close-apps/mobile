import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/helpers/communications_service.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/service_locator.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class NousContacterOuNouSuivre extends StatelessWidget {
  final String title;
  final bool isNousContacter;
  const NousContacterOuNouSuivre(
      {Key? key, required this.title, this.isNousContacter = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final CommunicationService _communicationService =
        locator<CommunicationService>();
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: isNousContacter
                ? [
                    Image.asset(logoBlue),
                    Utilities.getDefaultSizeBoxWithHeight(),
                    Text("Cliquez sur une option pour l'utiliser",
                        style: Utilities.style(fontSize: 16)),
                    Utilities.getDefaultSizeBoxWithHeight(),
                    ListTile(
                      title: Text(lTelephoneClose),
                      leading: Icon(Icons.call),
                      onTap: () {
                        _communicationService.call(telephoneClose);
                      },
                    ),
                    Divider(),
                    ListTile(
                      title: Text(lTelephoneClose),
                      leading: Icon(Icons.message),
                      onTap: () {
                        _communicationService.sendSms(telephoneClose);
                      },
                    ),
                    Divider(),
                    ListTile(
                      title: Text(lEmailClose),
                      leading: Icon(Icons.email),
                      onTap: () {
                        _communicationService.sendEmail(emailClose);
                      },
                    ),
                    Divider(),
                    ListTile(
                      title: Text(lWhatsappClose),
                      leading: Icon(FontAwesomeIcons.whatsapp),
                      onTap: () {
                        _communicationService.whatsapp(whatsappClose);
                      },
                    ),
                    Divider(),
                    ListTile(
                      title: Text(lFacebookClose),
                      leading: Icon(Icons.facebook),
                      onTap: () {
                        _communicationService.facebook(facebookClose);
                      },
                    ),
                  ]
                : [
                    Image.asset(logoBlue),
                    Utilities.getDefaultSizeBoxWithHeight(),
                    Text("Cliquez sur une option pour l'utiliser",
                        style: Utilities.style(fontSize: 16)),
                    Utilities.getDefaultSizeBoxWithHeight(),
                    ListTile(
                      title: Text(lTwitterClose),
                      leading: Icon(FontAwesomeIcons.twitter),
                      onTap: () {
                        _communicationService.twitter(twitter: twitterClose);
                      },
                    ),
                    Divider(),
                    ListTile(
                      title: Text(lFacebookClose2),
                      leading: Icon(Icons.facebook),
                      onTap: () {
                        _communicationService.facebook(facebookClose);
                      },
                    ),
                  ],
          ),
        ),
      ),
    );
  }
}
