import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/fidelite_carte.dart';
import 'package:closeapps/views/souscrire_acarte.dart';
import 'package:flutter/material.dart';

class InscriptionPdFCarte extends StatefulWidget {
  FideliteCarte? carte;
  InscriptionPdFCarte({Key? key, this.carte}) : super(key: key);

  @override
  _InscriptionPdFCarteState createState() => _InscriptionPdFCarteState();
}

class _InscriptionPdFCarteState extends State<InscriptionPdFCarte> {
  //bool _accepterCondition = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CustomScrollView(
      shrinkWrap: true,
      slivers: <Widget>[
        SliverAppBar(
          backgroundColor:
              widget.carte != null && widget.carte!.enseigneCouleur != null
                  ? Color(widget.carte!.enseigneCouleur!)
                  : colorBlueLogo,
          expandedHeight: expandedHeight,
          floating: false,
          pinned: true,

          /*
          title: Text(
            widget.carte != null ? widget.carte.title : "Aucune Libelle",
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: Utilities.style(
              color: colorWitheLogo,
            ),
          ),*/

          //textTheme: TextTheme(headline1:Utilities.style() ) ,
          flexibleSpace: FlexibleSpaceBar(
            collapseMode: CollapseMode.none,
            background: (widget.carte != null && widget.carte!.urlLogo != null)
                ? Image.network(
                    widget.carte!.urlLogo,
                    //widget.carte.urlLogo,

                    fit: BoxFit.fill,
                    //width: double.infinity,
                    //height: double.infinity,
                  )
                : Image.asset(
                    logoBlue,
                    fit: BoxFit.fill,
                    // width: double.infinity,
                    // height: double.infinity,
                  ),

            // Image.asset(
            //   widget.carte != null && widget.carte.urlLogo != null
            //       ? widget.carte.urlLogo
            //       : logoBlue,
            //   fit: BoxFit.fill,
            //   //width: double.infinity,
            //   //height: expandedHeight,
            // ),
            title: Text(
              widget.carte != null && widget.carte!.libelle != null
                  ? widget.carte!.libelle!
                  : "Aucun Libelle",
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: Utilities.style(
                color: colorWitheLogo,
                //fontSize: 20
              ),
            ),
          ),
        ),
        SliverFillRemaining(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            //mainAxisSize: MainAxisSize.max,
            //verticalDirection: VerticalDirection.down,
            //crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: GridView.count(
                      physics: NeverScrollableScrollPhysics(),
                      padding: EdgeInsets.all(8.0),
                      shrinkWrap:
                          true, // limite la heigth de l'element a son contenu et non a la heigth du parent
                      //scrollDirection: Axis.horizontal,
                      crossAxisCount: 2,
                      children: <Widget>[
                        InkWell(
                          splashColor: colorBlueLogo,
                          onTap: () {
                            // creation de la souscription

                            validerCondition(
                              context: context,
                              view: SouscrireACarte(carte: widget.carte!),
                              model: widget.carte,
                            );

                            //_accepterCondition = true;
                            // _accepterCondition
                            //     ? Utilities.navigatorPush(
                            //         context: context,
                            //         view: SouscrireACarte(carte: widget.carte))
                            //     : Utilities.getToast(lValiderConditions);
                          },
                          child: Card(
                            borderOnForeground: false,
                            child: Center(
                              child: Text(
                                lSincrireAuProgramme,
                                style: style20BlueBold,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ),
                        InkWell(
                          splashColor: colorBlueLogo,
                          onTap: () {
                            validerCondition(
                              context: context,
                            );

                            // Utilities.getToast(_accepterCondition
                            //     ? lAVenir
                            //     : lValiderConditions);
                          },
                          child: Card(
                            borderOnForeground: true,
                            child: Center(
                              child: Text(
                                lScannerMaCarte,
                                style: style20BlueBold,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
              // InkWell(
              //   onTap: () {
              //     validerCondition(context: context);
              //   },
              //   child: Center(
              //     child: Text(lConditionsDutilisations),
              //   ),
              // ),
              //Text(lConditionsDutilisations),
            ],
          ),
        )
      ],
    )
        /*Column(
          children: <Widget>[
            Expanded(
              child: Row(
                //shrinkWrap: true,
                children: <Widget>[
                 
                  Expanded(
                    child: GridView.count(
                      //scrollDirection: Axis.horizontal,
                      crossAxisCount: 2,
                      children: <Widget>[
                        Card(
                          child: Center(
                            child: Text("data"),
                          ),
                        ),
                        Card(
                          child: Center(
                            child: Text("data"),
                          ),
                        ),
                      ],
                    ),
                  ),

                  
                  // Banner(
                  //     message: "Je suis une BennerBenner",
                  //     location: BannerLocation.bottomEnd),
                  // Card(
                  //   child: Center(
                  //     child: Text("datadata"),
                  //   ),
                  // ),
                  // Row(
                  //   children: <Widget>[
                  //     Card(
                  //       child: Center(
                  //         child: Text("data"),
                  //       ),
                  //     ),
                  //     Spacer(),
                  //     Card(
                  //         child: Center(
                  //       child: Text("data"),
                  //     ))
                  //   ],
                  // ),
                  // Banner(              
                  //   textDirection: TextDirection.rtl,
                  //   child: Text("datadata"),
                  //     message: "Je suis une BennerBenner",
                  //     location: BannerLocation.bottomStart),
                      
                ],
              ),
            ),
            Center(child: Text(lConditionsDutilisations)),
          ],
        )*/

        /*GridView.count(
        crossAxisCount: 2,
        children: <Widget>[
          Card(
            child: Center(
              child: Text("data"),
            ),
          ),
          Card(
            child: Center(
              child: Text("data"),
            ),
          ),
          Center(
            child: Text(lConditionsDutilisations),
          ),
        ],
      ),*/
        );
  }

  void validerConditionOld({required BuildContext context, Function? function}) {
    Utilities.alertDialogCustom(
        context: context,
        title: questionConditionsDutilisation,
        content: defaultConditionsDutilisation,
        rightFuction: () {
          // setState(() {
          //   _accepterCondition = true;
          // });
          Navigator.pop(context);

          //function();
          Utilities.getToast(lAccepterConditions);
        },
        leftFuction: () {
          // setState(() {
          //   _accepterCondition = false;
          // });
          Navigator.pop(context);
          Utilities.getToast(lValiderConditions);

          // Utilities.getToast(
          //     "Merci d'avoir lu nos conditons. A la prochaine !!!");
        });
  }

  void validerCondition({required BuildContext context, dynamic view, dynamic model}) {
    Utilities.alertDialogCustom(
        context: context,
        title: questionConditionsDutilisation,
        //content: lorem(paragraphs: 2, words: 100),
        content: (model != null && model.conditionsDutilisation != null)
            ? model.conditionsDutilisation
            : lDefaultConditionsDutilisation,
        rightFuction: () {
          // setState(() {
          //   _accepterCondition = true;
          // });
          Navigator.pop(context);

          if (view != null) {
            Utilities.navigatorPush(context: context, view: view);
          } else {
            Utilities.getToast(lAVenir);
          }
          //function();
          Utilities.getToast(lAccepterConditions);
        },
        leftFuction: () {
          // setState(() {
          //   _accepterCondition = false;
          // });
          Navigator.pop(context);
          Utilities.getToast(lValiderConditions);
        },
        isHtmlContent: true);
  }

  souscrireAuPDF() {}
}
