import 'dart:async';

import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/fidelite_carte.dart';
import 'package:closeapps/models/user.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';

class GoogleMapView extends StatefulWidget {
  FideliteCarte informationsPDVDuPDF;
  List<FideliteCarte>? listPDVDuPDF;
  bool isPDFTampon;
  GoogleMapView(
      {Key? key,
      required this.informationsPDVDuPDF,
      this.isPDFTampon = true,
      this.listPDVDuPDF})
      : super(key: key);

  @override
  _GoogleMapViewState createState() => _GoogleMapViewState();
}

class _GoogleMapViewState extends State<GoogleMapView> {
// Object for PolylinePoints
  late PolylinePoints polylinePoints;

// List of coordinates to join
  List<LatLng> polylineCoordinates = [];

// Map storing polylines created by connecting two points
  Map<PolylineId, Polyline> polylines = {};

  //Completer<GoogleMapController> _controller = Completer();
  late GoogleMapController
      _controller; // il sera initailiser apres avant d'etre utiliser
  Location location = new Location();
  LatLng _initialcameraposition = LatLng(0, 0);

  List<Marker> markers = [];

  bool? etatLocation;
  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  static final CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(37.43296265331129, -122.08832357078792),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //getAutorisationLocation();
    initMarkers();

    _determinePosition();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    //free brightness (it works only on Android):
    _controller.dispose();
  }

  initMarkers() {
    Utilities.begin("GoogleMapView/initMarkers");
    print("listPDVDuPDF ::: ${widget.listPDVDuPDF}");
    if (widget.listPDVDuPDF != null) {
      widget.listPDVDuPDF!.forEach((pdvPDF) {
        print("initMarkers ::: ${pdvPDF.longitude} / ${pdvPDF.latitude}");

        if (pdvPDF.longitude != null && pdvPDF.latitude != null) {
          setState(() {
            markers.add(Marker(
              markerId: MarkerId('${pdvPDF.id}'),
              icon: BitmapDescriptor.defaultMarkerWithHue(
                  BitmapDescriptor.hueRed),
              infoWindow: InfoWindow(title: pdvPDF.pointDeVenteNom),
              position: LatLng(pdvPDF.latitude!, pdvPDF.longitude!),
            ));
          });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    print("build/etatLocation ::: $etatLocation");

    return new Scaffold(
      appBar: AppBar(
          title: Text(
        (widget.isPDFTampon
                ? (widget.informationsPDVDuPDF.programmeFideliteTamponLibelle !=
                        null
                    ? widget
                        .informationsPDVDuPDF.programmeFideliteTamponLibelle!
                    : "CLOSE APP")
                : (widget.informationsPDVDuPDF.programmeFideliteCarteLibelle !=
                        null
                    ? widget.informationsPDVDuPDF.programmeFideliteCarteLibelle!
                    : "CLOSE APP")) +
            sautDelLine +
            (widget.informationsPDVDuPDF.pointDeVenteNom != null
                ? widget.informationsPDVDuPDF.pointDeVenteNom
                : ""),
      )),
      body: etatLocation == null
          ? Center(child: CircularProgressIndicator())
          : etatLocation!
              ? GoogleMap(
                  zoomGesturesEnabled: true,
                  zoomControlsEnabled: false,

                  mapType: MapType.normal,
                  myLocationEnabled: true,
                  myLocationButtonEnabled: true,
                  initialCameraPosition:
                      CameraPosition(target: _initialcameraposition, zoom: 12),
                  // onMapCreated: (GoogleMapController controller) {
                  //   _controller.complete(controller);
                  // },

                  onMapCreated: _onMapCreated,
                  onTap: (latLng) {},
                  // markers: markers.map((e) => e).toSet(),
                  markers: Set<Marker>.from(markers),
                  polylines: Set<Polyline>.of(
                      polylines.values), // pour le tracages entre deux points
                )
              : Container(),
      // floatingActionButton: FloatingActionButton.extended(
      //   onPressed: _goToTheLake,
      //   label: Text('To the lake!'),
      //   icon: Icon(Icons.directions_boat),
      // ),
    );
  }

  /// Determine the current position of the device.
  ///
  /// When the location services are not enabled or permissions
  /// are denied the `Future` will return an error.
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      etatLocation = false;
      setState(() {});
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        etatLocation = false;
        setState(() {});
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      etatLocation = false;
      setState(() {});
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.

    Position position = await Geolocator.getCurrentPosition();
    _initialcameraposition = LatLng(position.latitude, position.longitude);
    etatLocation = true;
    setState(() {});
    _createPolylines(
        startLatitude: position.latitude,
        startLongitude: position.longitude,
        destinationLatitude: widget.informationsPDVDuPDF.longitude!,
        destinationLongitude: widget.informationsPDVDuPDF.latitude!);
    return position;
  }

  void _onMapCreatedOld(GoogleMapController _cntlr) {
    _controller = _cntlr;
    //_controller.complete(_cntlr);

    location.onLocationChanged.listen((LocationData currentLocation) {
      _controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
              target: LatLng(currentLocation.latitude ?? 0,
                  currentLocation.longitude ?? 0),
              zoom: 15),
        ),
      );
    });
  }

  void _onMapCreated(GoogleMapController _cntlr) {
    _controller = _cntlr;
    //_controller.complete(_cntlr);

    Geolocator.getPositionStream().listen((Position position) {
      //setState(() {
      if (position != null) {
        _controller.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(
                target: LatLng(position.latitude, position.longitude),
                zoom: 15),
          ),
        );
      }
      //});
    });
  }

  Future<LocationData?> getAutorisationLocation() async {
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        etatLocation = false;
        setState(() {});
        return null;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        etatLocation = false;
        setState(() {});
        return null;
      }
    }

    _locationData = await location.getLocation();

    if (_locationData != null &&
        _locationData.latitude != null &&
        _locationData.longitude != null) {
      _initialcameraposition =
          LatLng(_locationData.latitude!, _locationData.longitude!);

      print("_locationData.latitude ::: ${_locationData.latitude}");
      print("_locationData.longitude ::: ${_locationData.longitude}");
    }
    etatLocation = true;
    setState(() {});
    print("etatLocation ::: $etatLocation");
    return _locationData;
  }

  Future<void> _goToTheLake() async {
    _controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  }

  _createPolylines({
    required double startLatitude,
    required double startLongitude,
    required double destinationLatitude,
    required double destinationLongitude,
  }) async {
    Utilities.begin("GoogleMapView/_createPolylines");
    // Initializing PolylinePoints
    polylinePoints = PolylinePoints();

    // Generating the list of coordinates to be used for
    // drawing the polylines
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      API_KEY, // Google Maps API Key
      PointLatLng(startLatitude, startLongitude),
      PointLatLng(destinationLatitude, destinationLongitude),
      travelMode: TravelMode.transit,
    );

    // Adding the coordinates to the list
    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }

    setState(() {
      // Defining an ID
      PolylineId id = PolylineId('poly');

      // Initializing Polyline
      Polyline polyline = Polyline(
        polylineId: id,
        color: Colors.red,
        points: polylineCoordinates,
        width: 3,
      );

      // Adding the polyline to the map
      polylines[id] = polyline;
    }); // le reload apres finition
  }

  // Future<void> _goToTheLakeOld() async {
  //   final GoogleMapController controller = await _controller.future;
  //   controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  // }
}
