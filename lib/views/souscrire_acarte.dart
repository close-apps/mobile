import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/preferences_util.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/fidelite_carte.dart';
import 'package:closeapps/models/request_custom.dart';
import 'package:closeapps/models/response_custom.dart';
import 'package:closeapps/models/user.dart';
import 'package:closeapps/views/home.dart';
import 'package:closeapps/views/souscrire_acarte_avec_infos_parrain.dart';
import 'package:flutter/material.dart';

class SouscrireACarte extends StatefulWidget {
  FideliteCarte carte;
  SouscrireACarte({Key? key, required this.carte}) : super(key: key);

  @override
  _SouscrireACarteState createState() => _SouscrireACarteState();
}

class _SouscrireACarteState extends State<SouscrireACarte> {
  User _user = User();
  bool _isDisable = false;
  //bool autovalidateTelphone = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Souscription ${widget.carte != null && widget.carte.libelle != null ? widget.carte.libelle : 'au programme'}",
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
        ),
      ),
      body: Center(
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                lAvezVousEteParrainer,
                style: Utilities.style(),
              ),
              Utilities.getDefaultSizeBoxWithHeight(),
              SizedBox(
                width: double.infinity,
                child: Utilities.getButtonSubmit(
                    // shape: (_dynamicShape)? Utilities.getShape(topLeft: 50, bottomRight: 50): Utilities.getShape(bottomLeft: 50, topRight: 50),
                    //child: Text(lInscription),
                    libelle: lReponseOui,
                    onPressed: () {
                      Utilities.navigatorPush(
                          context: context,
                          view: SouscrireACarteAvecInfosParrain(
                              carte: widget.carte));
                    },
                    isDisable: _isDisable),
              ),
              Utilities.getDefaultSizeBoxWithHeight(),
              SizedBox(
                width: double.infinity,
                child: Utilities.getButtonSubmit(
                    // shape: (_dynamicShape)? Utilities.getShape(topLeft: 50, bottomRight: 50): Utilities.getShape(bottomLeft: 50, topRight: 50),
                    //child: Text(lInscription),
                    libelle: lReponseNon,
                    onPressed: _submit,
                    isDisable: _isDisable),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _submit() async {
    setState(() {
      // griser button
      _isDisable = true;
    });
    Utilities.loadingIndicator(context: context, color: colorRedLogo);

    // ajout des informations
    _user.carteId = await PreferencesUtil.instance.getInt(carteId);
    //_user.carteId = await Utilities.carteIdUserConnected;

    _user.programmeFideliteCarteId =
        widget.carte != null ? widget.carte.id : null;
    RequestCustom request = RequestCustom();
    request.user = await PreferencesUtil.instance.getInt(id);
    request.datas = [_user];

    ResponseCustom response = await baseApi.appelApi(
        endpointApi: souscriptionProgrammeFideliteCarte + create,
        request: request);

    Navigator.pop(context); // pour retirer le progressing

    if (response != null && response.hasError != null && !response.hasError!) {
      Utilities.getToast(succes);
      Utilities.resetAndOpenPage(context: context, view: Home());
    } else {
      Utilities.messageApi(response: response);
    }

    setState(() {
      // degriser button
      _isDisable = false;
    });
  }
}
