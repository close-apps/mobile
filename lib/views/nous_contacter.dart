import 'dart:io';

import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class NousContacter extends StatefulWidget {
  const NousContacter({Key? key}) : super(key: key);

  @override
  State<NousContacter> createState() => _NousContacterState();
}

class _NousContacterState extends State<NousContacter> {
  String url = "https://tawk.to/chat/61421abdd326717cb681a030/1ffl3gubf";
  @override
  void initState() {
    super.initState();
    // Enable hybrid composition.
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WebView(
        // initialUrl: 'https://tawk.to',
        initialUrl: 'https://tawk.to/chat/61421abdd326717cb681a030/1ffl3gubf',
      ),
      appBar: AppBar(
        title: Text("Chat Close"),
      ),
    );
  }
}
