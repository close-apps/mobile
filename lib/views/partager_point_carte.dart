import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/preferences_util.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/fidelite_carte.dart';
import 'package:closeapps/models/request_custom.dart';
import 'package:closeapps/models/response_custom.dart';
import 'package:closeapps/models/user.dart';
import 'package:closeapps/views/partage_point_ok.dart';
import 'package:flutter/material.dart';
import 'package:string_validator/string_validator.dart';

class PartagerPointCarte extends StatefulWidget {
  PartagerPointCarte(
      {Key? key,
      this.souscriptionPdfCarte,
      required this.programmeFideliteCarteId})
      : super(key: key);
  int programmeFideliteCarteId;
  FideliteCarte? souscriptionPdfCarte;

  @override
  _PartagerPointCarteState createState() => _PartagerPointCarteState();
}

class _PartagerPointCarteState extends State<PartagerPointCarte> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController _controllerPoint = new TextEditingController();
  TextEditingController _controllerTelephone = new TextEditingController();
  TextEditingController _controllerEmail = new TextEditingController();
  bool _autovalidateForm = false;
  bool _autovalidateEmail = false;
  bool _isDisable = false;

  User _user = new User();

  int xofInitial = 0;
  int xofRetirer = 0;

  int xofApres = 0;
  int nbrePointActuelle = 0;
  int nbrePointRetirer = 0;
  int nbrePointActuelleApres = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    xofInitial = Utilities.getXOF(fideliteCarte: widget.souscriptionPdfCarte);
    xofApres = xofInitial;
    nbrePointActuelle = ((widget.souscriptionPdfCarte != null &&
            widget.souscriptionPdfCarte!.nbrePointActuelle != null)
        ? widget.souscriptionPdfCarte!.nbrePointActuelle!
        : 0);
    nbrePointActuelleApres = nbrePointActuelle;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
        (widget.souscriptionPdfCarte != null &&
                    widget.souscriptionPdfCarte!.libelle != null
                ? widget.souscriptionPdfCarte!.libelle!
                : "CLOSE APP") +
            sautDelLine +
            lPartagerDesPoints,
      )),
      body: SingleChildScrollView(
          padding: EdgeInsets.all(16.0),
          child: Center(
            child: Form(
              key: _formKey,
              autovalidate: _autovalidateForm,
              //autovalidateMode: AutovalidateMode.always,
              //autovalidateMode: AutovalidateMode.always,
              child: Column(
                children: [
                  // Text(
                  //   "Nombre de points : ${widget.pdfCarte != null && widget.pdfCarte.nbrePointObtenu != null ? widget.pdfCarte.nbrePointObtenu : 0} -- XOF",
                  //   style: Utilities.style(),
                  // ),
                  Text(
                    "$nbrePointActuelle" +
                        espace +
                        lPoints +
                        tiret +
                        '$xofInitial' +
                        espace +
                        lXOF,
                    style: Utilities.style(),
                  ),
                  Divider(
                    height: 30,
                  ),
                  Utilities.getDefaultSizeBoxWithHeight(),
                  TextFormField(
                    //autovalidate: true,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    controller: _controllerTelephone,
                    onSaved: (input) {
                      _user.telephoneReceveur = input!.trim();
                    }, // recuperation des données saisies
                    onChanged: (value) {},
                    validator: (value) {
                      if ((value == null || value.isEmpty) &&
                          _controllerEmail.text.length == 0) {
                        return erreurSaisieTexte;
                      }
                      if (value != null &&
                          value.isNotEmpty &&
                          !RegExp(r'^[0-9]+$').hasMatch(value)) {
                        return erreurValeurSaisie;
                      }
                      return null;
                      //Utilities.validator(value, typeContentToValidate: typeText);
                    },
                    decoration: Utilities.getDefaultInputDecoration(
                      labelText: lTelephoneReceveur,
                    ),
                    keyboardType: TextInputType.phone, // le type du input
                  ),
                  Utilities.getDefaultSizeBoxWithHeight(),
                  TextFormField(
                    //autovalidate: _autovalidateEmail,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    controller: _controllerEmail,
                    onSaved: (input) {
                      _user.emailReceveur = input!.trim();
                    },
                    onChanged: (value) {
                      // if (!isEmail(value)) {
                      //   setState(() {
                      //     _autovalidateEmail = true;
                      //   });
                      // }
                      //_changeDynamicShape();
                    },
                    validator: (value) {
                      // if (value == null || value.isEmpty) {
                      //   return erreurSaisieTexte;
                      // }

                      if ((value == null || value.isEmpty) &&
                          _controllerTelephone.text.length == 0) {
                        return erreurSaisieTexte;
                      }
                      if (value != null &&
                          value.isNotEmpty &&
                          !isEmail(value)) {
                        return erreurSaisieEmail;
                      }
                      return null;
                    },
                    decoration: Utilities.getDefaultInputDecoration(
                      labelText: lEmailReceveur,
                    ),
                    keyboardType:
                        TextInputType.emailAddress, // le type du input
                  ),
                  Utilities.getDefaultSizeBoxWithHeight(),
                  TextFormField(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    controller: _controllerPoint,
                    textAlign: TextAlign.center,
                    onSaved: (input) {
                      _user.pointPartager = input!.trim();
                    },

                    onChanged: (value) {
                      calculValeurVariable(value: value);
                    }, // recuperation des données saisies
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        //_isDisable = true;
                        return erreurSaisieTexte;
                      }
                      if (value.isNotEmpty &&
                          !RegExp(r'^[0-9]+$').hasMatch(value)) {
                        return erreurValeurSaisie;
                      }
                      if (int.parse(value) == 0) {
                        //_isDisable = true;
                        return erreurValeurSaisie;
                      }
                      // if (comparerSiSommeEstValide(
                      //     value: int.parse(value),
                      //     nbrePointActuelle: nbrePointActuelle)) {
                      if (int.parse(value) > nbrePointActuelle) {
                        //_isDisable = true;
                        return erreurPointAPartager;
                      }
                      return null;
                      //Utilities.validator(value, typeContentToValidate: typeText);
                    },
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(20.0)),
                      labelText: lPoints,

                      prefixIcon: IconButton(
                          icon: Icon(Icons.remove),
                          onPressed: () {
                            // if (_controllerPoint.text != null &&
                            //     int.parse(_controllerPoint.text) > 0) {
                            //   _controllerPoint.text =
                            //       "${int.parse(_controllerPoint.text) - 1}";
                            // }
                            Utilities.decrementer(_controllerPoint);
                            calculValeurVariable();
                          }),
                      suffixIcon: IconButton(
                          icon: Icon(Icons.add_box),
                          onPressed: () {
                            // if (_controllerPoint.text != null &&
                            //     int.parse(_controllerPoint.text) > 0) {
                            //   _controllerPoint.text =
                            //       "${int.parse(_controllerPoint.text) + 1}";
                            // }
                            Utilities.incrementer(_controllerPoint);
                            calculValeurVariable();
                          }),
                    ),
                    keyboardType: TextInputType.number, // le type du input
                  ),
                  Utilities.getDefaultSizeBoxWithHeight(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(lMin),
                      //Text(lXOF),
                      Text('$xofRetirer ' + lXOF),
                      //Text('$nbrePointRetirer ' + lPoints),

                      Text(lMax),
                    ],
                  ),
                  Utilities.getDefaultSizeBoxWithHeight(),

                  Text(
                    "Après depense: $nbrePointActuelleApres" +
                        espace +
                        lPoints +
                        tiret +
                        '$xofApres' +
                        espace +
                        lXOF,
                    style: Utilities.style(),
                  ),
                  Utilities.getDefaultSizeBoxWithHeight(height: 20),
                  TextFormField(
                    onSaved: (input) {
                      _user.description = input!.trim();
                    },
                    onChanged: (value) {}, // recuperation des données saisies

                    decoration: Utilities.getDefaultInputDecoration(
                      labelText: lDescription,
                    ),
                    keyboardType: TextInputType.multiline, // le type du input
                    maxLines: 8,
                  ),
                  Utilities.getButtonSubmit(
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          Utilities.alertDialogCustom(
                              libelleLeftFuction: lAnnuler,
                              libelleRightFuction: lConfirmer,
                              rightFuction: () {
                                Navigator.pop(
                                    context); //  clean la boite de dialogue
                                _submit();
                              },
                              leftFuction: () {
                                Navigator.pop(context);
                              },
                              context: context,
                              title: lConfirmation,
                              content:
                                  Utilities.lContenuConfirmationPartagePoints(
                                points: _controllerPoint.text,
                                receveur: _controllerTelephone.text,
                              ));
                        } else {
                          setState(() {
                            _autovalidateForm = true;
                          });
                        }
                      },
                      //child: Text(lSuivant),
                      libelle: lSuivant,
                      isDisable: _isDisable),
                ],
              ),
            ),
          )),
    );
  }

  _submit() async {
    setState(() {
      // griser button
      _isDisable = true;
    });
    //Utilities.loadingIndicator(context: context, color: colorRedLogo); // en attente
    Utilities.loadingIndicator(
        context: context, color: colorRedLogo); // en attente

    _formKey.currentState!.save();
    // log data
    Utilities.log(_user);

    // get UserConnectId
    _user.programmeFideliteCarteId = widget.programmeFideliteCarteId;
    //_user.carteId = await Utilities.carteIdUserConnected;
    _user.carteId = await PreferencesUtil.instance.getInt(carteId);

    RequestCustom request = RequestCustom();
    //request.user = await Utilities.userIdConnected;

    request.user = await PreferencesUtil.instance.getInt(id);
    request.datas = [_user];

    ResponseCustom response = await baseApi.appelApi(
        endpointApi: souscriptionProgrammeFideliteCarte + partagerPoint,
        request: request);

    Navigator.pop(context); // lever loadingIndicator
    if (response != null && response.hasError != null && !response.hasError!) {
      // maj de la bd locale
      Utilities.navigatorPush(context: context, view: PartagePointOk());
    } else {
      Utilities.messageApi(response: response);
    }

    setState(() {
      // degriser button
      _isDisable = false;
    });
  }

  comparerSiSommeEstValide(
      {required int value, required int nbrePointActuelle}) {
    int nbreDePoint = 0;
    if (widget.souscriptionPdfCarte != null &&
        widget.souscriptionPdfCarte!
                .programmeFideliteCarteCorrespondanceNbrePoint !=
            null &&
        widget.souscriptionPdfCarte!.programmeFideliteCarteNbrePointObtenu !=
            null) {
      nbreDePoint = ((widget.souscriptionPdfCarte!
                      .programmeFideliteCarteNbrePointObtenu! *
                  value) /
              widget.souscriptionPdfCarte!
                  .programmeFideliteCarteCorrespondanceNbrePoint!)
          .round();
    }

    return nbreDePoint > nbrePointActuelle;
  }

  // calcul des valeurs variables
  calculValeurVariableNew({value}) {
    Utilities.begin("calculValeurVariable value: ");
    // donnee saisi
    //if (value.isNotEmpty) {
    if (_controllerPoint.text != null &&
        _controllerPoint.text.isNotEmpty &&
        RegExp(r'^[0-9]+$').hasMatch(_controllerPoint.text)) {
      //int? nbrePoint = int.parse(_controllerPoint.text);
      int? xofSaisie = int.parse(_controllerPoint.text);

      int nbreDePoint = 0;
      if (widget.souscriptionPdfCarte != null &&
          widget.souscriptionPdfCarte!
                  .programmeFideliteCarteCorrespondanceNbrePoint !=
              null &&
          widget.souscriptionPdfCarte!.programmeFideliteCarteNbrePointObtenu !=
              null) {
        nbreDePoint = ((widget.souscriptionPdfCarte!
                        .programmeFideliteCarteNbrePointObtenu! *
                    xofSaisie) /
                widget.souscriptionPdfCarte!
                    .programmeFideliteCarteCorrespondanceNbrePoint!)
            .round();

        xofRetirer = ((nbreDePoint *
                    widget.souscriptionPdfCarte!
                        .programmeFideliteCarteCorrespondanceNbrePoint!) /
                widget.souscriptionPdfCarte!
                    .programmeFideliteCarteNbrePointObtenu!)
            .round() // arrondir
            .toInt();
      }

      nbrePointRetirer = nbreDePoint;
      xofApres = (xofInitial >= xofRetirer) ? xofInitial - xofRetirer : 0;
      nbrePointActuelleApres = (nbrePointActuelle >= nbreDePoint)
          ? nbrePointActuelle - nbreDePoint
          : 0;
    } else {
      nbrePointActuelleApres = nbrePointActuelle;
      xofApres = xofInitial;
      xofRetirer = 0;
      nbrePointRetirer = 0;
    }
    setState(() {});
  }

  // calcul des valeurs variables
  calculValeurVariable({value}) {
    Utilities.begin("calculValeurVariable value: ");
    // donnee saisi
    //if (value.isNotEmpty) {
    if (_controllerPoint.text != null &&
        _controllerPoint.text.isNotEmpty &&
        RegExp(r'^[0-9]+$').hasMatch(_controllerPoint.text)) {
      int nbrePoint = int.parse(_controllerPoint.text);

      nbrePointActuelleApres =
          (nbrePointActuelle >= nbrePoint) ? nbrePointActuelle - nbrePoint : 0;

      if (widget.souscriptionPdfCarte != null &&
          widget.souscriptionPdfCarte!
                  .programmeFideliteCarteCorrespondanceNbrePoint !=
              null &&
          widget.souscriptionPdfCarte!.programmeFideliteCarteNbrePointObtenu !=
              null) {
        xofRetirer = ((nbrePoint *
                    widget.souscriptionPdfCarte!
                        .programmeFideliteCarteCorrespondanceNbrePoint!) /
                widget.souscriptionPdfCarte!
                    .programmeFideliteCarteNbrePointObtenu!)
            .round() // arrondir
            .toInt();

        xofApres = (xofInitial >= xofRetirer) ? xofInitial - xofRetirer : 0;
      }
    } else {
      nbrePointActuelleApres = nbrePointActuelle;
      xofApres = xofInitial;
      xofRetirer = 0;
    }

    setState(() {});
  }
}
