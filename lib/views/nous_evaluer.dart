import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/preferences_util.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/request_custom.dart';
import 'package:closeapps/models/response_custom.dart';
import 'package:closeapps/models/user.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

class NousEvaluer extends StatefulWidget {
  NousEvaluer({Key? key}) : super(key: key);

  @override
  _NousEvaluerState createState() => _NousEvaluerState();
}

class _NousEvaluerState extends State<NousEvaluer> {
  User _user = new User();

  bool isUpdate = false;
  bool _isDisable = false;
  bool _isLoading = true;

  int nbreEtoileSelectionner = 0;

  TextEditingController _controllerTextEditing = new TextEditingController();

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    Utilities.begin("NousEvaluer/didChangeDependencies");

    initData();
  }

  @override
  Widget build(BuildContext context) {
    Utilities.begin("NousEvaluer/build");
    return Scaffold(
      appBar: AppBar(
        title: Text(lNousEvaluer),
      ),
      body: _isLoading
          ? Center(child: CircularProgressIndicator())
          : SingleChildScrollView(
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(children: [
                    Text(
                      introductionNousEvaluer,
                      style: Utilities.style(),
                    ),
                    Utilities.getDefaultSizeBoxWithHeight(),
                    //Form(child: child)
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                            onPressed: () {
                              _user.nbreEtoile = 1;
                              setState(() {});
                            },
                            icon: Icon(
                              Icons.star,
                              color: _user.nbreEtoile != null &&
                                      _user.nbreEtoile! >= 1
                                  ? colorRedLogo
                                  : colorBlueLogo,
                            )),
                        IconButton(
                            onPressed: () {
                              _user.nbreEtoile = 2;
                              setState(() {});
                            },
                            icon: Icon(
                              Icons.star,
                              color: _user.nbreEtoile != null &&
                                      _user.nbreEtoile! >= 2
                                  ? colorRedLogo
                                  : colorBlueLogo,
                            )),
                        IconButton(
                            onPressed: () {
                              _user.nbreEtoile = 3;
                              setState(() {});
                            },
                            icon: Icon(
                              Icons.star,
                              color: _user.nbreEtoile != null &&
                                      _user.nbreEtoile! >= 3
                                  ? colorRedLogo
                                  : colorBlueLogo,
                            )),
                        IconButton(
                            onPressed: () {
                              _user.nbreEtoile = 4;
                              setState(() {});
                            },
                            icon: Icon(
                              Icons.star,
                              color: _user.nbreEtoile != null &&
                                      _user.nbreEtoile! >= 4
                                  ? colorRedLogo
                                  : colorBlueLogo,
                            )),
                        IconButton(
                            onPressed: () {
                              _user.nbreEtoile = 5;
                              setState(() {});
                            },
                            icon: Icon(
                              Icons.star,
                              color: _user.nbreEtoile != null &&
                                      _user.nbreEtoile! >= 5
                                  ? colorRedLogo
                                  : colorBlueLogo,
                            )),
                      ],
                    ),
                    Utilities.getDefaultSizeBoxWithHeight(),
                    TextFormField(
                      controller: _controllerTextEditing,
                      onSaved: (input) {},
                      onChanged: (value) {
                        _user.commentaire = value;
                      }, // recuperation des données saisies

                      decoration: Utilities.getDefaultInputDecoration(
                        labelText: lCommentaire,
                      ),
                      keyboardType: TextInputType.multiline, // le type du input
                      maxLines: 8,
                    ),
                    Utilities.getDefaultSizeBoxWithHeight(),

                    ElevatedButton(
                      onPressed: _isDisable ? null : () => _submit(),
                      //onPressed: () => _submit(),
                      child: Text(isUpdate ? lModifier : lEnvoyer),
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(colorRedLogo),
                          shape: MaterialStateProperty.all(
                              Utilities.getDefaultShape())),
                    )
                  ])),
            ),
    );
  }

  // pour recuperer l'ancienne notation
  initData() async {
    Utilities.begin("initData");

    _user.carteId = await PreferencesUtil.instance.getInt(carteId);
    _user.userId = await PreferencesUtil.instance.getInt(id);

    RequestCustom request = RequestCustom();
    //request.user = await Utilities.userIdConnected;

    request.data = _user;

    ResponseCustom response = await baseApi.appelApi(
        endpointApi: notesClose + getByCriteria, request: request);

    if (response != null &&
        response.hasError != null &&
        !response.hasError! &&
        response.items != null &&
        response.items!.length > 0) {
      _user = User.fromJson(response.items![0]);
      isUpdate = true;
      _controllerTextEditing.text = _user.commentaire ?? "";
    }
    _isLoading = false;
    setState(() {}); // pr reload les nouvelles valeurs
  }

  _submit() async {
    Utilities.begin("_submit");
    if (_user.nbreEtoile == null || _user.nbreEtoile == 0) {
      Utilities.getToast("Veuillez sellectioner un nombre d'étoile");
    } else {
      // envoi de données a l'API

      setState(() {
        // griser button
        _isDisable = true;
      });
      //Utilities.loadingIndicator(context: context, color: colorRedLogo); // en attente
      Utilities.loadingIndicator(
          context: context, color: colorRedLogo); // en attente

      // log data
      Utilities.log(_user);

      // get UserConnectId
      //_user.carteId = await Utilities.carteIdUserConnected;
      _user.carteId = await PreferencesUtil.instance.getInt(carteId);
      _user.userId = await PreferencesUtil.instance.getInt(id);

      RequestCustom request = RequestCustom();
      //request.user = await Utilities.userIdConnected;

      request.user = _user.userId;
      request.datas = [_user];

      ResponseCustom response = await baseApi.appelApi(
          endpointApi: notesClose + (isUpdate ? update : create),
          request: request);

      Navigator.pop(context); // lever loadingIndicator
      if (response != null &&
          response.hasError != null &&
          !response.hasError!) {
        // maj de la bd locale
        Utilities.getToast(succes);
        Navigator.pop(context); // lever le form de nous evaluer
      } else {
        Utilities.messageApi(response: response);
      }

      setState(() {
        // degriser button
        _isDisable = false;
      });
    }
  }
}
