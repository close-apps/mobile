import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/views/home.dart';
import 'package:flutter/material.dart';

class PartagePointOk extends StatelessWidget {
  const PartagePointOk({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(lPartagerDesPoints)),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Text(
                lEnvoiConfirme,
                style: Utilities.style(),
              ),
              Utilities.getButtonSubmit(
                onPressed: () {
                  Utilities.resetAndOpenPage(context: context, view: Home());
                },
                //child: Text(lRetourAMesCartes),
                libelle: lRetourAMesCartes,
              )
            ],
          ),
        ),
      ),
    );
  }
}
