/*import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/fidelite_carte.dart';
import 'package:closeapps/views/souscrire_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_lorem/flutter_lorem.dart';

class WelcomeAddCard extends StatefulWidget {
  FideliteCarte carte;
  WelcomeAddCard({Key key, this.carte}) : super(key: key);

  @override
  _WelcomeAddCardState createState() => _WelcomeAddCardState();
}

class _WelcomeAddCardState extends State<WelcomeAddCard> {
  bool _accepterCondition = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CustomScrollView(
      shrinkWrap: true,
      slivers: <Widget>[
        SliverAppBar(
          backgroundColor:
              widget.carte != null && widget.carte.colorLogo != null
                  ? widget.carte.colorLogo
                  : colorBlueLogo,
          expandedHeight: expandedHeight,
          floating: false,
          pinned: true,

          /*
          title: Text(
            widget.carte != null ? widget.carte.title : "Aucune Libelle",
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: Utilities.style(
              color: colorWitheLogo,
            ),
          ),*/

          //textTheme: TextTheme(headline1:Utilities.style() ) ,
          flexibleSpace: FlexibleSpaceBar(
            collapseMode: CollapseMode.none,
            background: Image.asset(
              widget.carte != null && widget.carte.urlLogo != null
                  ? widget.carte.urlLogo
                  : logoBlue,
              fit: BoxFit.fill,
              //width: double.infinity,
              //height: expandedHeight,
            ),
            title: Text(
              widget.carte != null && widget.carte.libelle != null
                  ? widget.carte.libelle
                  : "Aucun Libelle",
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: Utilities.style(
                color: colorWitheLogo,
                //fontSize: 20
              ),
            ),
          ),
        ),
        SliverFillRemaining(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            //mainAxisSize: MainAxisSize.max,
            //verticalDirection: VerticalDirection.down,
            //crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: GridView.count(
                        padding: EdgeInsets.all(8.0),
                        shrinkWrap:
                            true, // limite la heigth de l'element a son contenu et non a la heigth du parent
                        //scrollDirection: Axis.horizontal,
                        crossAxisCount: 2,
                        children: <Widget>[
                          InkWell(
                            splashColor: colorBlueLogo,
                            onTap: () {
                              _accepterCondition
                                  ? Utilities.navigatorPush(
                                      context: context, view: SouscrireCard())
                                  : Utilities.getToast(lValiderConditions);
                            },
                            child: Card(
                              borderOnForeground: false,
                              child: Center(
                                child: Text(
                                  lSincrireAuProgramme,
                                  style: style20BlueBold,
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                          InkWell(
                            splashColor: colorBlueLogo,
                            onTap: () {
                              Utilities.getToast(_accepterCondition
                                  ? lAVenir
                                  : lValiderConditions);
                            },
                            child: Card(
                              borderOnForeground: true,
                              child: Center(
                                child: Text(
                                  lScannerMaCarte,
                                  style: style20BlueBold,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              InkWell(
                onTap: () {
                  Utilities.alertDialogCustom(
                      context: context,
                      title: lConditionsDutilisations,
                      content: lorem(paragraphs: 2, words: 100),
                      rightFuction: () {
                        setState(() {
                          _accepterCondition = true;
                        });
                        Navigator.pop(context);
                        Utilities.getToast(
                            "Merci d'avoir accepter nos conditons !!!");
                      },
                      leftFuction: () {
                        setState(() {
                          _accepterCondition = false;
                        });
                        Navigator.pop(context);
                        Utilities.getToast(
                            "Merci d'avoir lu nos conditons. A la prochaine !!!");
                      });
                },
                child: Center(
                  child: Text(lConditionsDutilisations),
                ),
              ),
              //Text(lConditionsDutilisations),
            ],
          ),
        )
      ],
    )
        /*Column(
          children: <Widget>[
            Expanded(
              child: Row(
                //shrinkWrap: true,
                children: <Widget>[
                 
                  Expanded(
                    child: GridView.count(
                      //scrollDirection: Axis.horizontal,
                      crossAxisCount: 2,
                      children: <Widget>[
                        Card(
                          child: Center(
                            child: Text("data"),
                          ),
                        ),
                        Card(
                          child: Center(
                            child: Text("data"),
                          ),
                        ),
                      ],
                    ),
                  ),

                  /*
                  Banner(
                      message: "Je suis une BennerBenner",
                      location: BannerLocation.bottomEnd),
                  Card(
                    child: Center(
                      child: Text("datadata"),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Card(
                        child: Center(
                          child: Text("data"),
                        ),
                      ),
                      Spacer(),
                      Card(
                          child: Center(
                        child: Text("data"),
                      ))
                    ],
                  ),
                  Banner(              
                    textDirection: TextDirection.rtl,
                    child: Text("datadata"),
                      message: "Je suis une BennerBenner",
                      location: BannerLocation.bottomStart),
                      */
                ],
              ),
            ),
            Center(child: Text(lConditionsDutilisations)),
          ],
        )*/

        /*GridView.count(
        crossAxisCount: 2,
        children: <Widget>[
          Card(
            child: Center(
              child: Text("data"),
            ),
          ),
          Card(
            child: Center(
              child: Text("data"),
            ),
          ),
          Center(
            child: Text(lConditionsDutilisations),
          ),
        ],
      ),*/
        );
  }
}
*/
