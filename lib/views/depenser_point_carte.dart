import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/fidelite_carte.dart';
import 'package:closeapps/models/response_custom.dart';
import 'package:closeapps/models/user.dart';
import 'package:closeapps/views/scanner_carte.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DepenserPointCarte extends StatefulWidget {
  DepenserPointCarte({
    Key? key,
    //this.souscriptionPdfCarte,
    this.idCarte,
    required this.souscriptionPdfCarteId,
  }) : super(key: key);
  int? idCarte;
  //int programmeFideliteCarteId;
  int souscriptionPdfCarteId;

  //FideliteCarte souscriptionPdfCarte;

  @override
  _DepenserPointCarteState createState() => _DepenserPointCarteState();
}

class _DepenserPointCarteState extends State<DepenserPointCarte> {
  FideliteCarte? souscriptionPdfCarte;
  int? programmeFideliteCarteId;

  final _formKey = GlobalKey<FormState>();
  TextEditingController _controllerPoint = new TextEditingController();
  bool _autovalidateForm = false;
  User _user = new User();
  bool _isDisable = false;
  bool isWaitingData = true;

  int xofInitial = 0;
  int xofRetirer = 0;

  int xofApres = 0;
  int nbrePointActuelle = 0;
  int nbrePointActuelleApres = 0;
  int nbrePointRetirer = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    initData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
        (souscriptionPdfCarte != null && souscriptionPdfCarte!.libelle != null
                ? souscriptionPdfCarte!.libelle!
                //: "CLOSE APP",
                : "") +
            sautDelLine +
            lDepenserDesPoints,
      )),
      body: SingleChildScrollView(
          padding: EdgeInsets.all(16.0),
          child: Center(
            child: Form(
              key: _formKey,
              autovalidate: _autovalidateForm,
              //autovalidateMode: AutovalidateMode.always,
              //autovalidateMode: AutovalidateMode.always,
              child: Column(
                children: [
                  // Text(
                  //   "Avant depense ${souscriptionPdfCarte != null && souscriptionPdfCarte.nbrePointObtenu != null ? souscriptionPdfCarte.nbrePointObtenu : 0} points -- 0 F CFA",
                  //   style: Utilities.style(),
                  // ),
                  Text(
                    "Avant depense: $nbrePointActuelle" +
                        espace +
                        lPoints +
                        tiret +
                        '$xofInitial' +
                        espace +
                        lXOF,
                    style: Utilities.style(),
                  ),
                  Divider(
                    height: 30,
                  ),
                  TextFormField(
                    // inputFormatters: [
                    //   FilteringTextInputFormatter.allow(
                    //       RegExp(r'^\d+(?:\.\d+)?$')),
                    // ],

                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    controller: _controllerPoint,
                    textAlign: TextAlign.center,
                    onSaved: (input) {
                      _user.pointPartager = input!.trim();
                    },
                    onChanged: (value) {
                      calculValeurVariable(value: value);
                    }, // recuperation des données saisies
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        //_isDisable = true;
                        return erreurSaisieTexte;
                      }
                      if (value.isNotEmpty &&
                          !RegExp(r'^[0-9]+$').hasMatch(value)) {
                        return erreurValeurSaisie;
                      }
                      if (int.parse(value) == 0) {
                        //_isDisable = true;
                        return erreurValeurSaisie;
                      }
                      if (comparerSiSommeEstValide(
                          value: int.parse(value),
                          nbrePointActuelle: nbrePointActuelle)) {
                        //if (int.parse(value) > nbrePointActuelle) {
                        //_isDisable = true;
                        return erreurPointAPartager;
                      }
                      // if (int.parse(value) == 0) {
                      //   //_isDisable = true;
                      //   return erreurPartagerZeroPoint;
                      // }
                      // if (int.parse(value) < 0) {
                      //   //_isDisable = true;
                      //   return erreurPartagerZeroPoint;
                      // }
                      //_isDisable = false;
                      return null;
                      //Utilities.validator(value, typeContentToValidate: typeText);
                    },
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(20.0)),
                      labelText: lXOF,

                      prefixIcon: IconButton(
                          icon: Icon(Icons.remove),
                          onPressed: () {
                            // if (_controllerPoint.text != null &&
                            //     int.parse(_controllerPoint.text) > 0) {
                            //   _controllerPoint.text =
                            //       "${int.parse(_controllerPoint.text) - 1}";
                            // }
                            Utilities.decrementer(_controllerPoint);
                            calculValeurVariable();
                          }),
                      suffixIcon: IconButton(
                          icon: Icon(Icons.add_box),
                          onPressed: () {
                            // if (_controllerPoint.text != null &&
                            //     _controllerPoint.text.isNotEmpty &&
                            //     int.parse(_controllerPoint.text) > 0) {
                            //   _controllerPoint.text =
                            //       "${int.parse(_controllerPoint.text) + 1}";
                            // }

                            Utilities.incrementer(_controllerPoint);
                            calculValeurVariable();
                          }),
                    ),
                    keyboardType: TextInputType.number, // le type du input
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(lMin),
                      //Text('$xofRetirer ' + lXOF),
                      Text('$nbrePointRetirer ' + lPoints),
                      Text(lMax),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  // Text(
                  //   "Après depense ${souscriptionPdfCarte != null && souscriptionPdfCarte.nbrePointObtenu != null ? souscriptionPdfCarte.nbrePointObtenu : 0} points -- 0 F CFA",
                  //   style: Utilities.style(),
                  // ),
                  Text(
                    "Après depense: $nbrePointActuelleApres" +
                        espace +
                        lPoints +
                        tiret +
                        '$xofApres' +
                        espace +
                        lXOF,
                    style: Utilities.style(),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Utilities.getButtonSubmit(
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              Utilities.navigatorPush(
                                  context: context,
                                  view: ScannerCarte(
                                    pdfCarte: souscriptionPdfCarte,
                                    //typeActionCode: DEBITER,
                                    souscriptionProgrammeFideliteCarteId:
                                        souscriptionPdfCarte!.id!,
                                    programmeFideliteCarteId:
                                        programmeFideliteCarteId!,
                                    somme: int.parse(_controllerPoint.text),
                                  ));
                            } else {
                              setState(() {
                                _autovalidateForm = true;
                              });
                            }
                          },
                          //child: Text(lSuivant),

                          libelle: lEnMagasin,
                          isDisable: _isDisable),
                      Utilities.getButtonSubmit(
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              Utilities.getToast(lAVenir);
                            } else {
                              setState(() {
                                _autovalidateForm = true;
                              });
                            }
                          },
                          //child: Text(lSuivant),
                          libelle: lEnLigne,
                          isDisable: _isDisable),
                    ],
                  )
                ],
              ),
            ),
          )),
    );
  }

  initData() async {
    User data = User();
    data.id = widget.souscriptionPdfCarteId;
    ResponseCustom response = await baseApi.newAppelApi(
      data: data,
      endpointApi: souscriptionProgrammeFideliteCarte + getByCriteria,
    );

    if (response != null &&
        response.hasError != null &&
        !response.hasError! &&
        response.items != null &&
        response.items!.length > 0) {
      souscriptionPdfCarte = FideliteCarte.fromJson(response.items![0]);
      programmeFideliteCarteId = souscriptionPdfCarte!.programmeFideliteCarteId;
    }
    isWaitingData = false;

    xofInitial = Utilities.getXOF(fideliteCarte: souscriptionPdfCarte);
    xofApres = xofInitial;
    nbrePointActuelle = ((souscriptionPdfCarte != null &&
            souscriptionPdfCarte!.nbrePointActuelle != null)
        ? souscriptionPdfCarte!.nbrePointActuelle!
        : 0);
    nbrePointActuelleApres = nbrePointActuelle;
    setState(() {});
  }

  comparerSiSommeEstValide(
      {required int value, required int nbrePointActuelle}) {
    int nbreDePoint = 0;
    if (souscriptionPdfCarte != null &&
        souscriptionPdfCarte!.programmeFideliteCarteCorrespondanceNbrePoint !=
            null &&
        souscriptionPdfCarte!.programmeFideliteCarteNbrePointObtenu != null) {
      nbreDePoint =
          ((souscriptionPdfCarte!.programmeFideliteCarteNbrePointObtenu! *
                      value) /
                  souscriptionPdfCarte!
                      .programmeFideliteCarteCorrespondanceNbrePoint!)
              .round();
    }

    return nbreDePoint > nbrePointActuelle;
  }

  // calcul des valeurs variables
  calculValeurVariable({value}) {
    Utilities.begin("calculValeurVariable value: ");
    // donnee saisi
    //if (value.isNotEmpty) {
    if (_controllerPoint.text != null &&
        _controllerPoint.text.isNotEmpty &&
        RegExp(r'^[0-9]+$').hasMatch(_controllerPoint.text)) {
      //int? nbrePoint = int.parse(_controllerPoint.text);
      int? xofSaisie = int.parse(_controllerPoint.text);

      int nbreDePoint = 0;
      if (souscriptionPdfCarte != null &&
          souscriptionPdfCarte!.programmeFideliteCarteCorrespondanceNbrePoint !=
              null &&
          souscriptionPdfCarte!.programmeFideliteCarteNbrePointObtenu != null) {
        nbreDePoint =
            ((souscriptionPdfCarte!.programmeFideliteCarteNbrePointObtenu! *
                        xofSaisie) /
                    souscriptionPdfCarte!
                        .programmeFideliteCarteCorrespondanceNbrePoint!)
                .round();

        xofRetirer = ((nbreDePoint *
                    souscriptionPdfCarte!
                        .programmeFideliteCarteCorrespondanceNbrePoint!) /
                souscriptionPdfCarte!.programmeFideliteCarteNbrePointObtenu!)
            .round() // arrondir
            .toInt();
      }

      nbrePointRetirer = nbreDePoint;
      xofApres = (xofInitial >= xofRetirer) ? xofInitial - xofRetirer : 0;
      nbrePointActuelleApres = (nbrePointActuelle >= nbreDePoint)
          ? nbrePointActuelle - nbreDePoint
          : 0;
    } else {
      nbrePointActuelleApres = nbrePointActuelle;
      xofApres = xofInitial;
      xofRetirer = 0;
      nbrePointRetirer = 0;
    }
    setState(() {});
  }

  calculValeurVariableOld({value}) {
    Utilities.begin("calculValeurVariable value: ");
    // donnee saisi
    //if (value.isNotEmpty) {
    if (_controllerPoint.text != null &&
        _controllerPoint.text.isNotEmpty &&
        RegExp(r'^[0-9]+$').hasMatch(_controllerPoint.text)) {
      //int? nbrePoint = int.parse(_controllerPoint.text);
      int? nbrePoint = int.parse(_controllerPoint.text);
      nbrePointActuelleApres =
          (nbrePointActuelle >= nbrePoint) ? nbrePointActuelle - nbrePoint : 0;

      if (souscriptionPdfCarte != null &&
          souscriptionPdfCarte!.programmeFideliteCarteCorrespondanceNbrePoint !=
              null &&
          souscriptionPdfCarte!.programmeFideliteCarteNbrePointObtenu != null) {
        xofRetirer = ((nbrePoint *
                    souscriptionPdfCarte!
                        .programmeFideliteCarteCorrespondanceNbrePoint!) /
                souscriptionPdfCarte!.programmeFideliteCarteNbrePointObtenu!)
            .round() // arrondir
            .toInt();

        xofApres = (xofInitial >= xofRetirer) ? xofInitial - xofRetirer : 0;
      }
    } else {
      nbrePointActuelleApres = nbrePointActuelle;
      xofApres = xofInitial;
      xofRetirer = 0;
    }

    setState(() {});
  }
}
