import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/preferences_util.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/fidelite_carte.dart';
import 'package:closeapps/models/request_custom.dart';
import 'package:closeapps/models/response_custom.dart';
import 'package:closeapps/models/user.dart';
import 'package:closeapps/views/menu_pdf_carte.dart';
import 'package:flutter/material.dart';
import 'package:string_validator/string_validator.dart';

class ParrainerAmis extends StatefulWidget {
  ParrainerAmis(
      {Key? key,
      this.souscriptionPdfCarte,
      this.idCarte,
      required this.programmeFideliteCarteId})
      : super(key: key);
  int? idCarte;
  int programmeFideliteCarteId;
  FideliteCarte? souscriptionPdfCarte;

  @override
  _ParrainerAmisState createState() => _ParrainerAmisState();
}

class _ParrainerAmisState extends State<ParrainerAmis> {
  List<TableRow> listTableRow = [Utilities.headerParrainageTable];
  List<User> datasUser = [];
  //List<TableRow> listTableRow = List();

  final _formKey = GlobalKey<FormState>();
  TextEditingController _controllerTelephone = new TextEditingController();
  TextEditingController _controllerEmail = new TextEditingController();

  bool _autovalidateForm = false;
  bool _isDisable = false;

  User _user = new User();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
        (widget.souscriptionPdfCarte != null &&
                    widget.souscriptionPdfCarte!.libelle != null
                ? widget.souscriptionPdfCarte!.libelle!
                : "CLOSE APP") +
            sautDelLine +
            lParrainerDesAmis,
      )),
      body: SingleChildScrollView(
          padding: EdgeInsets.all(16.0),
          child: Center(
            child: Form(
              key: _formKey,
              autovalidate: _autovalidateForm,
              //autovalidateMode: AutovalidateMode.always,
              //autovalidateMode: AutovalidateMode.always,
              //autovalidateMode: AutovalidateMode.onUserInteraction,

              child: Column(
                children: [
                  Text(
                    "Veuillez ajouter vos filleuls",
                    style: Utilities.style(),
                  ),
                  Divider(
                    height: 30,
                  ),
                  Utilities.getDefaultSizeBoxWithHeight(),
                  TextFormField(
                    //autovalidate: true,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    controller: _controllerTelephone,
                    onSaved: (input) {
                      _user.telephoneReceveur = input!.trim();
                    }, // recuperation des données saisies
                    onChanged: (value) {
                      //_user.telephoneReceveur = value;
                    },

                    validator: (value) {
                      if ((value == null || value.isEmpty) &&
                          _controllerEmail.text.length == 0) {
                        return erreurSaisieTexte;
                      }
                      if (value != null &&
                          value.isNotEmpty &&
                          !RegExp(r'^[0-9]+$').hasMatch(value)) {
                        return erreurValeurSaisie;
                      }
                      return null;
                      //Utilities.validator(value, typeContentToValidate: typeText);
                    },
                    decoration: Utilities.getDefaultInputDecoration(
                      labelText: lTelephoneFilleul,
                    ),
                    keyboardType: TextInputType.number, // le type du input
                  ),
                  Utilities.getDefaultSizeBoxWithHeight(),
                  TextFormField(
                    //autovalidate: _autovalidateEmail,
                    controller: _controllerEmail,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    onSaved: (input) {
                      _user.emailReceveur = input!.trim();
                    },
                    onChanged: (value) {
                      // if (!isEmail(value)) {
                      //   setState(() {
                      //     _autovalidateEmail = true;
                      //   });
                      // }
                      //_changeDynamicShape();
                    },
                    validator: (value) {
                      if ((value == null || value.isEmpty) &&
                          _controllerTelephone.text.length == 0) {
                        return erreurSaisieTexte;
                      }
                      if (value != null &&
                          value.isNotEmpty &&
                          !isEmail(value)) {
                        return erreurSaisieEmail;
                      }
                      return null;
                    },
                    decoration: Utilities.getDefaultInputDecoration(
                      labelText: lEmailFilleul,
                    ),
                    keyboardType:
                        TextInputType.emailAddress, // le type du input
                  ),
                  Utilities.getDefaultSizeBoxWithHeight(),
                  Utilities.getButtonSubmit(
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          // ajouter a la liste
                          _ajouterFilleul();
                        } else {
                          setState(() {
                            _autovalidateForm = true;
                          });
                        }
                      },
                      //child: Icon(Icons.delete),
                      libelle: lAjouter,
                      isDisable: _isDisable),
                  Utilities.getDefaultSizeBoxWithHeight(),
                  Divider(
                    height: 30,
                  ),
                  Table(
                      // border: TableBorder.symmetric(
                      //     inside: BorderSide(width: 1, color: colorBlueLogo),
                      //     outside: BorderSide(width: 1, color: colorBlueLogo)),
                      defaultColumnWidth: FixedColumnWidth(150),
                      defaultVerticalAlignment:
                          TableCellVerticalAlignment.middle,
                      //border: TableBorder.all(color: colorBlueLogo),
                      // children: (listTableRow != null && listTableRow.isNotEmpty)
                      //     ? listTableRow
                      //     : Utilities.emptyTable(
                      //         nbreColumn: 3,
                      //         headerTable: Utilities.headerParrainageTable),

                      children: listTableRow),
                  Utilities.getDefaultSizeBoxWithHeight(height: 30),
                  Utilities.getButtonSubmit(
                      onPressed: () {
                        if (listTableRow.length > 1) {
                          Utilities.alertDialogCustom(
                              libelleLeftFuction: lAnnuler,
                              libelleRightFuction: lConfirmer,
                              rightFuction: () {
                                Navigator.pop(
                                    context); //  clean la boite de dialogue
                                _submit();
                              },
                              leftFuction: () {
                                // retour au menu de carte
                                Navigator.pop(context);
                                //Navigator.pop(context);

                                // ou Utilities.navigatorPush(context: context, view: MenusouscriptionPdfCarte());
                              },
                              context: context,
                              title: lConfirmation,
                              content: lContenuConfirmationParrainage);
                        } else {
                          Utilities.getToast(listeVide);
                        }
                      },
                      //child: Text(lSuivant),
                      libelle: lParrainer,
                      isDisable: _isDisable),
                ],
              ),
            ),
          )),
    );
  }

  _submit() async {
    setState(() {
      // griser le boutton
      _isDisable = true;
    });
    // Utilities.loadingIndicator(
    //     context: context, color: colorBlueLogo); // en attente
    // Navigator.of(context).pop(); // LEVEE LE Indicator

    Utilities.loadingIndicator(context: context, color: colorRedLogo);

    Utilities.begin("_submit");
    // appel du service

    User data = User();
    data.carteId = widget.idCarte;
    //data.carteId = await Utilities.carteIdUserConnected;

    data.programmeFideliteCarteId = widget.programmeFideliteCarteId;
    data.datasUser = datasUser;

    RequestCustom request = RequestCustom();
    request.data = data;
    request.user = await PreferencesUtil.instance.getInt(id);

    ResponseCustom response = await baseApi.appelApi(
        endpointApi: souscriptionProgrammeFideliteCarte + parrainer,
        request: request);

    Navigator.pop(context); // pour retirer le progressing
    // Navigator.of(context).pop(); // LEVEE LE Indicator
    if (response.hasError != null && !response.hasError!) {
      listTableRow = [Utilities.headerParrainageTable];
      datasUser = [];
      Navigator.of(context).pop(); // retour au menu de souscriptionPdfCarte
      Utilities.getToast(succes);
    } else {
      Utilities.messageApi(response: response);
    }
    setState(() {
      // griser le boutton
      _isDisable = false;
    });
  }

  _ajouterFilleul() {
    TableRow newTableRow = TableRow();
    User user = User();

    user.telephone = _controllerTelephone.text.trim();
    user.email = _controllerEmail.text.trim();
    newTableRow = TableRow(
      children: [
        Text(_controllerTelephone.text),
        Text(_controllerEmail.text),
        Center(
          child: IconButton(
              alignment: Alignment.center,
              icon: Icon(Icons.delete),
              onPressed: () {
                listTableRow.remove(newTableRow);
                datasUser.remove(user);
                setState(() {});
              }),
        )
      ],
    );
    listTableRow.add(newTableRow);
    datasUser.add(user);

    _formKey.currentState!.reset();
    _controllerTelephone = new TextEditingController();
    _controllerEmail = new TextEditingController();
    setState(() {});
  }

  _retirerFilleul() {}
}
