import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/views/inscription.dart';
import 'package:closeapps/views/login.dart';
import 'package:closeapps/views/nous_contacter.dart';
import 'package:flutter/material.dart';

class Welcome extends StatefulWidget {
  Welcome({Key? key}) : super(key: key);

  @override
  _WelcomeState createState() => _WelcomeState();
}

class _WelcomeState extends State<Welcome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //title: Text(lWelcome),
        //backgroundColor: colorRedLogo,
        elevation: zero,
      ),
      //backgroundColor: colorRedLogo,
      backgroundColor: colorBlueLogo,
      body: OrientationBuilder(builder: (context, orientation) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              (orientation == Orientation.portrait)
                  ? SizedBox(
                      height: 100,
                    )
                  : Container(),
              Image.asset(logoBlue),
              //Image.asset(logoWhite),

              /*Container(
                      height: 200,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(logoBlue),
                          //fit: BoxFit.cover,
                        ),
                      ),
                    ),*/
              SizedBox(
                height: 65,
              ),
              Utilities.getButtonSubmit(
                  //child: Text(lSeConnecter),
                  libelle: lSeConnecter,
                  onPressed: () =>
                      Utilities.navigatorPush(context: context, view: Login())),
              //Utilities.navigatorPush(context: context, view: NousContacter())),
              Utilities.getButtonSubmit(
                  //child: Text(lSinscrire),
                  libelle: lSinscrire,
                  onPressed: () => Utilities.navigatorPush(
                      context: context, view: Inscription()))
            ],
          ),
        );
      }),
    );
  }
}
