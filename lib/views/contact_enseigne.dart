import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/helpers/communications_service.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/service_locator.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/fidelite_carte.dart';
import 'package:closeapps/models/request_custom.dart';
import 'package:closeapps/models/response_custom.dart';
import 'package:closeapps/models/user.dart';
import 'package:flutter/material.dart';

class ContactEnseigne extends StatefulWidget {
  const ContactEnseigne({Key? key, this.programmeFideliteTampon})
      : super(key: key);

  final FideliteCarte? programmeFideliteTampon;

  @override
  _ContactEnseigneState createState() => _ContactEnseigneState();
}

class _ContactEnseigneState extends State<ContactEnseigne> {
  final CommunicationService _communicationService =
      locator<CommunicationService>();

  User? itemEnseigne;

  bool _waitingItemsEnseigne = true;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    getEnseigne();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
        (widget.programmeFideliteTampon!.libelle!) + sautDelLine + lContacts,
      )),
      body: _waitingItemsEnseigne
          ? Center(child: CircularProgressIndicator())
          : itemEnseigne != null
              ? SingleChildScrollView(
                  child: Center(
                    child: Column(
                      children: [
                        ListTile(
                          title: Text(
                            Utilities.initialStringValueWidget(
                              itemEnseigne!.nom!,
                            ),
                            style: Utilities.style(),
                          ),
                          //leading: Icon(Icons.),
                        ),
                        Divider(),
                        ListTile(
                          title: Text(Utilities.initialStringValueWidget(
                              itemEnseigne!.telephone!)),
                          leading: Icon(Icons.call),
                          onTap: () {
                            _communicationService.call(
                                Utilities.initialStringValueWidget(
                                    itemEnseigne!.telephone!));
                          },
                        ),
                        Divider(),
                        ListTile(
                          title: Text(Utilities.initialStringValueWidget(
                              itemEnseigne!.telephone!)),
                          leading: Icon(Icons.location_on),
                          onTap: () {
                            _communicationService.map();
                          },
                        ),
                        Divider(),
                        ListTile(
                          title: Text(Utilities.initialStringValueWidget(
                              itemEnseigne!.email!)),
                          leading: Icon(Icons.email),
                          onTap: () {
                            _communicationService.sendEmail(
                                Utilities.initialStringValueWidget(
                                    itemEnseigne!.email!));
                          },
                        )
                      ],
                    ),
                  ),
                )
              : Container(),
    );
  }

  getEnseigne() async {
    User data = User();
    // appel de service
    RequestCustom request = RequestCustom();

    data.id = widget.programmeFideliteTampon!.enseigneId;

    request.data = data;

    ResponseCustom response = await baseApi.appelApi(
        endpointApi:   enseigne + getByCriteria, request: request);

    // construction du itemsPdvdPdf
    if (response.hasError != null && !response.hasError!) {
      if (response.items != null && response.items!.isNotEmpty) {
        itemEnseigne = User.fromJson(response.items![0]);
      }
    } else {}

    setState(() {
      _waitingItemsEnseigne = false;
    });
  }
}
