import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/custom_widgets/my_text_form_field.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/request_custom.dart';
import 'package:closeapps/models/response_custom.dart';
import 'package:closeapps/models/user.dart';
import 'package:closeapps/views/inscription.dart';
import 'package:flutter/material.dart';
import 'package:string_validator/string_validator.dart';

class ChangerPassword extends StatefulWidget {
  ChangerPassword({Key? key}) : super(key: key);

  @override
  _ChangerPasswordState createState() => _ChangerPasswordState();
}

class _ChangerPasswordState extends State<ChangerPassword> {
  final _formKey = GlobalKey<FormState>();
  var apiBaseUrl = "";
  var endpointConnexion = "user/connexion";

  User _user = User();

  String? _email;

  String? _login;

  String? _password;

  bool _autovalidateEmail = false;

  bool _autovalidate = false;
  bool _dynamicShape = true;
  bool _isObscureTextPassword = true;

  bool _autovalidatePassword = false;
  bool _autovalidateConfirmPassword = false;

  TextEditingController _controllerNouveauPassword = TextEditingController();
  TextEditingController _controllerConfirmNouveauPassword =
      TextEditingController();

  bool _isObscureTextNouveauPassword = true;
  bool _isObscureTextConfirmNouveauPassword = true;

  bool _isDisable = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(lPasswordOublie),
      ),
      body: Center(
        child: ListView(shrinkWrap: true, children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Form(
              autovalidate: _autovalidate,
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                //crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  /*CircleAvatar(
                    backgroundColor: colorBlueLogo,
                    child: Image.asset(logoWhite),
                  ),
                  Text(
                    lMessageBienvenueConnexion,
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  */
                  //ImageIcon('images/logo.jpeg'),
                  //Utilities.loginFormField(_user, labelText: login),
                  //Utilities.passwordFormField(_user, labelText: password),

                  TextFormField(
                    onSaved: (input) {
                      _user.login = input!.trim();
                    }, // recuperation des données saisies
                    onChanged: (value) {
                      setState(() {
                        _dynamicShape = !_dynamicShape;
                      });
                    },
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return erreurSaisieTexte;
                      }
                      return null;
                      //Utilities.validator(value, typeContentToValidate: typeText);
                    },
                    decoration: Utilities.getDefaultInputDecoration(
                        labelText: lIdentifiant, isRequiredChamp: true),
                    autovalidate: _autovalidateEmail,
                  ),
                  Utilities.getDefaultSizeBoxWithHeight(),

                  TextFormField(
                    obscureText: _isObscureTextPassword,
                    onSaved: (input) {
                      _user.password = input!.trim();
                    },
                    onChanged: (value) {
                      setState(() {
                        _dynamicShape = !_dynamicShape;
                      });
                    }, // recuperation des données saisies
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return erreurSaisieTexte;
                      }
                      return null;
                      //Utilities.validator(value, typeContentToValidate: typeText);
                    },
                    decoration: Utilities.getDefaultInputDecoration(
                      labelText: lAncienPassword,
                      isRequiredChamp: true,
                      suffixIcon: IconButton(
                        icon: Icon(_isObscureTextPassword
                            ? Icons.visibility_off
                            : Icons.visibility),
                        onPressed: () {
                          setState(() {
                            _isObscureTextPassword = !_isObscureTextPassword;
                          });
                        },
                      ),
                    ),
                  ),
                  Utilities.getDefaultSizeBoxWithHeight(),

                  MyTextFormField(
                    controller: _controllerNouveauPassword,
                    obscureText: _isObscureTextNouveauPassword,
                    decoration: Utilities.getDefaultInputDecoration(
                      labelText: lMotDePasse,
                      isRequiredChamp: true,
                      suffixIcon: IconButton(
                        icon: Icon(_isObscureTextNouveauPassword
                            ? Icons.visibility_off
                            : Icons.visibility),
                        onPressed: () {
                          setState(() {
                            _isObscureTextNouveauPassword =
                                !_isObscureTextNouveauPassword;
                          });
                        },
                      ),
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return erreurSaisieTexte;
                      }
                      if (!equals(
                          value, _controllerConfirmNouveauPassword.text)) {
                        return erreurPassword;
                      }
                      return null;
                      //Utilities.validator(value, typeContentToValidate: typeText);
                    },
                    onSaved: (input) {
                      _user.newPassword = input;
                    },
                    onChanged: (value) {
                      if (_controllerConfirmNouveauPassword.text.isNotEmpty) {
                        setState(() {
                          _autovalidatePassword = true;
                        });
                      }
                    },
                    autovalidate: _autovalidatePassword,
                  ),
                  Utilities.getDefaultSizeBoxWithHeight(),

                  MyTextFormField(
                    controller: _controllerConfirmNouveauPassword,
                    obscureText: _isObscureTextConfirmNouveauPassword,
                    decoration: Utilities.getDefaultInputDecoration(
                      labelText: lConfirmerPassword,
                      isRequiredChamp: true,
                      suffixIcon: IconButton(
                        icon: Icon(_isObscureTextConfirmNouveauPassword
                            ? Icons.visibility_off
                            : Icons.visibility),
                        onPressed: () {
                          setState(() {
                            _isObscureTextConfirmNouveauPassword =
                                !_isObscureTextConfirmNouveauPassword;
                          });
                        },
                      ),
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return erreurSaisieTexte;
                      }
                      if (!equals(value, _controllerNouveauPassword.text)) {
                        return erreurPassword;
                      }

                      return null;
                      //Utilities.validator(value, typeContentToValidate: typeText);
                    },
                    onSaved: (input) {
                      //dataUser.libelle = input;
                    },
                    onChanged: (value) {
                      if (_controllerNouveauPassword.text.isNotEmpty) {
                        setState(() {
                          _autovalidateConfirmPassword = true;
                        });
                      }
                    },
                    autovalidate: _autovalidateConfirmPassword,
                  ),

                  Utilities.getDefaultSizeBoxWithHeight(),

                  SizedBox(
                    width: double.infinity,
                    child: Utilities.getButtonSubmit(
                        // shape: (_dynamicShape)? Utilities.getShape(topLeft: 50, bottomRight: 50): Utilities.getShape(bottomLeft: 50, topRight: 50),
                        //child: Text(lEnvoyer),
                        libelle: lEnvoyer,
                        onPressed: _submit,
                        isDisable: _isDisable),
                  ),
                ],
              ),
            ),
          ),
        ]),
      ),
    );
  }

  _submit() async {
    if (_formKey.currentState!.validate()) {
      setState(() {
        // griser button
        _isDisable = true;
      });

      Utilities.loadingIndicator(context: context, color: colorRedLogo);

      // permet d'executer toutes les validator des TextFormField
      _formKey.currentState!.save();

      print(_user.toString());

      RequestCustom request = RequestCustom();

      request.data = _user;

      String uri = user + changerPassword;
      ResponseCustom response =
          await baseApi.appelApi(endpointApi: uri, request: request);

      Navigator.pop(context); // pour retirer le progressing

      if (response != null) {
        if (response.hasError != null && !response.hasError!) {
          Utilities.messageApi(response: response);
          /*
          await Utilities.showDialogCustom(
              context: context,
              titre: "Message",
              contenu: notificationPasswordOublie);
          */

          Utilities.getToast(notificationPasswordOublie);
          Utilities.resetAndOpenPage(context: context);
        } else {
          // verifier
          Utilities.messageApi(response: response);
        }
      }

      setState(() {
        // degriser button
        _isDisable = false;
      });
    } else {
      setState(() {
        _autovalidate = true;
      });
    }
  }
}
