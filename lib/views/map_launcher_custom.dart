import 'dart:html';

import 'package:closeapps/models/fidelite_carte.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:map_launcher/map_launcher.dart';

//void main() => runApp(MapLauncherCustom());

class MapLauncherCustom extends StatefulWidget {
  FideliteCarte informationsPDVDuPDF;
  MapLauncherCustom({
    Key? key,
    required this.informationsPDVDuPDF,
  }) : super(key: key);

  @override
  _MapLauncherCustomState createState() => _MapLauncherCustomState();
}

class _MapLauncherCustomState extends State<MapLauncherCustom> {
  // get ma position and

  late Position initialPosition;

  openMapsSheet(context) async {
    try {
      final availableMaps = await MapLauncher.installedMaps;

      showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return SafeArea(
            child: SingleChildScrollView(
              child: Container(
                child: Wrap(
                  children: <Widget>[
                    for (var map in availableMaps)
                      ListTile(
                        onTap: () {
                          map.showDirections(
                              destination: Coords(
                                  widget.informationsPDVDuPDF.latitude!,
                                  widget.informationsPDVDuPDF.longitude!),
                              origin: Coords(initialPosition.latitude,
                                  initialPosition.longitude),
                              destinationTitle:
                                  widget.informationsPDVDuPDF.pointDeVenteNom,
                              originTitle: "Position initiale");
                        },
                        title: Text(map.mapName),
                      ),
                  ],
                ),
              ),
            ),
          );
        },
      );
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Map Launcher Demo'),
        ),
        body: Center(child: Builder(
          builder: (context) {
            return MaterialButton(
              onPressed: () => openMapsSheet(context),
              child: Text('Show Maps'),
            );
          },
        )),
      ),
    );
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      setState(() {});
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        setState(() {});
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      setState(() {});
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.

    Position position = await Geolocator.getCurrentPosition();
    initialPosition = position;
    setState(() {});

    return position;
  }

  checkAvailableAndShow() async {
    bool isGoogleMaps =
        await MapLauncher.isMapAvailable(MapType.google) ?? false;

    if (isGoogleMaps) {
      await MapLauncher.showMarker(
        mapType: MapType.google,
        coords: Coords(0, 0),
        title: "title",
        // description: description,
      );
    }
  }

  showDirectionWithFirstMap() async {
    final List<AvailableMap> availableMaps = await MapLauncher.installedMaps;
    await availableMaps.first.showDirections(
      destination: Coords(0, 0),
    );
  }

  test() async {
    if ((await MapLauncher.isMapAvailable(MapType.google))!) {
      await MapLauncher.showMarker(
        mapType: MapType.google,
        coords: Coords(0, 0),
        title: "title",
        description: "description",
      );
    }
  }

  showMarkerWithFirstMap() async {
    final List<AvailableMap> availableMaps = await MapLauncher.installedMaps;
    await availableMaps.first.showMarker(
      coords: Coords(0, 0),
      title: "title",
    );
  }
}
