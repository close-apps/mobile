import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/fidelite_carte.dart';
import 'package:closeapps/views/partager_point_carte.dart';
import 'package:flutter/material.dart';

class MenuPartagerPoint extends StatefulWidget {
  MenuPartagerPoint(
      {Key? key,
      this.souscriptionPdfCarte,
      this.idCarte,
      required this.programmeFideliteCarteId})
      : super(key: key);
  int? idCarte;
  int programmeFideliteCarteId;
  FideliteCarte? souscriptionPdfCarte;

  @override
  _MenuPartagerPointState createState() => _MenuPartagerPointState();
}

class _MenuPartagerPointState extends State<MenuPartagerPoint> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
        (widget.souscriptionPdfCarte != null &&
                    widget.souscriptionPdfCarte!.libelle != null
                ? widget.souscriptionPdfCarte!.libelle!
                : "CLOSE APP") +
            sautDelLine +
            lPartagerDesPoints,
      )),
      body: Center(
        child: SingleChildScrollView(
            padding: Utilities.getEdgeInsets(),
            child: Column(
              children: [
                SizedBox(
                  width: double.infinity,
                  // height: double.infinity,
                  child: Utilities.getButtonSubmit(
                    onPressed: () {
                      Utilities.navigatorPush(
                          context: context,
                          view: PartagerPointCarte(
                            souscriptionPdfCarte: widget.souscriptionPdfCarte,
                            programmeFideliteCarteId: widget
                                .souscriptionPdfCarte!
                                .programmeFideliteCarteId!,
                          ));
                    },
                    //child: Text(lPartager),
                    libelle: lPartager,
                  ),
                ),
                Utilities.getDefaultSizeBoxWithHeight(height: 20),
                SizedBox(
                  width: double.infinity,
                  // height: double.infinity,
                  child: Utilities.getButtonSubmit(
                      //child: Text(lRecevoir),
                      libelle: lRecevoir,
                      onPressed: () {
                        Utilities.getToast(lAVenir);
                      }),
                )
              ],
            )),
      ),
    );
  }
}
