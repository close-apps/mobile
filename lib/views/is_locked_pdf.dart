import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:flutter/material.dart';

class IsLockedPdf extends StatelessWidget {
  const IsLockedPdf(
      {Key? key, this.raisonLocked, this.programmeFideliteLibelle})
      : super(key: key);
  final String? raisonLocked;
  final String? programmeFideliteLibelle;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(lVerrouillage),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Vous etes bloqué au programme $programmeFideliteLibelle pour des raisons de non respects des règles de sécurité.\nVeuillez contacter votre enseigne pour plus de details.\nMerci de votre compréhension !!!",
                  style: Utilities.style(),
                  textAlign: TextAlign.justify,
                ),
                raisonLocked != null
                    ? Utilities.getDefaultDivider()
                    : Container(),
                raisonLocked != null
                    ? Text(
                        "Message de l'enseigne \n$raisonLocked!",
                        style: Utilities.style(color: colorBlueLogo),
                        textAlign: TextAlign.justify,
                      )
                    : Container()
              ],
            ),
          ),
        ),
      ),
    );
  }
}
