import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/custom_widgets/form_inscription.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/models/user.dart';
import 'package:flutter/material.dart';

class Inscription extends StatefulWidget {
  User? user;
  int? userId;
  //String login;
  bool isUpdate;
  Inscription({
    Key? key,
    this.isUpdate = false,
    this.user,
    this.userId,
  }) : super(key: key);

  @override
  _InscriptionState createState() => _InscriptionState();
}

class _InscriptionState extends State<Inscription> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(lInscription),
      ),
      body: FormInscription(
        userId: widget.userId,
        //login: widget.login,
        isUpdate: widget.isUpdate,
        user: widget.user,
      ),
    );
  }
}
