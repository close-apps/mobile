import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/fidelite_carte.dart';
import 'package:closeapps/views/inscription_pdf_tampon.dart';
import 'package:closeapps/views/menu_pdf_tampon.dart';
import 'package:flutter/material.dart';

class InscriptionTamponOk extends StatelessWidget {
  final FideliteCarte? souscriptionPdfTampon;
  const InscriptionTamponOk({Key? key, this.souscriptionPdfTampon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(lInscription),
      ),
      body: Center(
        child: Column(
          children: [
            Text(
              lSuccesSouscriptionProgramme,
              style: Utilities.style(),
            ),
            Utilities.getButtonSubmit(
                libelle: okButtonLabel,
                onPressed: () {
                  Navigator.of(context).pop();
                  Utilities.navigatorPush(
                      context: context,
                      view: MenuPdfTampon(
                          souscriptionPDFTamponId: souscriptionPdfTampon!.id!));
                  // view: InscriptionPDFTampon(tampon: tampon));
                }),
          ],
        ),
      ),
    );
  }
}
