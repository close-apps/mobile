import 'package:closeapps/custom_widgets/app_bar_custom.dart';
import 'package:closeapps/custom_widgets/boutton_connect_with.dart';
import 'package:closeapps/custom_widgets/sing_in.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/preferences_util.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/request_custom.dart';
import 'package:closeapps/models/response_custom.dart';
import 'package:closeapps/models/user.dart';
import 'package:closeapps/views/changer_password.dart';
import 'package:closeapps/views/inscription.dart';
import 'package:closeapps/views/password_oublie.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart' as FireUser;
import 'package:flutter_signin_button/button_list.dart';
import 'package:flutter_signin_button/button_view.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:getwidget/getwidget.dart';
import 'package:getwidget/types/gf_button_type.dart';

class Login extends StatefulWidget {
  Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formKey = GlobalKey<FormState>();
  var apiBaseUrl = "";
  var endpointConnexion = "user/connexion";

  User _user = User();

  String? _email;

  String? _login;

  String? _password;

  bool _autovalidate = false;
  bool _dynamicShape = true;
  bool _isObscureTextPassword = true;

  bool _isDisable = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(lConnexion),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
              //shrinkWrap: true,
              children: <Widget>[
                //child: ListView(shrinkWrap: true, children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Form(
                    autovalidate: _autovalidate,
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      //crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        /*CircleAvatar(
                      backgroundColor: colorBlueLogo,
                      child: Image.asset(logoWhite),
                    ),
                    Text(
                      lMessageBienvenueConnexion,
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        fontStyle: FontStyle.italic,
                      ),
                    ),
                    */
                        //ImageIcon('images/logo.jpeg'),
                        //Utilities.loginFormField(_user, labelText: login),
                        //Utilities.passwordFormField(_user, labelText: password),

                        TextFormField(
                          onSaved: (input) {
                            _user.login = input!.trim();
                          }, // recuperation des données saisies
                          onChanged: (value) {
                            setState(() {
                              _dynamicShape = !_dynamicShape;
                            });
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return erreurSaisieTexte;
                            }
                            return null;
                            //Utilities.validator(value, typeContentToValidate: typeText);
                          },
                          decoration: Utilities.getDefaultInputDecoration(
                              labelText: lIdentifiant, isRequiredChamp: true),
                          keyboardType: TextInputType.text, // le type du input
                        ),
                        Utilities.getDefaultSizeBoxWithHeight(),

                        TextFormField(
                          obscureText: _isObscureTextPassword,
                          onSaved: (input) {
                            _user.password = input!.trim();
                          },
                          onChanged: (value) {
                            setState(() {
                              _dynamicShape = !_dynamicShape;
                            });
                          }, // recuperation des données saisies
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return erreurSaisieTexte;
                            }
                            return null;
                            //Utilities.validator(value, typeContentToValidate: typeText);
                          },
                          decoration: Utilities.getDefaultInputDecoration(
                            labelText: lMotDePasse,
                            isRequiredChamp: true,
                            suffixIcon: IconButton(
                              icon: Icon(_isObscureTextPassword
                                  ? Icons.visibility_off
                                  : Icons.visibility),
                              onPressed: () {
                                setState(() {
                                  _isObscureTextPassword =
                                      !_isObscureTextPassword;
                                });
                              },
                            ),
                          ),
                          keyboardType:
                              TextInputType.visiblePassword, // le type du input
                        ),
                        /*
                          TextFormField(
                            onSaved: (input) { _user.password = input ;}, // recuperation des données saisies
                            //obscureText: true, // pour rendre un texte invisible
                            validator:(value) => Utilities.validator(value),
                            decoration: InputDecoration(
                                labelText: password,
                                helperText: "test mot de passe"
                            ),
                            keyboardType: TextInputType.visiblePassword, // le type du input
                          ),

                           */

                        SizedBox(
                          height: 10,
                        ),
                        SizedBox(
                          width: double.infinity,
                          child: Utilities.getButtonSubmit(
                              // shape: (_dynamicShape)? Utilities.getShape(topLeft: 50, bottomRight: 50): Utilities.getShape(bottomLeft: 50, topRight: 50),
                              //child: Text(lConnexion),
                              libelle: lConnexion,
                              onPressed: _submit,
                              isDisable: _isDisable),
                        ),
                        /*
                    RaisedButton(
                      //shape : Utilities.getShape(bottomLeft: 50, topRight: 50) ,
                      shape: (_dynamicShape)
                          ? Utilities.getShape(topLeft: 50, bottomRight: 50)
                          : Utilities.getShape(bottomLeft: 50, topRight: 50),
                      child: Text(connexion),
                      onPressed: _onSubmit,
                    ),
                    */
                        /*SizedBox(
                      //width: double.infinity,
                      child: Utilities.getButtonSubmit(
                          // shape: (_dynamicShape)? Utilities.getShape(topLeft: 50, bottomRight: 50): Utilities.getShape(bottomLeft: 50, topRight: 50),
                          child: Text(lSinscrire),
                          onPressed: () {
                            Utilities.navigatorPush(
                                context: context, view: Inscription());
                          },
                          isDisable: _isDisable),
                    ),*/

                        InkWell(
                          child: Text(
                            lSinscrire,
                            style: TextStyle(color: colorBlueLogo),
                          ),
                          onTap: () {
                            Utilities.navigatorPush(
                                context: context, view: Inscription());
                          },
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            InkWell(
                              child: Text(
                                //cChangerPassword,
                                lChangerPassword,
                                textAlign: TextAlign.right,
                                style: TextStyle(color: colorBlueLogo),
                              ),
                              onTap: () {
                                Utilities.navigatorPush(
                                    context: context, view: ChangerPassword());
                              },
                            ),
                            InkWell(
                              child: Text(
                                cPasswordOublie,
                                textAlign: TextAlign.right,
                                style: TextStyle(color: colorBlueLogo),
                              ),
                              onTap: () {
                                Utilities.navigatorPush(
                                    context: context, view: PasswordOublie());
                              },
                            ),
                          ],
                        ),
                        Divider(),
                        //SingInButton(),

                        SignInButton(
                          Buttons.Google,
                          onPressed: () async {
                            FireUser.User userGoogle =
                                await signInWithGoogle(); // importation de la classe User ambigue d'où FireUser.User
                            if (userGoogle != null &&
                                userGoogle.displayName != null &&
                                userGoogle.email != null) {
                              // construction du dataUser
                              _submit(userGoogle: userGoogle);
                            } else {
                              Utilities.getToast(echec);
                            }
                          },
                          elevation: 1,
                          text: "Utiliser Google",
                          shape: Utilities.getDefaultShape(),
                          //mini: true,
                        ),
                        SignInButton(Buttons.FacebookNew,
                            onPressed: () => Utilities.getToast(lAVenir),
                            elevation: 1,
                            padding: EdgeInsets.all(0),
                            shape: Utilities.getDefaultShape(),
                            text: "Utiliser Facebook"),
                        // GFSocialButton(
                        //   //fullWidthButton: true,
                        //   onPressed: () {},
                        //   text: libelleBouttonConnexionGoogle,
                        //   icon: Icon(
                        //     FontAwesomeIcons.google,
                        //     color: Colors.white,
                        //   ),
                        //   color: colorBlueLogo,
                        //   textColor: Colors.white,
                        //   elevation: 2,
                        //   shape: GFButtonShape.square,
                        // ),
                        // GFSocialButton(
                        //   onPressed: () {},
                        //   text: libelleBouttonConnexionFacebook,
                        //   icon: Icon(
                        //     Icons.facebook,
                        //     color: Colors.white,
                        //   ),
                        //   color: colorBlueLogo,
                        //   textColor: Colors.white,
                        //   elevation: 2,
                        // ),
                        // SizedBox(
                        //   width: double.infinity,
                        //   // height: double.infinity,
                        //   child: SingInButton(
                        //     onPressed: () async {
                        //       FireUser.User userGoogle =
                        //           await signInWithGoogle(); // importation de la classe User ambigue d'où FireUser.User
                        //       if (userGoogle != null &&
                        //           userGoogle.displayName != null &&
                        //           userGoogle.email != null) {
                        //         // construction du dataUser
                        //         _submit(userGoogle: userGoogle);
                        //       } else {
                        //         Utilities.getToast(echec);
                        //       }
                        //     },
                        //     libelleBoutton: libelleBouttonConnexionGoogle,
                        //   ),
                        // ),
                        // SizedBox(
                        //   width: double.infinity,
                        //   // height: double.infinity,
                        //   child: SingInButton(
                        //     libelleBoutton: libelleBouttonConnexionFacebook,
                        //     logo: facebookLogo,
                        //     onPressed: () => Utilities.getToast(lAVenir),
                        //   ),
                        // ),
                        /*
                        Container(
                          alignment: Alignment.topRight,
                          child: InkWell(
                            child: Text(
                              cPasswordOublie,
                              textAlign: TextAlign.right,
                              style: TextStyle(color: colorBlueLogo),
                            ),
                            onTap: () {
                              Utilities.navigatorPush(
                                  context: context, view: PasswordOublie());
                            },
                          ),
                        )
                        //
                        */
                      ],
                    ),
                  ),
                ),
              ]),
        ),
      ),
    );
  }

  _submit({FireUser.User? userGoogle}) async {
    if ((userGoogle != null) || _formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      setState(() {
        // griser button
        _isDisable = true;
      });
      Utilities.loadingIndicator(context: context, color: colorRedLogo);

      // if (userGoogle != null) {
      //   List<String> tabNom = userGoogle.displayName?.split(" ");
      //   if (tabNom != null) {
      //     _user.nom = tabNom.last;
      //     _user.prenoms = tabNom.first;
      //   }
      //   _user.telephone = userGoogle.phoneNumber;
      //   _user.email = userGoogle.email;
      //   _user.byFirebaseUser = true;
      // }
      Utilities.setUserByFireUserGoogle(fireUser: userGoogle, user: _user);

      // log data
      Utilities.logByTag(contenu: _user.toString());

      //connexion a la bd
      RequestCustom request = RequestCustom();
      request.data = _user;

      ResponseCustom response = await baseApi.appelApi(
          endpointApi: user + connexion, request: request);

      Navigator.pop(context); // pour retirer le progressing
      if (response != null &&
          response.hasError != null &&
          !response.hasError! &&
          response.items != null &&
          response.items!.length > 0) {
        // maj de la bd locale
        User userConnected = User.fromJson(response.items![0]);
        // sauvegarder les informations neccessaires en local
        Utilities.saveInLocalBd(dataUser: userConnected);
        /*
        await PreferencesUtil.instance.putInt(lId, userConnected.id);
        await PreferencesUtil.instance.putString(lNom, userConnected.nom);
        await PreferencesUtil.instance.putString(lEmail, userConnected.email);
        await PreferencesUtil.instance.putString(lLogin, userConnected.login);
        await PreferencesUtil.instance
            .putString(lPassword, userConnected.password);
        await PreferencesUtil.instance.putString(lPhone, userConnected.phone);
        await PreferencesUtil.instance
            .putString(lPrenoms, userConnected.prenoms);
        await PreferencesUtil.instance.putBool(lIsConnected, true);
        */
        Utilities.getToast(lConnexionReussi);
        Utilities.resetAndOpenPage(context: context);
      } else {
        Utilities.messageApi(response: response);
      }

      setState(() {
        // degriser button
        _isDisable = false;
      });

      /*
      String login = await PreferencesUtil.instance.getString(lLogin);
      String email = await PreferencesUtil.instance.getString(lEmail);
      String password = await PreferencesUtil.instance.getString(lPassword);

      String loginSaisi = _user.login;
      String passwordSaisi = _user.password;

      if (login.isEmpty || password.isEmpty || email.isEmpty) {
        Utilities.getToast("Merci de créer votre compte");
      } else {
        if ((loginSaisi == login || loginSaisi == email) &&
            passwordSaisi == password) {
          Utilities.getToast(lConnexionReussi);
          await PreferencesUtil.instance.putBool(lIsConnected, true);
          // suite
          Utilities.resetAndOpenPage(context: context);
        } else {
          Utilities.getToast(lErreurConnexion);
        }
      }
      */

    } else {
      setState(() {
        _autovalidate = true;
      });
    }
  }

  _submitOld() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      setState(() {
        // griser button
        _isDisable = true;
      });

      // log data
      Utilities.logByTag(contenu: _user.toString());

      //connexion a la bd

      RequestCustom request = RequestCustom();
      request.data = _user;

      ResponseCustom response = await baseApi.appelApi(
          endpointApi: user + connexion, request: request);

      if (response != null &&
          response.hasError != null &&
          !response.hasError! &&
          response.items != null &&
          response.items!.length > 0) {
        // maj de la bd locale
        User userConnected = User.fromJson(response.items![0]);
        // sauvegarder les informations neccessaires en local
        Utilities.saveInLocalBd(dataUser: userConnected);
        /*
        await PreferencesUtil.instance.putInt(lId, userConnected.id);
        await PreferencesUtil.instance.putString(lNom, userConnected.nom);
        await PreferencesUtil.instance.putString(lEmail, userConnected.email);
        await PreferencesUtil.instance.putString(lLogin, userConnected.login);
        await PreferencesUtil.instance
            .putString(lPassword, userConnected.password);
        await PreferencesUtil.instance.putString(lPhone, userConnected.phone);
        await PreferencesUtil.instance
            .putString(lPrenoms, userConnected.prenoms);
        await PreferencesUtil.instance.putBool(lIsConnected, true);
        */
        Utilities.getToast(lConnexionReussi);
        Utilities.resetAndOpenPage(context: context);
      } else {
        Utilities.messageApi(response: response);
      }

      setState(() {
        // degriser button
        _isDisable = false;
      });

      /*
      String login = await PreferencesUtil.instance.getString(lLogin);
      String email = await PreferencesUtil.instance.getString(lEmail);
      String password = await PreferencesUtil.instance.getString(lPassword);

      String loginSaisi = _user.login;
      String passwordSaisi = _user.password;

      if (login.isEmpty || password.isEmpty || email.isEmpty) {
        Utilities.getToast("Merci de créer votre compte");
      } else {
        if ((loginSaisi == login || loginSaisi == email) &&
            passwordSaisi == password) {
          Utilities.getToast(lConnexionReussi);
          await PreferencesUtil.instance.putBool(lIsConnected, true);
          // suite
          Utilities.resetAndOpenPage(context: context);
        } else {
          Utilities.getToast(lErreurConnexion);
        }
      }
      */

    } else {
      setState(() {
        _autovalidate = true;
      });
    }
  }
}
