import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/request_custom.dart';
import 'package:http/http.dart';

class NetworkCalls {
  //Future<String> post(
  Future<Uint8List> post(
      {required RequestCustom bodyRequest, required String endpointApi}) async {
    Map<String, String> headers = {"Content-type": "application/json"};
    Utilities.log("jsonEncode bodyRequest ${json.encode(bodyRequest)}");
    //client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);

    //   setStatevar rep = await client.post(Uri.https(baseUrlAPIDev, "/" + endpointApi),
    var rep = await client.post(Uri.http(baseUrlAPIProd, "/" + endpointApi),
        body: json.encode(bodyRequest), headers: headers);
    //var rep = await client.post(uri, body: bodyRequest.toJson(), headers: headers) ;
    //var rep = await client.post(uri, body: bodyRequest) ;
    checkAndThrowError(rep);
//return rep.body;
    return rep.bodyBytes;
  }

  Future<String> postInLocal(
      {required RequestCustom bodyRequest, required String endpointApi}) async {
    Map<String, String> headers = {"Content-type": "application/json"};
    Utilities.log("jsonEncode bodyRequest ${json.encode(bodyRequest)}");
    //client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);

    var rep = await client.post(Uri.http(baseUrlAPIDevLocal, "/" + endpointApi),
        body: json.encode(bodyRequest), headers: headers);
    //var rep = await client.post(uri, body: bodyRequest.toJson(), headers: headers) ;
    //var rep = await client.post(uri, body: bodyRequest) ;
    checkAndThrowError(rep);
    return rep.body;
  }

  Future<String> postByHttpClient(
      {required RequestCustom bodyRequest, required String endpointApi}) async {
    Map<String, String> headers = {"Content-type": "application/json"};
    Utilities.log("jsonEncode bodyRequest ${json.encode(bodyRequest)}");
    //client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);

    httpClient.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    // var rep = await client.post(Uri.https(baseUrlAPIDev, "/" + endpointApi),
    var rep = await client.post(Uri.http(baseUrlAPIProd, "/" + endpointApi),
        body: json.encode(bodyRequest), headers: headers);
    //var rep = await client.post(uri, body: bodyRequest.toJson(), headers: headers) ;
    //var rep = await client.post(uri, body: bodyRequest) ;
    checkAndThrowError(rep);
    return rep.body;
  }

  dynamic postCustom(
      {required RequestCustom bodyRequest, required String endpointApi}) {
    Map<String, String> headers = {"Content-type": "application/json"};

    client
        // .post(Uri.https(baseUrlAPIDev, "/" + endpointApi),
        .post(Uri.http(baseUrlAPIProd, "/" + endpointApi),
            body: json.encode(bodyRequest), headers: headers)
        .then((rep) {
      checkAndThrowError(rep);
      return rep.body;
    }).catchError((err) {
      print(err);
    });
  }

  static void checkAndThrowError(Response response) {
    if (response.statusCode != HttpStatus.ok) throw Exception(response.body);
  }
}
