import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/service_locator.dart';
import 'package:closeapps/verification_in_background.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  setupLocator(); // pour les communications avec les apps exterieures

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title:
          appName, // le nom qui s'affiche quand on regarde les apps qui sont en arriere plan
      home: VerificationInBackground(),
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: colorBlueLogoToMaterialColor,
      ),
    );
  }
}
