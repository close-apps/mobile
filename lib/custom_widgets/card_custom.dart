import 'package:cached_network_image/cached_network_image.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/fidelite_carte.dart';
import 'package:flutter/material.dart';
import 'package:getwidget/getwidget.dart';

class CardCustom extends StatefulWidget {
  FideliteCarte pdfOuSouscriptionPdf;
  //Function onTap;
  CardCustom({
    Key? key,
    required this.pdfOuSouscriptionPdf,
  }) : super(key: key);

  @override
  _CardCustomState createState() => _CardCustomState();
}

class _CardCustomState extends State<CardCustom> {
  @override
  Widget build(BuildContext context) {
    // Card(
    //   child: Stack(
    //     overflow: Overflow.clip,
    //     //£fit: StackFit.loose,
    //     children: <Widget>[
    //       // Image.asset(HuitPourCent.logo),
    //       (widget.pdfOuSouscriptionPdf != null &&
    //               widget.pdfOuSouscriptionPdf.urlLogo != null)
    //           ? Image.network(
    //               widget.pdfOuSouscriptionPdf.urlLogo,
    //               //widget.fideliteCarte.urlLogo,
    //               height: 150,
    //               width: 200,
    //               //fit: BoxFit.fitWidth,
    //               fit: BoxFit.fill,
    //               //width: double.infinity,
    //               //height: double.infinity,
    //             )
    //           : Image.asset(
    //               logoBlue,
    //               height: 150,
    //               width: 200,
    //               fit: BoxFit.fill,
    //               // width: double.infinity,
    //               // height: double.infinity,
    //             ),
    //       // Image.asset(
    //       //   widget.fideliteCarte != null &&
    //       //           widget.fideliteCarte.urlLogo != null
    //       //       ? widget.fideliteCarte.urlLogo
    //       //       : logoBlue,
    //       //   width: double.infinity,
    //       //   height: double.infinity,
    //       // ),
    //       //Image.asset(HuitPourCent.logo,width:double.infinity, height: double.infinity),
    //       Padding(
    //         padding: const EdgeInsets.all(8.0),
    //         child: Container(
    //           child: Text(
    //               widget.pdfOuSouscriptionPdf != null &&
    //                       widget.pdfOuSouscriptionPdf.libelle != null
    //                   ? widget.pdfOuSouscriptionPdf.libelle!
    //                   : "CLOSE APP",
    //               //maxLines: 1,
    //               overflow: TextOverflow.ellipsis,
    //               style: Utilities.style(
    //                   fontSize: 15,
    //                   color: (widget.pdfOuSouscriptionPdf != null &&
    //                           widget.pdfOuSouscriptionPdf.enseigneCouleur !=
    //                               null)
    //                       ? Color(widget.pdfOuSouscriptionPdf.enseigneCouleur)
    //                       : colorBlueLogo)),
    //           alignment: Alignment.bottomCenter,
    //         ),
    //       )
    //     ],
    //   ),
    // );

    return GFCard(
      boxFit: BoxFit.fill,
      showOverlayImage: true,
      imageOverlay: (widget.pdfOuSouscriptionPdf != null &&
              widget.pdfOuSouscriptionPdf.urlLogo != null)
          ? NetworkImage(
              widget.pdfOuSouscriptionPdf.urlLogo,
            ) as ImageProvider<Object>?
          : AssetImage(
              logoBlue,
            ),
      showImage: true,
      margin: EdgeInsets.all(8),
      title: GFListTile(
        title: Container(
          padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 2),
          //padding: const EdgeInsets.all(8.0),
          decoration: ShapeDecoration(
              shape: Utilities.getDefaultShape(),
              color: (widget.pdfOuSouscriptionPdf.isLocked != null &&
                      widget.pdfOuSouscriptionPdf.isLocked!
                  ? GFColors.DANGER
                  : widget.pdfOuSouscriptionPdf.isNotifed != null &&
                          widget.pdfOuSouscriptionPdf.isNotifed!
                      ? GFColors.WARNING
                      : Colors.white)),
          alignment: Alignment.bottomCenter,
          child: Text(
            widget.pdfOuSouscriptionPdf != null &&
                    widget.pdfOuSouscriptionPdf.libelle != null
                ? widget.pdfOuSouscriptionPdf.libelle!
                : "CLOSE APP",
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        margin: EdgeInsets.all(0),
        padding: EdgeInsets.all(0),
      ),
    );
  }
}
