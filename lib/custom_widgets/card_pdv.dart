import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/fidelite_carte.dart';
import 'package:closeapps/views/google_map_view.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:map_launcher/map_launcher.dart';

class CardPDV extends StatelessWidget {
  const CardPDV(
      {Key? key,
      required this.informationsPDVDuPDF,
      this.listPDVDuPDF,
      this.isPDFTampon = true})
      : super(key: key);
  final List<FideliteCarte>? listPDVDuPDF;
  final bool isPDFTampon;
  final FideliteCarte informationsPDVDuPDF;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: colorBlueLogo,
      //onTap: onTap != null ? onTap : () {},
      onTap: () async {
        if (informationsPDVDuPDF.longitude != null &&
            informationsPDVDuPDF.latitude != null) {
          // Utilities.navigatorPush(
          //     context: context,
          //     view: GoogleMapView(
          //         listPDVDuPDF: listPDVDuPDF,
          //         informationsPDVDuPDF: informationsPDVDuPDF,
          //         isPDFTampon: isPDFTampon));

          Position? initialPosition = await _determinePosition();
          if (initialPosition != null) {
            bool isGoogleMaps =
                await MapLauncher.isMapAvailable(MapType.google) ?? false;

            if (isGoogleMaps) {
              final availableMaps = await MapLauncher.installedMaps;

              for (int i = 0; i < availableMaps.length; i++) {
                if (availableMaps[i].mapType == MapType.google) {
                  availableMaps[i].showDirections(
                      destination: Coords(informationsPDVDuPDF.latitude!,
                          informationsPDVDuPDF.longitude!),
                      origin: Coords(
                          initialPosition.latitude, initialPosition.longitude),
                      destinationTitle: informationsPDVDuPDF.pointDeVenteNom,
                      originTitle: "Position initiale");
                  break;
                }
              }
            } else {
              Utilities.getToast(
                  "Vous devez avoir google maps installée afin de profiter de cette fonctionnalité !!!");
            }
          } else {
            Utilities.getToast(
                "Merci de lui accorder les droits afin de profiter de cette fonctionnalité !!!");
          }
        } else {
          Utilities.getToast(pdvSansCoordonneMap);
        }
      },
      child: Card(
        borderOnForeground: false,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                lNom +
                    deuxPoint +
                    (informationsPDVDuPDF.pointDeVenteNom ?? lAVenir),
                style: Utilities.style(color: colorBlueLogo, fontSize: 14),
                //textAlign: TextAlign.center,
              ),
              Utilities.getDefaultSizeBoxWithHeight(),
              Text(
                lVille +
                    deuxPoint +
                    //(villeLibelle != null ? villeLibelle : lAVenir),
                    (informationsPDVDuPDF.villePointDeVente ?? lAVenir),
                style: Utilities.style(color: colorBlueLogo, fontSize: 14),
                //textAlign: TextAlign.center,
              ),
              Utilities.getDefaultSizeBoxWithHeight(),
              Text(
                lCommune +
                    deuxPoint +
                    (informationsPDVDuPDF.communePointDeVente ?? lAVenir),
                //(communeLibelle != null ? communeLibelle : lAVenir),
                //overflow: TextOverflow.ellipsis,
                style: Utilities.style(color: colorBlueLogo, fontSize: 14),
                //textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<Position?> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      //return Future.error('Location services are disabled.');
      return null;
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        //return Future.error('Location permissions are denied');
        return null;
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      //setState(() {});
      // return Future.error(
      //     'Location permissions are permanently denied, we cannot request permissions.');
      return null;
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.

    return await Geolocator.getCurrentPosition();
  }
}
