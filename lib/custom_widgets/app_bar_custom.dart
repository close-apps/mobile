import 'package:closeapps/helpers/constants.dart';
import 'package:flutter/material.dart';

class AppBarCustom extends StatefulWidget implements PreferredSizeWidget {
  List<Widget>? actions;
  Widget? title;
  Widget? leading;
  //Color? backgroundColor;
  double? elevation;

  AppBarCustom({
    Key? key,
    this.actions,
    this.title,
    this.elevation,
    this.leading,
    //this.backgroundColor })
    //this.backgroundColor = colorBlueLogo
  }) : super(key: key);

  @override
  _AppBarCustomState createState() => _AppBarCustomState();

  @override
  // TODO: implement preferredSize
  //Size get preferredSize => null ;
  //Size get preferredSize => Size.fromHeight(kToolbarHeight + (bottom?.preferredSize?.height ?? 0.0)) ;
  Size get preferredSize => new Size.fromHeight(AppBar().preferredSize.height);
}

class _AppBarCustomState extends State<AppBarCustom> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: widget.elevation,
      leading: widget.leading,
      actions: widget.actions,
      title: widget.title,
      // backgroundColor: widget.backgroundColor
    );
  }
}
