import 'package:closeapps/custom_widgets/boutton_connect_with.dart';
import 'package:closeapps/custom_widgets/my_text_form_field.dart';
import 'package:closeapps/custom_widgets/sing_in.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/preferences_util.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/request_custom.dart';
import 'package:closeapps/models/response_custom.dart';
import 'package:closeapps/models/user.dart';
import 'package:firebase_auth/firebase_auth.dart' as FireUser;
import 'package:flutter/material.dart';
import 'package:flutter_signin_button/button_list.dart';
import 'package:flutter_signin_button/button_view.dart';
//import 'package:passwordfield/passwordfield.dart';
import 'package:string_validator/string_validator.dart';

class FormInscription extends StatefulWidget {
  User? user;
  //String login;
  int? userId;
  bool isUpdate;
  FormInscription(
      //{Key key, this.user, this.isUpdate = false, this.userId, this.login})
      {
    Key? key,
    this.user,
    this.isUpdate = false,
    this.userId,
  }) : super(key: key);

  @override
  _FormInscriptionState createState() => _FormInscriptionState();
}

class _FormInscriptionState extends State<FormInscription> {
  final _formKey = GlobalKey<FormState>();

  bool _dynamicShape = true;
  bool _isDisable = false;

  TextEditingController _controllerPrenoms = TextEditingController();
  TextEditingController _controllerLogin = TextEditingController();
  TextEditingController _controllerPhone = TextEditingController();
  TextEditingController _controllerPhoneParrain = TextEditingController();
  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerNom = TextEditingController();
  TextEditingController _controllerDateNaissance = TextEditingController();
  TextEditingController _controllerVille = TextEditingController();
  TextEditingController _controllerCommune = TextEditingController();
  TextEditingController _controllerPassword = TextEditingController();
  TextEditingController _controllerConfirmPassword = TextEditingController();

  bool _autovalidate = false;
  bool _isObscureTextPassword = true;
  bool _isObscureTextConfirmPassword = true;
  bool _autovalidateEmail = false;
  bool _autovalidatePassword = false;
  bool _autovalidateConfirmPassword = false;
  bool _isWaiting = true;
  User? dataUser;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (widget.user == null) {
      widget.user = User();
    }

    dataUser = widget.user!;
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    print(widget.userId);

    initialize(userId: widget.userId);
  }

  @override
  Widget build(BuildContext context) {
    print(widget.isUpdate);
    return (widget.isUpdate && _isWaiting)
        //? (_isWaiting) ? CircularProgressIndicator() : Container()
        ? Center(child: CircularProgressIndicator())
        : Form(
            autovalidate: _autovalidate,
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListView(
                // shrinkWrap: true,
                children: <Widget>[
                  //Image.asset(logoBlue, width: 100, height: 50),
                  Utilities.getDefaultSizeBoxWithHeight(),
                  MyTextFormField(
                    controller: _controllerNom,
                    decoration: Utilities.getDefaultInputDecoration(
                        labelText: lNom, isRequiredChamp: true),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return erreurSaisieTexte;
                      }
                      return null;
                      //Utilities.validator(value, typeContentToValidate: typeText);
                    },
                    onSaved: (input) {
                      dataUser!.nom = input;
                    },
                    onChanged: (value) {
                      _changeDynamicShape();
                    },
                    //controller: controllerLibelle,
                  ),
                  //!isUpdate() ?
                  Utilities.getDefaultSizeBoxWithHeight(),

                  MyTextFormField(
                    controller: _controllerPrenoms,
                    decoration: Utilities.getDefaultInputDecoration(
                      labelText: lPrenoms,
                    ),
                    validator: (value) {
                      return null;
                    },
                    onSaved: (input) {
                      dataUser!.prenoms = input;
                    },
                    onChanged: (value) {
                      _changeDynamicShape();
                    },
                  ),
                  Utilities.getDefaultSizeBoxWithHeight(),

                  //: Container(),
                  MyTextFormField(
                    controller: _controllerEmail,
                    decoration: Utilities.getDefaultInputDecoration(
                        labelText: lEmail, isRequiredChamp: true),
                    keyboardType: TextInputType.emailAddress,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return erreurSaisieTexte;
                      }
                      if (!isEmail(value)) {
                        return erreurSaisieEmail;
                      }

                      return null;
                    },
                    onSaved: (input) {
                      dataUser!.email = input;
                    },
                    onChanged: (value) {
                      if (!isEmail(value)) {
                        setState(() {
                          _autovalidateEmail = true;
                        });
                      }
                      //_changeDynamicShape();
                    },
                    autovalidate: _autovalidateEmail,
                  ),
                  Utilities.getDefaultSizeBoxWithHeight(),

                  !isUpdate()
                      ? MyTextFormField(
                          controller: _controllerLogin,
                          decoration: Utilities.getDefaultInputDecoration(
                              labelText: lIdentifiant, isRequiredChamp: true),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return erreurSaisieTexte;
                            }
                            return null;
                          },
                          onSaved: (input) {
                            dataUser!.login = input;
                          },
                          onChanged: (value) {
                            _changeDynamicShape();
                          },
                        )
                      : Container(),
                  Utilities.getDefaultSizeBoxWithHeight(),

                  MyTextFormField(
                    controller: _controllerPhone,
                    decoration: Utilities.getDefaultInputDecoration(
                      labelText: lPhone,
                    ),
                    keyboardType: TextInputType.number,
                    validator: (value) {
                      // Utilities.validator(value, typeContentToValidate: typeText);
                      /*
                if (value.isEmpty) {
                  return erreurSaisieTexte;
                }
                */
                      return null;
                    },
                    onSaved: (input) {
                      dataUser!.telephone = input;
                    },
                    onChanged: (value) {
                      _changeDynamicShape();
                    },
                  ),
                  Utilities.getDefaultSizeBoxWithHeight(),
                  !isUpdate()
                      ? MyTextFormField(
                          controller: _controllerPassword,
                          obscureText: _isObscureTextPassword,
                          decoration:
                              // InputDecoration(
                              //   labelText: lMotDePasse,
                              //   suffixIcon: IconButton(
                              //     icon: Icon(_isObscureTextPassword
                              //         ? Icons.visibility_off
                              //         : Icons.visibility),
                              //     onPressed: () {
                              //       setState(() {
                              //         _isObscureTextPassword =
                              //             !_isObscureTextPassword;
                              //       });
                              //     },
                              //   ),
                              //   //suffix: Icon(Icons.visibility),
                              // )
                              Utilities.getDefaultInputDecoration(
                                  labelText: lMotDePasse,
                                  isRequiredChamp: true,
                                  suffixIcon: IconButton(
                                    icon: Icon(_isObscureTextPassword
                                        ? Icons.visibility_off
                                        : Icons.visibility),
                                    onPressed: () {
                                      setState(() {
                                        _isObscureTextPassword =
                                            !_isObscureTextPassword;
                                      });
                                    },
                                  )),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return erreurSaisieTexte;
                            }
                            if (!equals(
                                value, _controllerConfirmPassword.text)) {
                              return erreurPassword;
                            }
                            return null;
                            //Utilities.validator(value, typeContentToValidate: typeText);
                          },
                          onSaved: (input) {
                            dataUser!.password = input;
                          },
                          onChanged: (value) {
                            if (_controllerConfirmPassword.text.isNotEmpty) {
                              setState(() {
                                _autovalidatePassword = true;
                              });
                            }
                          },
                          autovalidate: _autovalidatePassword,
                        )
                      : Container(),
                  Utilities.getDefaultSizeBoxWithHeight(),

                  !isUpdate()
                      ? MyTextFormField(
                          controller: _controllerConfirmPassword,
                          obscureText: _isObscureTextConfirmPassword,
                          decoration:
                              // InputDecoration(
                              //   labelText: lConfirmerPassword,
                              //   suffixIcon: IconButton(
                              //     icon: Icon(_isObscureTextConfirmPassword
                              //         ? Icons.visibility_off
                              //         : Icons.visibility),
                              //     onPressed: () {
                              //       setState(() {
                              //         _isObscureTextConfirmPassword =
                              //             !_isObscureTextConfirmPassword;
                              //       });
                              //     },
                              //   ),
                              //   //suffix: Icon(Icons.visibility),
                              // )
                              Utilities.getDefaultInputDecoration(
                                  labelText: lConfirmerPassword,
                                  isRequiredChamp: true,
                                  suffixIcon: IconButton(
                                    icon: Icon(_isObscureTextConfirmPassword
                                        ? Icons.visibility_off
                                        : Icons.visibility),
                                    onPressed: () {
                                      setState(() {
                                        _isObscureTextConfirmPassword =
                                            !_isObscureTextConfirmPassword;
                                      });
                                    },
                                  )),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return erreurSaisieTexte;
                            }
                            if (!equals(value, _controllerPassword.text)) {
                              return erreurPassword;
                            }

                            return null;
                            //Utilities.validator(value, typeContentToValidate: typeText);
                          },
                          onSaved: (input) {
                            //dataUser.libelle = input;
                          },
                          onChanged: (value) {
                            if (_controllerPassword.text.isNotEmpty) {
                              setState(() {
                                _autovalidateConfirmPassword = true;
                              });
                            }
                          },
                          autovalidate: _autovalidateConfirmPassword,
                        )
                      : Container(),
                  Utilities.getDefaultSizeBoxWithHeight(
                      //height: 40,
                      ),
                  Utilities.getButtonSubmit(
                      //shape: (_dynamicShape)? Utilities.getShape(topLeft: 50, bottomRight: 50): Utilities.getShape(bottomLeft: 50, topRight: 50),
                      //child: Text(isUpdate() ? lModifier : lSinscrire),
                      libelle: (isUpdate() ? lModifier : lSinscrire),
                      onPressed: _submit,
                      isDisable: _isDisable),
                  // SingInButton(
                  //   onPressed: () async {
                  //     FireUser.User userGoogle =
                  //         await signInWithGoogle(); // importation de la classe User ambigue d'où FireUser.User

                  //     if (userGoogle != null &&
                  //         userGoogle.displayName != null &&
                  //         userGoogle.email != null) {
                  //       // construction du dataUser
                  //       _submit(userGoogle: userGoogle);
                  //     } else {
                  //       Utilities.getToast(echec);
                  //     }
                  //   },
                  //   libelleBoutton: libelleBouttonSinscrireGoogle,
                  // ),
                  // SingInButton(
                  //   libelleBoutton: libelleBouttonSinscrireFacebook,
                  //   logo: facebookLogo,
                  //   onPressed: () => Utilities.getToast(lAVenir),
                  // ),

                  isUpdate()
                      ? Container()
                      : SignInButton(
                          Buttons.Google,
                          onPressed: () async {
                            FireUser.User userGoogle =
                                await signInWithGoogle(); // importation de la classe User ambigue d'où FireUser.User

                            if (userGoogle != null &&
                                userGoogle.displayName != null &&
                                userGoogle.email != null) {
                              // construction du dataUser
                              _submit(userGoogle: userGoogle);
                            } else {
                              Utilities.getToast(echec);
                            }
                          },
                          elevation: 1,
                          text: libelleBouttonSinscrireGoogle,
                          //text: "Google",

                          shape: Utilities.getDefaultShape(),
                          //mini: true,
                        ),
                  isUpdate()
                      ? Container()
                      : SignInButton(
                          Buttons.FacebookNew,
                          onPressed: () => Utilities.getToast(lAVenir),
                          elevation: 1,
                          padding: EdgeInsets.all(0),
                          shape: Utilities.getDefaultShape(),
                          text: libelleBouttonSinscrireFacebook,
                        ),
                ],
              ),
            ),
          );
  }

  initialize({String? baseUrl, int? userId}) async {
    if (userId != null && userId > 0) {
      RequestCustom request = RequestCustom();

      User data = User();
      //data.login = login;
      data.id = userId;
      request.data = data;

      ResponseCustom response = await baseApi.appelApi(
          //apiBaseUrl: baseUrlAPIDev + user + getByCriteria, request: request);
          endpointApi: user + getByCriteria,
          request: request);

      if (response != null &&
          response.hasError != null &&
          !response.hasError! &&
          response.items != null &&
          response.items!.length > 0) {
        widget.user = User.fromJson(response.items![0]);

        _initializeInput(user: widget.user);
        setState(() {
          _isWaiting = false;
        });
      } else {
        Utilities.messageApi(response: response);
        setState(() {
          _isWaiting = false;
        });
      }
    } else {
      setState(() {
        _isWaiting = false;
      });
    }
  }

  // initialiser les inputs
  _initializeInput({User? user}) {
    Utilities.begin("_initializeInput");
    if (user != null) {
      _controllerNom.text = user.nom ?? "";
      _controllerPrenoms.text = user.prenoms ?? "";
      _controllerLogin.text = user.login ?? "";
      _controllerEmail.text = user.email ?? "";
      _controllerPhone.text = user.telephone ?? "";
    }
  }

  void _submit({FireUser.User? userGoogle}) async {
    if ((userGoogle != null) || _formKey.currentState!.validate()) {
      setState(() {
        // griser button
        _isDisable = true;
      });
      //Utilities.progressingAfterSubmit(context: context);

      Utilities.loadingIndicator(context: context, color: colorRedLogo);

      // permet d'executer toutes les validator des TextFormField
      _formKey.currentState!
          .save(); // permet d'executer toutes les onSaved des TextFormField
      // appel de service

      dataUser!.id = widget.userId;

      // if (userGoogle != null) {
      //   List<String> tabNom = userGoogle.displayName?.split(" ");
      //   if (tabNom != null) {
      //     dataUser.nom = tabNom.last;
      //     dataUser.prenoms = tabNom.first;
      //   }
      //   dataUser.telephone = userGoogle.phoneNumber;
      //   dataUser.email = userGoogle.email;
      //   dataUser.byFirebaseUser = true;
      // }
      Utilities.setUserByFireUserGoogle(fireUser: userGoogle, user: dataUser);
      print(dataUser.toString());

      RequestCustom request = RequestCustom();
      request.user = widget.userId;
      dataUser!.isUserMobile = true;
      request.datas = [dataUser];

      String uri = user + (isUpdate() ? update : create);
      ResponseCustom response =
          await baseApi.appelApi(endpointApi: uri, request: request);

      Navigator.pop(context); // pour retirer le progressing
      if (response != null) {
        if (response.hasError != null && !response.hasError!) {
          Utilities.messageApi(response: response);
          // vider le formmualaire
          _clearForm();
          // redirection à l'accueil
          if (isUpdate()) {
            Navigator.of(context).pop(); // pour le formmualaire
          } else {
            // redirection sur la page d'inscription reussi
            // tous supprimer
            Utilities.resetAndOpenPage(context: context);
            // Utilities.navigate(context, MyApp());
          }

          User userSave = User.fromJson(response.items![0]);

          //dataUser.id = userSave.id;

          // sauvegarder les informations neccessaires en local
          Utilities.saveInLocalBd(dataUser: userSave);

          /*
          await PreferencesUtil.instance.putInt(lId, userSave.id);
          await PreferencesUtil.instance.putString(lNom, dataUser.nom);
          await PreferencesUtil.instance.putString(lEmail, dataUser.email);
          await PreferencesUtil.instance.putString(lLogin, dataUser.login);
          await PreferencesUtil.instance
              .putString(lPassword, dataUser.password);
          await PreferencesUtil.instance.putString(lPhone, dataUser.phone);
          await PreferencesUtil.instance.putString(lPrenoms, dataUser.prenoms);
          await PreferencesUtil.instance.putBool(lIsConnected, true);
          */

          // redirection sur la page d'inscription reussi
          showDialog(
            context: context,
            builder: (BuildContext context) {
              // return object of type Dialog
              return AlertDialog(
                title: Text((isUpdate()) ? majReussi : inscriptionReussi),
                content: Text(
                  (isUpdate()) ? messageMAJProfil : messageApresInscription,
                  textAlign: TextAlign.justify,
                ),
                actions: <Widget>[
                  // usually buttons at the bottom of the dialog
                  FlatButton(
                    child: new Text(okButtonLabel),
                    onPressed: () {
                      Navigator.of(context).pop(); // le show dialog

                      //Utilities.onSelectDrawerItem(context: context, view: Offres());
                    },
                  ),
                ],
              );
            },
          );
        } else {
          //Utilities.navigate(context, InscriptionReussi()) ;
          Utilities.messageApi(response: response);
        }
      } else {
        Utilities.messageApi();
      }
      setState(() {
        // degriser button
        _isDisable = false;
      });
      //Utilities.navigatorPush(context: context, view: Home());
    } else {
      setState(() {
        _autovalidate = true;
      });
    }
  }

  _changeDynamicShape() {
    setState(() {
      _dynamicShape = !_dynamicShape;
    });
  }

  _passwordCorrect() {
    if (_controllerConfirmPassword.text.isNotEmpty &&
        _controllerPassword.text.isNotEmpty) {
      equals(_controllerConfirmPassword.text, _controllerPassword.text);
    }
  }

  bool isUpdate() {
    return (widget.isUpdate != null && widget.isUpdate);
  }

  _clearForm() {
    Utilities.clearForm([
      _controllerCommune,
      _controllerConfirmPassword,
      _controllerDateNaissance,
      _controllerEmail,
      _controllerLogin,
      _controllerNom,
      _controllerPassword,
      _controllerPhone,
      _controllerPhoneParrain,
      _controllerVille
    ]);
  }
}
