import 'dart:convert';

import 'package:closeapps/helpers/utilities.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;
final GoogleSignIn googleSignIn = GoogleSignIn();

Future<User> signInWithGoogle() async {
//Future<String> signInWithGoogle() async {
  Utilities.begin("signInWithGoogle");
  final GoogleSignInAccount? googleSignInAccount = await googleSignIn.signIn();
  final GoogleSignInAuthentication googleSignInAuthentication =
      await googleSignInAccount!.authentication;

  // final AuthCredential credential = GoogleAuthProvider.getCredential(
  //   accessToken: googleSignInAuthentication.accessToken,
  //   idToken: googleSignInAuthentication.idToken,
  // );

  final AuthCredential credential = GoogleAuthProvider.credential(
    accessToken: googleSignInAuthentication.accessToken,
    idToken: googleSignInAuthentication.idToken,
  );

  final UserCredential authResult =
      await _auth.signInWithCredential(credential);
  final User? user = authResult.user;

  assert(!user!.isAnonymous);
  assert(await user!.getIdToken() != null);

  final User? currentUser = _auth.currentUser;
  assert(user!.uid == currentUser!.uid);
  Utilities.end("signInWithGoogle");
  //Utilities.getToast(user.displayName);
  //Utilities.getToast(user.email);
  //Utilities.getToast(user.phoneNumber);

  // insertion dans la bd en ligne
  // insertion dans la bd en local

  print('signInWithGoogle succeeded: $user');
  print('displayName: ${user!.displayName}');
  print('email: ${user.email}');
  print('emailVerified: ${user.emailVerified}');
  print('isAnonymous: ${user.isAnonymous}');
  print('photoURL: ${user.photoURL}');
  print('isAnonymous: ${user.isAnonymous}');

  return user;
}

/*
Future<String> signInWithGoogleOld() async {
  final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
  final GoogleSignInAuthentication googleSignInAuthentication =
      await googleSignInAccount.authentication;

  final AuthCredential credential = GoogleAuthProvider.getCredential(
    accessToken: googleSignInAuthentication.accessToken,
    idToken: googleSignInAuthentication.idToken,
  );

  final AuthResult authResult = await _auth.signInWithCredential(credential);
  final FirebaseUser user = authResult.user;

  assert(!user.isAnonymous);
  assert(await user.getIdToken() != null);

  final FirebaseUser currentUser = await _auth.currentUser();
  assert(user.uid == currentUser.uid);

  return 'signInWithGoogle succeeded: $user';
}
*/

void signOutGoogle() async {
  await googleSignIn.signOut();

  print("User Sign Out");
}
