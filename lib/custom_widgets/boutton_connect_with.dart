import 'package:closeapps/helpers/constants.dart';
import 'package:flutter/material.dart';

class SingInButton extends StatelessWidget {
  String? logo;
  String libelleBoutton;
  Function()? onPressed;
  Color? splashColor;
  Color? colorBorderSide;
  SingInButton(
      {Key? key,
      required this.libelleBoutton,
      this.logo,
      required this.onPressed,
      this.colorBorderSide,
      this.splashColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: OutlineButton(
        splashColor: splashColor != null ? splashColor : Colors.grey,
        onPressed: onPressed ?? () {},
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
        highlightElevation: 0,
        borderSide: colorBorderSide != null
            ? BorderSide(color: colorBorderSide!)
            : BorderSide(color: Colors.grey),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 6, 0, 6),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image(image: AssetImage(logo ?? googleLogo), height: 35.0),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text(
                  libelleBoutton.isNotEmpty
                      ? libelleBoutton
                      : libelleBouttonConnexionGoogle,
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.grey,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
