import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/models/fidelite_carte.dart';
import 'package:flutter/material.dart';
import 'package:getwidget/getwidget.dart';

class ItemCustom extends StatefulWidget {
  FideliteCarte pdfOuSouscriptionPdf;
  ItemCustom({Key? key, required this.pdfOuSouscriptionPdf}) : super(key: key);

  @override
  _ItemCustomState createState() => _ItemCustomState();
}

class _ItemCustomState extends State<ItemCustom> {
  @override
  Widget build(BuildContext context) {
    // ListTile(
    //   shape: Utilities.getDefaultShape(),
    //   leading: CircleAvatar(
    //     //child: Text("data"),

    //     //radius: 50, backgroundImage: NetworkImage("url"),
    //     child: (widget.pdfOuSouscriptionPdf != null &&
    //             widget.pdfOuSouscriptionPdf.urlLogo != null)
    //         ? Image.network(widget.pdfOuSouscriptionPdf.urlLogo)
    //         : Image.asset(logoBlue),
    //     radius: 25,
    //     backgroundColor: Colors.white,
    //     // backgroundImage: AssetImage(widget.pdfOuSouscriptionPdf != null &&
    //     //         widget.pdfOuSouscriptionPdf.urlLogo != null
    //     //     ? widget.pdfOuSouscriptionPdf.urlLogo
    //     //     : logoBlue),
    //   ),

    //   // CircleAvatar(
    //   //     radius: 20.0,
    //   //     backgroundColor: Colors.white,
    //   //     child: CircleAvatar(
    //   //       radius:  17.0 ,
    //   //       backgroundColor: Colors.grey[200],
    //   //       backgroundImage: CachedNetworkImageProvider("https://close-apps-front-office.herokuapp.com/assets/images/logo_fond_bleu.png"),
    //   //     ),
    //   //   ),

    //   title: Text(
    //       widget.pdfOuSouscriptionPdf != null &&
    //               widget.pdfOuSouscriptionPdf.libelle != null
    //           ? widget.pdfOuSouscriptionPdf.libelle!
    //           : "CLOSE APP",
    //       style: Utilities.style(
    //           color: (widget.pdfOuSouscriptionPdf != null &&
    //                   widget.pdfOuSouscriptionPdf.enseigneCouleur != null)
    //               ? Color(widget.pdfOuSouscriptionPdf.enseigneCouleur)
    //               : colorBlueLogo)),

    //   //Spacer(),
    //   //IconButton(icon: Icon(Icons.arrow_right), onPressed: () {})
    // );

    return GFListTile(
        margin: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
        color: widget.pdfOuSouscriptionPdf.isLocked != null &&
                widget.pdfOuSouscriptionPdf.isLocked!
            ? GFColors.DANGER
            : widget.pdfOuSouscriptionPdf.isNotifed != null &&
                    widget.pdfOuSouscriptionPdf.isNotifed!
                ? GFColors.WARNING
                : GFColors.WHITE,
        title: Text(
            widget.pdfOuSouscriptionPdf != null &&
                    widget.pdfOuSouscriptionPdf.libelle != null
                ? widget.pdfOuSouscriptionPdf.libelle!
                : "CLOSE APP",
            style: Utilities.style(
                color: (widget.pdfOuSouscriptionPdf != null &&
                        widget.pdfOuSouscriptionPdf.enseigneCouleur != null)
                    ? Color(widget.pdfOuSouscriptionPdf.enseigneCouleur)
                    : colorBlueLogo)),
        avatar: GFImageOverlay(
          image: (widget.pdfOuSouscriptionPdf != null &&
                  widget.pdfOuSouscriptionPdf.urlLogo != null)
              ? NetworkImage(widget.pdfOuSouscriptionPdf.urlLogo)
              : AssetImage(logoBlue) as ImageProvider<Object>?,
          //boxFit: BoxFit.fill,
          width: 30,
          height: 30,
          //shape: Utilities.getDefaultShape(),
        )

        //  CircleAvatar(
        //   child: (widget.pdfOuSouscriptionPdf != null &&
        //           widget.pdfOuSouscriptionPdf.urlLogo != null)
        //       ? Image.network(widget.pdfOuSouscriptionPdf.urlLogo)
        //       : Image.asset(logoBlue),
        //   radius: 25,
        //   backgroundColor: Colors.white,
        // ),
        );
  }
}


// return Card(
//       child: Row(
//         children: <Widget>[
//           CircleAvatar(
//             //child: Text("data"),

//             //radius: 50, backgroundImage: NetworkImage("url"),

//             radius: 25,
//             backgroundImage: AssetImage(widget.pdfOuSouscriptionPdf != null &&
//                     widget.pdfOuSouscriptionPdf.urlLogo != null
//                 ? widget.pdfOuSouscriptionPdf.urlLogo
//                 : logoBlue),
//             backgroundColor: colorBlueLogo,
//           ),
//           /*ListTile(
//             leading: Text("fghjkl"),
//             trailing: IconButton(icon: Icon(Icons.arrow_right), onPressed: () {}),
//           ),*/
//           //Spacer(flex: 2,),
//           SizedBox(
//             // mettre espace entre 2 elements width ou height
//             width: 12.0,
//           ),

//           Text(
//               widget.pdfOuSouscriptionPdf != null &&
//                       widget.pdfOuSouscriptionPdf.libelle != null
//                   ? widget.pdfOuSouscriptionPdf.libelle!
//                   : "CLOSE APP",
//               style: Utilities.style(
//                   color: (widget.pdfOuSouscriptionPdf != null &&
//                           widget.pdfOuSouscriptionPdf.colorLogo != null)
//                       ? widget.pdfOuSouscriptionPdf.colorLogo
//                       : colorBlueLogo)),

//           //Spacer(),
//           //IconButton(icon: Icon(Icons.arrow_right), onPressed: () {})
//         ],
//       ),
//     );
  