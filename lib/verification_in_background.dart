import 'package:closeapps/helpers/constants.dart';
import 'package:closeapps/helpers/preferences_util.dart';
import 'package:closeapps/helpers/utilities.dart';
import 'package:closeapps/views/home.dart';
import 'package:closeapps/views/loader_2.dart';
import 'package:closeapps/views/welcome.dart';
import 'package:flutter/material.dart';

class VerificationInBackground extends StatefulWidget {
  VerificationInBackground({Key? key}) : super(key: key);

  @override
  _VerificationInBackgroundState createState() =>
      _VerificationInBackgroundState();
}

class _VerificationInBackgroundState extends State<VerificationInBackground> {
  bool verificationLoading = true;
  bool _isNewUser = false;
  @override
  void initState() {
    Utilities.begin("VerificationInBackground/initState");
    // TODO: implement initState
    super.initState();
    isNewUser();
  }

  @override
  Widget build(BuildContext context) {
    Utilities.begin("VerificationInBackground/build");
    return Scaffold(
      // appBar: AppBar(
      //   //title: Text(appName),
      //   backgroundColor: colorBlueLogo,
      // ),
      body: Container(
          child: (verificationLoading)
              //? Loader()
              ? Loader2()
              : (_isNewUser
                  ? Welcome()
                  //Navigator.push(context, MaterialPageRoute(builder: (context) => Welcome()))
                  : Home())),
    );
  }

  // verification method if new user or old
  isNewUser() async {
    // connexion in dataBase or networkCall
    bool isConnect = false;
    await Future.delayed(Duration(seconds: nbreSecondAnimation), () {});

    await PreferencesUtil.instance.getBool(isConnected).then((resp) {
      isConnect = resp;
    }).catchError((err) {
      print(err);
    });

    setState(() {
      verificationLoading = false;
      _isNewUser = !isConnect;
      //_isNewUser = isConnected != null ? !isConnect : true; ancien
    });

    //return true;
  }
}
